# PLOT POINTS:
# 1) Studying at Jack's
# 2) Suspecting Jack is gay
# 3) Trying to get you a private tutor (threatening your relationship)

label start_dinner_2:

    m "Hi sweetie."
    show mom sit

    if waiting_action == "eat":
        m "Oh, you started eating without me. You're very impatient."
        n "...right."
    elif waiting_action == "wait":
        m "You could have started without me. No need to let your food get cold."
        n "...sure."
    elif waiting_action == "play":
        m "It's immature to play with your food, you know."
        n "Yeah, yeah."

    m "Your father's running late. He'll be joining us for dinner in an hour's time."

    menu:
        "Cool. Let's eat.":
            n "Cool. Let's eat."
            n "*nom nom nom*"
            m ". . ."
            m "What's your plans for tomorrow?"
        "I have something to tell both of you.":
            n "I have something to tell both of you."
            m "Alright. Tell us both later when he comes back."
            n "Oh. Okay."
            m ". . ."
            n "*nom nom nom*"
            m "So, what's your plans for tomorrow?"
        "There's something I need to tell just you first.":
            n "There's something I need to tell just you first."
            m "Hold on Nick, I haven't asked about your day yet!"
            n "Today was fine."
            m "Okay. And what's your plans for tomorrow?"

label start_dinner_2_1:

    n "Oh. Uh... studying."
    n "Yeah. Tomorrow I'm studying."
    m "What subject?"
    n "Er..."

    menu:
        "Chemistry.":
            $ message = studying_subject = _("Chemistry")
        "Calculus.":
            $ message = studying_subject = _("Calculus")
        "Compsci.":
            $ message = studying_subject = _("Computer Science")

label start_dinner_2_2:

    $ n("[message!t]")
    m "Good."
    m "You really, really could improve your grades in your [studying_subject] class."
    n ". . ."
    m "So, I'll be at the library tomorrow."
    m "Will I see you studying there?"
    n "Actually, I'm gonna study at Jack's place."
    m "Again?"
    m "You spend a lot of time with him."

    menu:
        "We just study together, that's all.":
            $ message = "We just study together, that's all."
            $ relationship = "study"
            jump buddy_1
        "Mom, Jack is... more than a friend.":

            $ relationship = "best friend"
            n "Mom, Jack is... more than a friend."

            $ lying_about_hanging_out = True
            m "Oh, like best friends?"
            n "Um. Well--"
            m "So you're just hanging out, not studying."
            n "We ARE studying!"
            m ". . ."
            m "Alright, just don't lie to me."
            n "I'm not."
            jump buddy_1_point_5
        "Well yeah, that's what good pals do.":
            $ message = "Well yeah, that's what good pals do."
            $ relationship = "friend"
            jump buddy_1


# ///////////////////////////////////////
# ////// 2) SUSPECTING Jack IS GAY ///////
# ///////////////////////////////////////


label buddy_1:
    $ n("[message!t]")

    if relationship != "study":
        $ lying_about_hanging_out = True
        m "Oh. So you're just hanging out, not studying."
        n "We ARE studying!"
        m ". . ."
        m "Alright, just don't lie to me."
        n "I'm not."
    else:
        m "Okay. I'm just making sure."
        n "Of... what?"

    jump buddy_1_point_5

label buddy_caught_lying_1(locmessage):
    $ n("[locmessage!t]")
    m "Wait..."
    m "I thought you said you 'just study together'."
    m "You didn't tell me you were friends."
    $ lying_about_relationship = True
    menu:
        "Oops, I meant he's just a studymate.":
            $ message = "Oops, I meant he's just a studymate."
        "Well, he can also be my friend...":
            $ message = "Well, he can also be my friend..."
        "No, I always said we were friends.":
            $ message = "No, I always said we were friends."
    return

label buddy_1_point_5:

    m "Just... don't hang around him too much."
    m "People might get the wrong idea."

    menu:
        "Oh. No, yeah, we're just friends.":
            $ message = "Oh. No, yeah, we're just friends."
            if (relationship == "study" and not lying_about_relationship):
                call buddy_caught_lying_1(message)
                jump buddy_2
            else:
                jump buddy_2
        "The wrong idea might be the right idea.":
            $ message = "The wrong idea might be the right idea."
            jump buddy_4
        "What do you mean by... wrong idea?":
            $ message = "What do you mean by... wrong idea?"
            jump buddy_3

label buddy_2:
    $ n("[message!t]")
    m "Okay."
    if (lying_about_relationship):
        m "Just don't lie to me."
        n "I won't."
        m ". . ."
        m "But... about you hanging out with Jack."
    m "It's just that some people might assume things, since..."
    m "You know... he looks like..."
    m "A gay?"
    jump buddy_choice

label buddy_3:
    $ n("[message!t]")
    m "Just between mother and son, I think he might be... you know..."
    n "No, what?"
    m "A gay!"
    m "He looks and talks like a gay."
    jump buddy_choice

label buddy_4:
    $ n("[message!t]")
    m "Oh, that's like a zen thing, right?"
    n "Um."
    m "Zen is also about nature, and your classmate Jack, he..."
    m "...you know, doesn't seem natural?"
    menu:
        "You think he's gay.":
            n "You think he's gay."
            m "Yes!"
            m "You suspect it, too!"
            jump buddy_choice
        "Don't say that about my friend!":
            $ message = "Don't say that about my friend!"

            if (relationship == "study" and not lying_about_relationship):
                call buddy_caught_lying_1(message)

                $ n("[message!t]")
                m "Okay."
                m "Just don't lie to me."
                n "I won't."
                m ". . ."

                m "But yes, even you agree that it's bad to be seen as 'not natural'."
                n "I never said--"
                m "And I'm just looking out for you! Because he acts like, you know..."
                m "A gay!"
                jump buddy_choice

            else:

                $ n("[message!t]")
                m "I'm just being honest."
                m "But yes, even you agree that it's bad to be seen as 'not natural'."
                n "I never said--"
                m "And I'm just looking out for you! Because he acts like, you know..."
                m "A gay!"
                jump buddy_choice

        "What do you mean, he's not natural?":
            $ message = "What do you mean, he's not natural?"
            jump buddy_3

label buddy_choice:
    if (relationship == "friend"):
        m "And since you say he's a 'good pal'..."
        m "People might think you're a gay like him, too."
    if (relationship == "best friend"):
        m "And since you say he's your BEST friend..."
        m "People might think you're a gay like him, too."
    menu:
        "Ha, he sure acts gay. Luckily, he's not.":
            n "Ha, he sure acts gay. Luckily, he's not."
            m "See? You also think there's something not right about it."
            n "...sure."
            jump buddy_aftermath
        "What's wrong with being gay?!":
            n "What's wrong with being gay?!"
            m "Nothing! Nothing."
            jump buddy_aftermath
        "Maybe... my friend might be gay.":
            $ message = "Maybe... my friend might be gay."

            if (relationship == "study" and not lying_about_relationship):
                call buddy_caught_lying_1(message)
                m "Okay."
                m "Just don't lie to me."
                n "I won't."
                m ". . ."
                jump buddy_aftermath
            else:
                $ n("[message!t]")
                jump buddy_aftermath


label buddy_aftermath:

    m "Don't get me wrong."
    m "I'm not saying those kind of people are bad!"
    m "I just think... you should be careful around one of them."
    m "Jack might, you know, try to recruit you."

    show clockn 1910
    show dinner_nicky_defiant as nicky

    menu:
        "what.":
            $ message = "what."
        "whaaat.":
            $ message = "whaaat."
        "whaaaaaaaaaaaaaaat.":
            $ message = "whaaaaaaaaaaaaaaat."

label buddy_aftermath_2:

    $ n("[message!t]")

    n "How do you even..."
    n "Ugh, nevermind."
    m "Nick, I'm sorry you find me annoying."
    n "No, mom, stop doing th--"
    m "Let's go back to talking about your grades."
    m "Now, what did you say you were studying tomorrow?"

    show dinner_nicky_sit as nicky
    n ". . ."
    n "Errrmmmmm..."

    menu:
        "Compsci?":
            $ studying_subject_2 = message = _("Computer Science")
        "Chemistry?":
            $ studying_subject_2 = message = _("Chemistry")
        "Calculus?":
            $ studying_subject_2 = message = _("Calculus")


# //////////////////////////////////////////
# ////// 3) A POSSIBLE PRIVATE TUTOR ///////
# //////////////////////////////////////////

label grades_start:
    $ n("[message!t]")
    m ". . ."
    if (studying_subject != studying_subject_2):
        jump grades_start_1
    else:
        jump grades_start_2

label grades_start_1:
    m "You first told me it was [studying_subject]."
    m "Now you tell me it's [studying_subject_2]?"
    $ lying_about_studying = True
    n "Mom, I was just confus--"
    if (lying_about_hanging_out or lying_about_relationship):
        m "This is TWICE you've lied to me during this dinner."
        n "I didn't lie about--"
    m "Either way, your grades in both subjects are terrible."
    n ". . ."
    jump grades_explaining

label grades_start_2:
    m "You hesitated for a moment there."
    n "I was eating."
    m "Okay."
    if (lying_about_hanging_out):
        m "I wonder if you're studying with Jack at all, or just always hanging out."
        n "We study."
    m ". . ."
    m "Still, your grades in your [studying_subject_2] class are terrible."
    n ". . ."
    jump grades_explaining

label grades_explaining:
    jump start_dinner_3
