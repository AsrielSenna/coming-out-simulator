# Plot points:
# Trying to stay overnight.
# Reveal - hippie parents, reading poetry, ...(?)
# Threats -- date your tutor, changing school(?)
# He's distracting you. Movie & Hippies.
# Oh my god, you've been reading my texts!...

label start_dinner_3:

    n "Mom."

    menu:
        "That's why I'm studying more with Jack.":
            n "That's why I'm studying more with Jack."
        "Look, I'm trying. I really am.":
            n "Look, I'm trying. I really am."
        "My grades are fine.":
            n "My grades are fine."

label tutor:

    m "I'm worried for you. Jack's not a good influence."

    if (hippies):
        m "I think his parents might even be drug addicts..."
        n "What makes you say th--"
    elif (im_a_poet):
        m "All he does is do poetry."
        n "What makes you say th--"

    m "I'm getting you a home tutor."
    n "...what?"

    if (studying_subject != studying_subject_2):
        m "She'll be tutoring you in [studying_subject] and [studying_subject_2]."
    else:
        m "She'll be tutoring you in [studying_subject]."

    m "Her name is Claire. She's smart, pretty, and Caucasian. She's your age, too."

    menu:
        "Are you trying to stop me from seeing Jack?":
            $ message = "Are you trying to stop me from seeing Jack?"
            jump tutor_seeing
        "Are you trying to matchmake me with her?":
            $ message = "Are you trying to matchmake me with her?"
            jump tutor_matchmake
        "Can we talk about tutors another time?":
            $ message = "Can we talk about tutors another time?"
            jump tutor_forget

label tutor_seeing:
    $ n("[message!t]")
    m "I'm sorry, {i}seeing{/i} Jack?"
    m "Be careful how you say that. You make it sound like..."

    menu:
        "Like we're dating? Yeah. We are.":
            n "Like we're dating? Yeah. We are."
            m ". . ."
            n ". . ."
            n "...Hello?"
            m ". . ."
            n "Anyone there?"
            m ". . ."
            jump threat_school
        "I just meant meeting Jack.":
            n "I just meant meeting Jack."
            m "Okay. Just being clear about some things."
            n "Yeah."
            m ". . ."
            m "Claire's really cute."
            n "Sure."
            m "She has perky breasts."
            jump threat_tutor
        "We're. Not. Boyfriends.":
            n "We're. Not. Boyfriends."
            m ". . ."
            m "Okay."
            m "I never said you were, but... okay."
            n "We're friends."

            if (relationship == "friend"):
                m "\"Good pals\"..."
            if (relationship == "best friend"):
                m "\"BEST friends\"..."

            jump threat_tutor

label tutor_matchmake:
    $ n("[message!t]")
    m "Well, if that's what you want, I could!"
    n "nooooo."
    m "Don't be shy! You're growing up to be a man."
    m "And you're going to give me lots of grandkids."

    menu:
        "Stop it! I haven't even met Claire yet!":
            n "Stop it! I haven't even met Claire yet!"
            m "Yet!"
            m "She's coming over tomorrow!"
            n "What? But I promised Jack--"
            m "I ironed your best clothes. You'll make a good first impression."
            jump threat_tutor
        "The odds of that are 50-50, coz I'm bi.":

            $ admit_bisexuality = True

            n "The odds of that are 50-50, coz I'm bi."
            m "Um. Bi?..."

            show dinner_nicky_defiant as nicky

            n "Yes. As in BISEXUAL."
            n "As in I AM SEXUALLY ATTRACTED TO BOTH MEN AND WOMEN."
            m ". . ."
            n ". . ."
            jump threat_school
        "No. I don't ever want to have kids.":
            n "No. I don't ever want to have kids."
            m "You'll change your mind when you grow up."
            m "Raising a child is wonderful. Your children will look up to you!"
            n "...of course, you narcissist."
            m "Excuse me?"
            n "Nothing."
            m ". . ."
            jump threat_tutor

label tutor_forget:
    $ n("[message!t]")
    m "No, because I've already scheduled Claire to come over tomorrow."
    n "What?!"
    n "No. I promised to study with Jack tomorrow."
    m ". . ."
    m "How long did you want to stay over at his place?"

    menu:
        "Overnight.":
            n "Overnight."
            m ". . ."
            n ". . ."
            n "...Hello?"
            n "It's not weird. Friends have sleepovers all the time."
            m ". . ."
            jump threat_school
        "Just the afternoon.":
            n "Just the afternoon."
            if (lying_about_hanging_out):
                m "I knew it. I caught your lie earlier."
                n "Huh?"
            else:
                m "...I knew it."
            m "You're just hanging out with him."
            jump Threat_tutor
        "Maybe an hour or so.":
            n "Maybe an hour or so."
            m "That's not enough to really get studying done."
            if (lying_about_hanging_out):
                m "I knew it. I caught your lie earlier."
                n "Huh?"
            m "You're just hanging out with him."
            jump threat_tutor

label threat_tutor:

    show dinner_nicky_defiant as nicky

    n ". . ."
    m "Claire will be tutoring you every day after school, starting tomorrow."

    menu:
        "Every day?! What about my friends?!":
            n "Every day?! What about my friends?!"
            m "Sweetie, I'm your friend!"
            n ". . ."
            m "Also Claire can be your friend. Maybe more than friends."
            n ". . ."
            n "Are we done?"
            m "Just... one more thing."
        "Okay, but my weekends are free, right?":
            n "Okay, but my weekends are free, right?"
            m "Yes."
            n "Okay. Good that this is all settled now."
            m "...Yes."
            n ". . ."
            m "Just... one more thing."
        "What if I just DON'T study with Claire?":
            n "What if I just DON'T study with Claire?"
            m "Well, if you also want to hang out with her, that's good too."
            m "Anything to make you more manly."
            n "ugh."
            m "Oh."
            m "One more thing."
    jump plot_twist

label threat_school:

    $ changing_schools = True

    m "You're changing schools."

    show dinner_nicky_outrage as nicky

    n "WHAT?!"
    m "I think it's not just Jack, it's the entire school that's a bad influence on you."
    n "ARE YOU SERIOUS."
    m "The whole Canadian culture is making you confused about who you are."

    show dinner_nicky_defiant as nicky

    menu:
        "No, it's YOUR Asian culture that's backwards!":
            n "No, it's YOUR Asian culture that's backwards!"
            m "Don't be so rude!"
            m "It's YOUR culture, too!"
            n ". . ."
        "You can't do this to your CHILD!":
            n "You can't do this to your CHILD!"
            m "Don't be so rude!"
            m "I'm your MOTHER, it's my right to do whatever I want with you!"
            n ". . ."
        "Whatever, ALL schools have queer people.":
            n "Whatever, ALL schools have queer people."
            m "Don't be so rude!"
            m "And watch it, I could change my mind and start homeschooling you."
            n ". . ."
    jump plot_twist

label plot_twist:

    m "Yesterday, when you were supposedly studying with Jack?"
    m "I know you secretly went off to watch a movie."

    show dinner_nicky_sit as nicky
    n ". . ."

    show clockn 1920

    menu:
        "Oh my god. You read my texts.":
            n "Oh my god. You read my texts."
            m "Yes. See how smart you can be when you're not with Jack?"
        "No, we didn't. We studied.":
            n "No, we didn't. We studied."
            m "You are a very stubborn boy."
            m "I read your text messages."
        "What makes you think that?":
            n "What makes you think that?"
            m "Because I read your text messages."
    jump plot_twist_2

label plot_twist_2:

    n ". . ."
    m "Before dinner. I was in your room."

    # Dinner_1
    m "You yelled out '[what_you_called_out]' from downstairs, while I unlocked your phone..."
    m "And read what you and Jack have been sending to each other."
    m "I'm your mother. I have the right."

    n ". . ."

    if (im_a_poet or hippies):
        if (im_a_poet):
            m "Weird poetry?"
        if (hippies):
            m "Talking about smoking marijuana?"
        m "Helping you lie to your own mother?"
        m "What else have you been doing behind my back?"

    menu:
        "This has to be a bad dream.":
            n "This has to be a bad dream."
            m "Like that 'Deception' movie?"
            n "It's... it's 'Inception'."
            m "Don't talk back to me."
        "I'm sorry. I'm so sorry.":
            n "I'm sorry. I'm so sorry."
            m "I forgive you."
            m "You're my child, of course I forgive you."
        "I hate you.":
            n "I hate you."
            m "That's okay."
            m "I still love you, Nick."
    jump plot_twist_3

label plot_twist_3:
    jump start_dinner_4
