init python:
    def textbub(color, lign=.0):
        return Transform(Fixed(Frame("gui/nvl_entry.png", 5, 5),
                               Transform(Solid("#fff", xysize=(14, 14)), rotate=45, xcenter=lign, yalign=.5),
                               fit_first=True),
                         matrixcolor=TintMatrix(color))

    def beep(event, soundname, interact=True, **kwargs):
        if event == "begin" and not renpy.in_rollback() and not renpy.is_skipping():
            renpy.play(soundname, channel="voice")

    beep = renpy.curry(beep)

# character.js
define N = Character(kind=nvl_narrator, window_xalign=.0, what_text_align=.0, window_background=textbub("#fff"), what_color="#000", callback=beep(soundname="audio/text_mid.mp3"))
define p = Character(kind=nvl_narrator, window_xalign=1., what_text_align=1., window_background=textbub("#333", lign=1.), what_color="#bbb", callback=beep(soundname="audio/text_low.mp3"))
define n = Character(kind=nvl_narrator, window_xalign=1., what_text_align=1., window_background=textbub("#79b8fe", lign=1.), callback=beep(soundname="audio/text_mid.mp3"))
define m = Character(kind=nvl_narrator, window_xalign=.0, what_text_align=.0, window_background=textbub("#fff"), callback=beep(soundname="audio/text_high.mp3"))
define f = Character(kind=nvl_narrator, window_xalign=.0, what_text_align=.0, window_background=textbub("#333"), what_color="#bbb", callback=beep(soundname="audio/text_low.mp3"))
define j = Character(kind=nvl_narrator, window_xalign=.0, what_text_align=.0, window_background=textbub("#fff"), callback=beep(soundname="audio/text_high.mp3"))

define config.default_transform = Transform(yoffset=-40) # Transform(yanchor=1., ypos=480) # reset
# I have no idea why everything is offset by 40 pixels in the base game

# nvl-related config
define narrator = nvl_narrator
define menu = nvl_menu

# f(l)ags
default message = None

default asked_about = None
default asked_credits = None
default main_menu_convo_1 = None
default main_menu_convo_2 = None

default inception_answer = None
default im_a_poet = None
default hippies = None
default sadsack = None
default coming_out_readiness = None

default what_you_called_out = None
default waiting_action = None
default studying_subject = None
default relationship = None
default lying_about_hanging_out = None
default lying_about_relationship = None
default studying_subject_2 = None
default lying_about_studying = None
default admit_bisexuality = None
default changing_schools = None
default crying = None
default what_are_you = None
default top_or_bottom = None
default promise_silence = None
default grounded = None
default tried_talking_about_it = None
default father_oblivious = None
default punched = None

default told_jack = None
default blame = None
default breaking_up_soon = None

# default coming_out_stories_left = 3
# default order_of_stories
# default told_story_lie
# default told_story_truth
# default told_story_half_truth
default stories_said = () # merging those redundant variables
default outro_convo_lie = None
default outro_convo_truth = None
default outro_convo_half_truth = None

label main_menu:
    return

# The game starts here

# menu.js
label start:
    scene black at reset
    show coffeehouse as background
    show cup steam
    show coffee_nicky_still as nicky
    play music coffeehouse volume .7

    nvl clear
    N "{b}COMING OUT SIMULATOR {s}2014{/s} 2021{/b}"
    N "A half-true game about half-truths."
    N "Hey there, player. Welcome to this game, I guess."
    N "What would you like to do now?"

    menu:
        "Let's play this thing!":
            $ message = "Let's play this thing!"
            jump play
        "Who are you? (Credits)":
            $ message = _("Who are you?")
            jump credits
        "Hm, tell me more. (About This Game)":
            $ message = _("Hm, tell me more.")
            jump about
    return

label sipcoffee(message):
    if message is not None:
        show coffee_nicky_drink as nicky
        hide cup
        play sound coffee_sip
        $ p("[message!t]")
        show coffee_nicky_still as nicky
        show cup steam behind nicky
    return

label play:
    call sipcoffee(message)

    # Asked neither
    if (not asked_about and not asked_credits):
        N "Jumping right into it! Great!"
        N "No messing around with reading the Credits or the About This Game sections or--"
        p "Shush."
        N "Fine, fine."
    # Asked both
    elif (asked_about and asked_credits):
        p ". . ."
        p "Why did you make that a clickable option, when it was the only option left."
        N "NO IDEA"
    # Asked either
    elif (asked_about or asked_credits):
        N "Yes, let's!"

    N "Let's travel back four years ago, to 2010..."
    p "That was FOUR years ago?!"
    N "...to the evening that changed my life forever."

    N "Tell me, dear player, how do you think this all ends?"

    menu:
        "With flowers and rainbows and gay unicorns?":
            $ main_menu_convo_1 = 1

            p "With flowers and rainbows and gay unicorns?"
            N "Yes. That is exactly how this game ends."
            p "Really?"
            N "No."

        "Apparently, with you redditing at Starbucks.":
            $ main_menu_convo_1 = 2

            p "Apparently, with you redditing at Starbucks."
            N "Hey, I'm coding on this laptop. Turning my coming-of-age story into the game you're playing right now."
            p "Naw, you're probably procrastinating."
            N "Look who's talking."
            p "Touché, douché."
            N "Anyway..."

        "IT ALL ENDS IN BLOOD":
            $ main_menu_convo_1 = 3

            p "IT ALL ENDS IN BLOOD"
            N "Uh, compared to that, I guess my story isn't that tragic."
            N "Although that's kind of a glass one-hundredths-full interpretation."
            p "blooooood."
            N "Anyway..."

label play_2:
    if not asked_about:
        N "If you didn't skip the About This Game section, you'd know this is a very personal story."
        p "Shush."

    N "This game includes dialogue that I, my parents, and my ex-boyfriend actually said."
    N "As well as all the things we could have, should have, and never would have said."
    N "It doesn't matter which is which."
    N "Not anymore."

    menu:
        "How can I win a game with no right answers?":
            $ main_menu_convo_2 = 2

            p "How can I win a game with no right answers?"
            N "Exactly."
            p ". . ."

        "You're a bit of a downer, aren't you?":
            $ main_menu_convo_2 = 1

            p "You're a bit of a downer, aren't you?"
            N "LIFE is a bit of a downer."
            p "So that's a yes."

        "This 'true' game is full of lies?":
            $ main_menu_convo_2 = 3

            p "This 'true' game is full of lies?"
            N "Even if the dialogue was 100\% accurate, it'd still be 100\% lies."
            p ". . ."

label play_3:
    N "You'll be playing as me, circa 2010."
    if (not asked_credits):
        N "Because you skipped the Credits, my (not-yet-legal) name is Nicky Case. Just so you know."
        p "Shush."

    if main_menu_convo_1 == 1:
        N "This game doesn't end with gay unicorns. {nw}"
    elif main_menu_convo_1 == 2:
        N "This game is a coming-out, a coming-of-age, a coming-to-terms. {nw}"
    elif main_menu_convo_1 == 3:
        N "This game ends not in blood, but in tears. {nw}"
    if main_menu_convo_2 == 1:
        extend "Sorry for being a bit of a downer."
    elif main_menu_convo_2 == 2:
        extend "And there are no right answers."
    elif main_menu_convo_2 == 3:
        extend "And it's full of lies."

    play audio coffee_sip
    show coffee_nicky_drink as nicky
    hide cup

    p "Hey, I just said that!"

    # HACK - Just clear dialogue & stuff.
    pause 1
    nvl clear

    pause .5
    show coffee_nicky_throw as nicky
    play audio coffee_throw

    pause 1
    show coffee_nicky_still_2 as nicky
    pause .5

    N "When you play..."
    N "Choose your words wisely."
    N "Every character will remember everything you say. Or don't say."
    p "Yeah. You even brought up my choices in this MAIN MENU."
    N "Exactly."

    N ". . ."
    N "Some things are hard not to remember."

    nvl clear
    scene black at reset
    jump start_jack_1

label credits:
    $ asked_credits = True

    if (asked_about):
        call sipcoffee(message)
    else:
        call sipcoffee("Who are you?")

    N "Ah, how rude of me! Let me introduce myself."
    N "Hi, I'm Nicky Case."
    N "That's not my legal name, it's just my REAL name."

    p "That's totes weird, dude."
    if (asked_about):
        p "And like you just told me, this is your personal story?"
    else:
        p "And you made this game?"

    N "Yep, I am the sole writer / programmer / artist of Coming Out Simulator 2014."

    if (asked_about):
        p "All of this yourself?"
        p "I said it before and I'll say it again..."
        p "Of course. You narcissist."
        N "Well it's not ALL me."
        N "The sounds & audio are from various public domain sources."
    else:
        N "The sounds & audio, though, are from various public domain sources."

    N "But although it's mostly just me behind this game..."
    N "...there's a lot of people behind this game's story."

    if (asked_about):
        menu:
            "Speaking of which, let's play that! Now!":
                $ message = "Speaking of which, let's play that! Now!"
                jump play
    else:
        menu:
            "Speaking of that, can we play it now?":
                $ message = "Speaking of that, can we play it now?"
                jump play
            "Why'd you make this? (About This Game)":
                $ message = _("Why'd you make this?")
                jump about

label about:

    $ asked_about = True

    call sipcoffee(message)

    if (asked_credits):
        N "I wanted to tell my story."
    else:
        N "This game..."
        N "...more like a conversation simulator, really..."
        N "...is a very personal story."

    p "Of course. You narcissist."
    N "Ha, of course."

    if (asked_credits):
        p "Actually no, a narcissist would use their real name."
        N "I told you, it IS my real na--"
        p "Aight, aight. Weirdo."

    N "I made this game for the #Nar8 Game Jam. Gave me an excuse. And a deadline!"
    p "You procrastinated until the last day to enter, didn't you."
    N "Yes."
    N "Also! This game is uncopyrighted. Dedicated to the public domain."
    N "I'm as open with my source code as I am with my sexuality."

    p "Ugh, that's a terrible pun."
    N "Howzabout a 'Fork Me' programming pun?"
    p "noooooo."

    if (asked_credits):
        menu:
            "Let's just play this game already.":
                $ message = "Let's just play this game already."
                jump play
    else:
        menu:
            "Bad puns aside, can we play now?":
                $ message = "Bad puns aside, can we play now?"
                jump play
            "So who ARE you? (Credits)":
                $ message = _("So who ARE you?")
                jump credits
