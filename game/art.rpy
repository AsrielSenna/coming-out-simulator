transform sprite(width, height, fps=2):
    crop_relative True
    size (width, height)
    block:
        crop (.0, .0, .25, 1.)
        pause 1./fps
        crop (.25, .0, .25, 1.)
        pause 1./fps
        crop (.5, .0, .25, 1.)
        pause 1./fps
        crop (.75, .0, .25, 1.)
        pause 1./fps
        repeat

# ////////// COFFEE /////////////

image background coffeehouse = "backgrounds/coffeehouse.png"
image background coffeehouse_2 = "backgrounds/coffeehouse_2.png"

image coffee_nicky_still = "stills/coffee_nicky_still.png"
image coffee_nicky_still_2 = "stills/coffee_nicky_still_2.png"
image coffee_nicky_drink = "stills/coffee_nicky_drink.png"
image coffee_nicky_throw = "stills/coffee_nicky_throw.png"

image coffee_nicky_packup_1 = "stills/coffee_nicky_packup_1.png"
image coffee_nicky_packup_2 = "stills/coffee_nicky_packup_2.png"
image coffee_nicky_packup_3 = "stills/coffee_nicky_packup_3.png"
image coffee_nicky_packup_4 = "stills/coffee_nicky_packup_4.png"

image coffee_nicky_date_1 = "stills/coffee_nicky_date_1.png"
image coffee_nicky_date_2 = "stills/coffee_nicky_date_2.png"
image coffee_nicky_date_3 = "stills/coffee_nicky_date_3.png"
image coffee_nicky_date_4 = "stills/coffee_nicky_date_4.png"

image cup_steam = sprite(width=80, height=80, fps=2)("sprites/cup_steam.png")


# ////////// BEDROOM /////////////

image background bedroom = "backgrounds/bedroom.png"
image background bedroom_2 = "backgrounds/bedroom_2.png"

image bedroom_us_1 = "stills/bedroom_us_1.png"
image bedroom_us_2 = "stills/bedroom_us_2.png"
image bedroom_punch = Transform("stills/bedroom_punch.png", pos=(256, 404))

image bedroom_light_1 = sprite(width=362, height=362, fps=2)("sprites/bedroom_light_1.png")


image bedroom_light_2 = sprite(width=362, height=362, fps=2)("sprites/bedroom_light_2.png")


# ////////// DINNER /////////////

image background dinner = "backgrounds/dinner.png"

image dinner table = Transform("stills/dinner_table.png", ypos=420)
image dinner table_2 = Transform("stills/dinner_table_2.png", ypos=420)

image dinner_nicky_sit = Transform("stills/dinner_nicky_sit.png", ypos=300)
image dinner_nicky_defiant = Transform("stills/dinner_nicky_defiant.png", ypos=300)
image dinner_nicky_outrage = Transform("stills/dinner_nicky_outrage.png", ypos=300)
image dinner_nicky_punched = Transform("stills/dinner_nicky_punched.png", ypos=300)
image dinner_punch_arm = Transform("stills/dinner_punch_arm.png", ypos=300)

image mom stand = Transform("stills/mom_stand.png", ypos=300)
image mom sit = Transform("stills/mom_sit.png", ypos=300)
image mom cry = Transform("stills/mom_cry.png", ypos=300)
image mom vomit = Transform("stills/mom_vomit.png", ypos=300)

image dad serious = Transform("stills/dad_serious.png", ypos=300)
image dad threat = Transform("stills/dad_threat.png", ypos=300)

image clockn 1855 = Transform("stills/clock_1855.png", xpos=155+5, ypos=294+37)
image clockn 1900 = Transform("stills/clock_1900.png", xpos=155+5, ypos=294+37)
image clockn 1910 = Transform("stills/clock_1910.png", xpos=155+5, ypos=294+37)
image clockn 1920 = Transform("stills/clock_1920.png", xpos=155+5, ypos=294+37)
image clockn 1930 = Transform("stills/clock_1930.png", xpos=155+5, ypos=294+37)
image clockn 1940 = Transform("stills/clock_1940.png", xpos=155+5, ypos=294+37)
image clockn 1950 = Transform("stills/clock_1950.png", xpos=155+5, ypos=294+37)
image clockn 2000 = Transform("stills/clock_2000.png", xpos=155+5, ypos=294+37)

image clock_ticking = sprite(width=50, height=100, fps=2)("sprites/clock_ticking.png")

image clock_meowing = sprite(width=50, height=100, fps=10)("sprites/clock_meowing.png")


# simplicity additions
image cup steam:
    "cup_steam"
    pos (44, 359)

image bedroom light 1:
    "bedroom_light_1"
    ypos 159

image bedroom light 2:
    "bedroom_light_2"
    ypos 159

image clock ticking:
    "clock_ticking"
    pos (155, 294)

image clock meowing:
    "clock_meowing"
    pos (155, 294)
