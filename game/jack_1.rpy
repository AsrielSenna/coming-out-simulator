# PLOT BEATS:
# 1) In medias res talking about Inception
# 2) Thanks for movie, we still up to stay over tomorrow night?
# 3) You need to stop hiding... # Can't even CALL.
# Weave in previous bits of convo pieces.
# Also, FULL CIRCLE with the Inception!
# OKAY, TOO CONVOLUTED, CUT OUT THE DIFFERENT FAMILIES & TYPO parts.

label start_jack_1:

    # /////// SET UP SCENE ////////

    show bedroom as background
    show bedroom_us_1 as us
    show bedroom light 1

    play music bedroom_1

    # /////////////////////////////

    j "And when he simply announces,"
    j "'I bought the airline.'"
    j "That was positively priceless!"
    n "Is that what he said?"
    n "I missed out what everyone in the theater was laughing about."
    j "You either need subtitles, or to clean your ears more often."
    j "So how did you interpret the ending?"

    menu:
        "It was totally all a dream.":
            jump Inception_Dream
        "He's got to be back in the real world!":
            jump Inception_Awake
        "Doesn't matter. Cobbs just finally let go.":
            jump Inception_Neither

label Inception_Dream(message="It was totally all a dream."):

    $ inception_answer = "dream"

    $ n("[message!t]")
    j "So his entire redemption story was a lie?"
    n "A big fat lie."
    j "You're a bit of a downer, aren't you?"

    menu:
        "Yup, I'm just a sad sack of sadness.":
            $ message = "Yup, I'm just a sad sack of sadness."
            jump Sadsack
        "Sometimes... but not when I'm with you.":
            $ im_a_poet = True

            n "Sometimes... but not when I'm with you."
            j "Ah Nicky, you amateur poet."
            n "Get me some french breads and wine,"
            n "Coz that's got to be the cheesiest thing I've ever said."
            j "Apologize for nothing."
            n "Anywho..."
            jump thanks
        "I'm just a realist.":
            $ hippies = True

            n "I'm just a realist."
            j "You need more positive thinking in your life."
            n "And YOU need to stop being such a new-age hippie."
            n "Anywho..."
            jump thanks

label Inception_Awake(message="He's got to be back in the real world!"):

    $ inception_answer = "awake"
    $ im_a_poet = True

    $ n("[message!t]")
    n "Otherwise, the whole movie would've all just been a lie."
    n "What's the point of living a lie?"
    j "Ah Nicky, you amateur poet."
    j "I take it you liked the film?"

    menu:
        "Aw yiss. Yes I did.":
            n "Aw yiss. Yes I did."
        "Mehhh, it was a tad confusing at times.":
            n "Mehhh, it was a tad confusing at times."
            j "I believe that was the purpose."
            n "Mission accomplished, then."
            n "Anywho..."
        "BWOOOOOOOOOOONG":
            n "BWOOOOOOOOOOONG"
            j "I'll interpret that as a yes."
    jump thanks

label Inception_Neither(message="Doesn't matter. Cobbs just finally let go."):

    $ inception_answer = "neither"

    $ n("[message!t]")
    j "Oh?"
    n "He didn't even bother looking to see if the top fell!"
    n "Lies, truths, or half-truths... Cobbs no longer cares."
    n "He's finally happy, and that's all that matters."
    j "You either are being quite poetic, or quite depressing."

    menu:
        "I'm a poet, and I didn't even know it.":

            $ im_a_poet = True

            n "I'm a poet,"
            n "and I wasn't even aware of the fact."
            j "You're a lyrical miracle, the evidence is empircal."
            n "That's hysterical."
            n "Anywho..."
            jump thanks
        "Nah, I'm just a sad sack of sadness.":
            $ message = "Nah, I'm just a sad sack of sadness."
            jump Sadsack
        "Or both.":

            $ hippies = True
            $ im_a_poet = True

            n "Or both."
            n "POETRY IS PAIN. ART IS SUFFERING."
            j "You sound like my mother."
            n "Your parents are {i}such{/i} new-age hippies."
            n "Anywho..."
            jump thanks

label Sadsack:

    $ sadsack = True

    $ n("[message!t]")
    j "Aw, sorry to hear that."
    j "I hope our little date at the movies cheered you up?"
    n "Sure did!"
    jump thanks

label thanks:

    n "So yeah! Thanks for taking me out to watch Inception!"
    j "My pleasure, Nicky."
    j "You should parody Inception in that odd web game of yours!"
    n "Mmm, maybe maybe."
    n "Let's meet again tomorrow evening!"

    j "Although..."
    n "Hope I can convince the parents to let me out overnight."

    j "I wish you didn't tell your mom and dad we were just studying, when we were actually at the cinema."
    n "I'll pretend we'll be cramming for the midterms all nigh-- huh?"

    j "You can't keep hiding like this."
    n "Jack..."

    menu:
        "They can never, ever know.":
            $ coming_out_readiness="no"
            n "They can never, ever know."
            j "Really, never?"
        "I wish I could tell them, too.":
            $ coming_out_readiness="yes"
            n "I wish I could tell them, too."
        "I'm not ready to tell them yet.":
            $ coming_out_readiness="maybe"
            n "I'm not ready to tell them yet."
            j "I can help you be ready."

label hiding:

    j "Nicky, hiding like this is eating away at your soul."

    if (inception_answer == "awake"):
        j "Like you said, what's the point of living a lie?"
    if (inception_answer == "dream"):
        j "It's... how'd you put it... 'a big fat lie'?"

    if (sadsack):
        j "When you said just now you're a sadsack?"
        j "I know you weren't joking. Not really."

    n "Jack, come on."
    j "I came out to my parents last year."
    if (hippies):
        n "That's NOT a fair comparison."
        n "LIKE I SAID, you and your parents are a bunch of new-age hippies."
        n "When I'm at your place, I can't tell if all the smoke is incense or marijuana."
        j "Hey! We only smoke weed every other day!"
        n "Heh."
        j "The point is, my parents supported my coming out."
    else:
        j "And they were very supportive!"

    j "You're in Canada now. A lot of people here are LGBT friendly."
    j "How do you know your parents won't be supportive of you, too?"

    menu:
        "Asian parents are usually very homophobic.":
            $ message = "Asian parents are usually very homophobic."
            jump hiding_2
        "I don't know... I guess I haven't tried...":
            $ message = "I don't know... I guess I haven't tried..."
            jump hiding_2
        "They don't support anything but STUDYING.":
            $ message = "They don't support anything but STUDYING."
            jump hiding_2

label hiding_2:

    $ n("[message!t]")

    if (coming_out_readiness == "no"):
        n "Again... They can never, ever know."

    j "You have trust issues."
    j "You're even texting me instead of calling..."
    j "...because you think your parents might listen in."

    n "They would!"

    j "This mode of communication."
    j "It's imprecise, impersonal, impossible to truly connect."

    if (im_a_poet):
        n "Heh. You're an amateur poet like me, apparently."
    else:
        n "It's not too bad..."

    if (coming_out_readiness == "yes"):
        j "You yourself just said you wish you could tell them."
        j "Tell them."
    else:
        j "Nicky."
    j "Tell them about us. Tonight."

    menu:
        "Tonight?! Heck no.":
            $ message = "Tonight?! Heck no."
            jump hiding_3
        "Sigh... I'll try my best.":
            $ message = "Sigh... I'll try my best."
            jump hiding_3
        "I'll just carefully hint at it.":
            $ message = "I'll just carefully hint at it."
            jump hiding_3

label hiding_3:

    $ n("[message!t]")
    j ". . ."
    n "I don't want to freak them out too much."
    n "Still need to convince them to let me stay at your place tomorrow night."
    n "I'll tell 'em I'm studying with you again."
    j ". . ."
    n "It's dinnertime. I'm heading downstairs now."

    j "Hey... I agree."
    n "Huh?"
    j "With your thoughts on the movie ending, that is."
    if inception_answer == "dream":
        j "I think Cobbs was still dreaming, living a lie."
    elif inception_answer == "awake":
        j "I think Cobbs reconnected with his real family, in the real world."
    elif inception_answer == "neither":
        j "I think it doesn't matter, as long as Cobbs is happy."
    n "Oh."
    j "Okay."
    if (coming_out_readiness == "maybe"):
        j "Hope you changed your mind about being 'not ready to tell them yet'."
    j "Good luck. Text me in an hour."

    n "See ya."
    if (hippies or im_a_poet):
        if im_a_poet:
            n "You amateur poet."
        else:
            n "You new-age hippie."
    else:
        n "You goof."

    jump jack_1_end

label jack_1_end:
    nvl clear
    scene black at reset
    jump start_dinner_1
