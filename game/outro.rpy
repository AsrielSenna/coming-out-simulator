# Then we broke up soon/X...
# Three stories (Lie / Truth / Half-truth) ... one interaction with each.
# Did you skip or not? Tie that into the sections.
# Your final choice, a whaaaaaat.

label start_outro:

    # Just clear dialogue & stuff.
    nvl clear
    scene black at reset

    # /////// SET UP SCENE ////////

    show background coffeehouse_2
    show cup steam
    show coffee_nicky_still as nicky

    play music coffeehouse volume .7

    # ///////////////////////////////

    if (breaking_up_soon):
        N "And then we broke up three days later."
    else:
        N "And then we broke up three weeks later."

    # Weave - intro
    if (main_menu_convo_1 == 1):
        p ". . ."
        N "Told you this didn't end in gay unicorns."
    elif (main_menu_convo_1 == 3):
        p ". . ."
        N "Told you. Not blood, but tears."
    elif (main_menu_convo_2 == 1):
        p ". . ."
        N "You were right. I'm a bit of a downer."

    menu:
        "MY FEELS.":
            p "MY FEELS."
            N "Let the feels flow, my friend."
            jump closure
        "Aw, come on, that's cold dude.":
            p "Aw, come on, that's cold dude."
            N "I don't deny that."
            jump closure
        "Can't say I didn't see that coming...":
            p "Can't say I didn't see that coming..."
            N "Yeah... Jack and I saw it coming, too."
            jump closure

label closure:

    play sound coffee_sip
    show coffee_nicky_drink as nicky
    hide cup

    p "Ugh."
    p "I feel gross just using the same-coloured dialogue balloons as the Father character."

    show coffee_nicky_still as nicky
    show cup steam behind nicky

    N "Which reminds me. Many of the characters have been swapped around."
    N "All names have been changed, except mine."
    N "I left my little brother out entirely, because he's innocent."
    N "And I put my Father back in, even though he'd left the family long before 2010."

    if (main_menu_convo_2 == 3):
        N "Like you said, this 'true' game is full of lies."

    p "You could have at least given me a different colour."
    N "It's been four years since that night..."
    N "What do you think happened afterwards?"

    if (main_menu_convo_2 == 2):
        N "Don't worry. Like we said in the Main Menu, there are no right answers."

    $ stories_said = []

    menu:
        "Dude, I dunno, just freaking tell me.":
            p "Dude, I dunno, just freaking tell me."
            N "Alright, I will tell you what happened."
            N "...and what happened, and what happened."
            p "What."
            jump closure_story
        "Let me guess, It Gets Better™?":
            p "Let me guess, It Gets Better™?"
            N "Yes, actually! In all three versions of what happened."
            p "What."
            jump closure_story
        "Flowers and rainbows and gay unicorns?":
            p "Flowers and rainbows and gay unicorns?"
            N "Yes, actually! At least, in one of my three versions of what happened."
            p "Of course."
            jump closure_story

label closure_story:

    if (len(stories_said) == 0):
        N "Which post-coming-out story do you want to hear first?"
        N "Don't worry, you'll get to hear all three of them."
    elif (len(stories_said) == 1):
        N "Now, which version do you want to hear next?"
    elif (len(stories_said) == 2):
        N "Finally, let's hear the last story..."
    else:
        jump finale_1

    menu:
        "The Lie." if not 'lie' in stories_said:
            $ message = "The Lie."
            jump tell_me_a_lie
        "The Truth." if not 'truth' in stories_said:
            $ message = "The Truth."
            jump tell_me_a_truth
        "The Half-Truth." if not 'half-truth' in stories_said:
            $ message = "The Half-Truth."
            jump tell_me_a_half_truth

label is_last_story:
    if (len(stories_said) == 3):
        if (asked_about and asked_credits):
            p "Again, with the making the only option a clickable option..."
        else:
            p "Why did you make that a clickable option, when it was the only option left."
            N "No idea. Moving on."
    return


label tell_me_a_lie:

    $ stories_said.append("lie")

    play sound coffee_sip
    show coffee_nicky_drink as nicky
    hide cup
    $ p("[message!t]")
    show coffee_nicky_still as nicky
    show cup steam behind nicky

    N "Very well."
    call is_last_story

    N "I ran away from home, with nothing more than a luggage bag full of edible underwear."
    if (im_a_poet):
        N "I roamed the Great White North. Supporting myself by writing amateur poetry for strangers."
    else:
        N "I roamed the Great White North. Supporting myself by making not-fun web games."
    N "I ate flowers. Followed the rainbows. And befriended a homosexual unicorn."
    p ". . ."
    N "Eventually I made it to Alaska, where I met an adult bisexual couple named Bonnie & Clyde."
    N "Bonnie was a mid-30s cougar, and Clyde was an early-40s manther."

    # FAMILY WITH BENEFITS
    # Weave in -- top or bottom

    menu:
        "I guess edible undies are both food & clothing.":
            $ outro_convo_lie = 1
            p "I guess edible undies are both food & clothing."
            N "And thanks to my flexibility, the luggage bag doubles as housing!"
            jump tell_me_a_lie_2
        "This story is a fractal of fracked up.":
            $ outro_convo_lie = 2
            p "This story is a fractal of fracked up."
            N "MY STORY. MY RULES."
            jump tell_me_a_lie_2
        "...\"manther\".":
            $ outro_convo_lie = 3
            p "...\"manther\"."
            N "Also known as a faguar."
            jump tell_me_a_lie_2

label tell_me_a_lie_2:

    N "They took me in as their foster child, and I was their full-time boytoy."

    if (outro_convo_lie == 1):
        p "...Thanks again to your, uh, flexibility."

    if top_or_bottom == "top":
        N "As we know, I like having my partners be 'the woman' of a relationship."
    elif top_or_bottom == "bottom":
        N "As we know, I'm usually 'the woman' of a relationship."
    elif top_or_bottom == "versatile":
        N "As we know, I like taking turns at being 'the woman' of a relationship."

    N "They raised me, showed me love, and I grew up to be a productive member of society."

    if outro_convo_lie == 2:
        p "And when you zoom in on this fractal, there's MORE fracked-up-ness."
    elif outro_convo_lie == 3:
        p "...\"MANTHER\"."

    N "They were my new family."
    N "Family... with benefits."

    p ". . ."

    jump closure_story





label tell_me_a_truth:

    $ stories_said.append("truth")

    play sound coffee_sip
    show coffee_nicky_drink as nicky
    hide cup
    $ p("[message!t]")
    show coffee_nicky_still as nicky
    show cup steam behind nicky

    N "Here it goes."
    call is_last_story

    N "I took Jack's advice and parodied Inception in my 'odd web game', Reimagine :The Game:."
    if inception_answer == "awake":
        N "Didn't say that Cobbs was awake in the ending, though."
    elif inception_answer == "dream":
        N "Didn't say that the movie was all just a dream, though."
    elif inception_answer == "neither":
        N "Still think it doesn't matter if Cobbs was still dreaming."
    N "Reimagine :The Game: got internet-famous-ish! A good portfolio piece."
    N "A few months later, I landed an internship at Electronic Arts in the Bay Area. Far away from my family in Canada."

    menu:
        "Eww, Electronic Arts...?":
            $ outro_convo_truth = 3
            p "Eww, Electronic Arts...?"

            N "Yeah, I know, I know."
            N "I'm now repenting for my sins by making artsy-fartsy indie games like this one."
            p "Repent harder, dammit."
            jump tell_me_a_truth_2
        "And the Bay Area is very LGBT friendly.":
            $ outro_convo_truth = 2
            p "And the Bay Area is very LGBT friendly."

            N "That's why they call it the Gay Area!"
            p "Uh.. nobody calls it that."
            jump tell_me_a_truth_2
        "Oh, I love EA! They make The Sims, right?":
            $ outro_convo_truth = 1
            p "Oh, I love EA! They make The Sims, right?"

            N "Yup! I didn't work on those, though. Our team was making a web game version of--"
            N "[[LITERALLY CANNOT DISCLOSE]"
            p "Oh."
            jump tell_me_a_truth_2

label tell_me_a_truth_2:

    N "After EA, I went on to go indie."
    N "But I stayed in touch with friends at EA, and stayed in the Bay Area."

    N "My technical skills grew."
    N "My social skills grew."
    N "And here... I'm finally starting to figure out my identity."

    if outro_convo_truth == 1:
        p "Well, I'm looking forward to Literally Cannot Disclose: The Game."
    elif outro_convo_truth == 2:
        p "But seriously, no one calls it the Gay Area."
    elif outro_convo_truth == 3:
        p "But seriously, ew. Electronic Arts."

    jump closure_story





label tell_me_a_half_truth:
    $ stories_said.append("half-truth")

    play sound coffee_sip
    show coffee_nicky_drink as nicky
    hide cup
    $ p("[message!t]")
    show coffee_nicky_still as nicky
    show cup steam behind nicky

    N "As you wish."
    call is_last_story

    N "Claire, in an ironic twist of fate, was also bisexual."
    N "We told each other about it during a [studying_subject] study session."

    p "What a twist!"

    N "Claire was insecure about her sexual orientation, like me."
    N "We were both somewhat inexperienced. Claire's only been with women, and I've only been with Jack."

    # CLAIRE AND I HELPED EACH OTHER EXPLORE OURSELVES, LESS GUILT, MORE EXPERIENCE.
    # Weave in -- studying what

    menu:
        "A mirror version of you, but reversed...":
            $ outro_convo_half_truth = 1
            p "A mirror version of you, but reversed..."
            N "Well, uh, all mirror images are reversed."
            p "You know what I mean."
            N "But yeah, Claire and I shared our experiences with one another."
            jump tell_me_a_half_truth_2
        "So, you taught each other the other side?":
            $ outro_convo_half_truth = 3
            p "So, you taught each other the other side?"
            jump tell_me_a_half_truth_2
        "Did you end up having sexytimes together?":
            $ outro_convo_half_truth = 2
            p "Did you end up having sexytimes together?"
            N "No. She's like a sister to me. A sister I would not have sex with."
            p "You... did not need to clarify that."
            N "But yeah, Claire and I shared our experiences with one another."
            jump tell_me_a_half_truth_2

label tell_me_a_half_truth_2:

    N "And exchanged tips!"
    N "Like... do a 'come hither' motion with your fingers, or, rub the head against the roof of your mouth."
    p "T.M.I, dude..."

    if (changing_schools or not father_oblivious):
        N "I did move to her school, in the end."

    N "We were best friends. We still are! We've now both moved to the US, far away from our hateful families."
    N "Together, we helped each other overcome our insecurities, and discover who we were..."
    N "Proud bisexual sluts."

    p "What a touching story. I think."

    N "And of course, we wingman/wingwoman for each other."

    p ". . ."

    jump closure_story





label finale_1:

    N "And that's the last of the post-coming-out stories!"

    # HACK - Just clear dialogue & stuff.
    pause 1
    nvl clear

    hide cup
    show coffee_nicky_throw as nicky
    play sound coffee_throw

    pause 1
    show coffee_nicky_still_2 as nicky

    # //////////////////////////

    N "Dear player, I couldn't help but notice..."
    if (stories_said[0] == "truth"):
        N "You went straight for the Truth first."
    elif (stories_said[2] == "truth"):
        N "You saved the Truth for last."
    elif (stories_said[0] == "lie"):
        N "You wanted to hear the Lie first."
    else:
        N "You saved the Lie for last."
    N "What does that say about you?..."
    p ". . ."

    p "You know... usually when a game gives you multiple endings, they don't do them ALL AT ONCE."
    N "Hah! You thought these were ENDINGS?"

    menu:
        "Let me guess... This Is Just The Beginning?":
            p "Let me guess... This Is Just The Beginning?"
            N "This is just the begi-- oh. Okay, yeah."
            jump finale_2
        "Well yeah. This game's over, right?":
            p "Well yeah. This game's over, right?"
            N "True... but the story, which is my story, my life, continues."
            jump finale_2
        "oh god how long IS this damn game.":
            p "oh god how long IS this damn game."
            N "Don't worry. Your next choice is the very last one, I swear."
            jump finale_2

label finale_2:

    show coffee_nicky_packup_1 as nicky

    N ". . ."
    N "You know, if I could go back and relive all my other possible choices..."
    N "... which in a sense, I did, by writing this game..."
    N "... I wouldn't change a thing."

    show coffee_nicky_packup_2 as nicky

    # SERIOUSNESS.
    play sound laptop_shut
    play music bedroom_1 volume .4

    p "? ? ?"

    if (punched):
        N "My texts getting read. Being forced to change schools. Getting punched in the face."
    elif (father_oblivious == False):
        N "My texts getting read. Being forced to change schools. All the verbal abuse."
    elif (changing_schools):
        N "My texts getting read. Being forced to change schools. The attempted 'gay rehab' with Claire."
    else:
        N "My texts getting read. No more after-school hours to myself. The attempted 'gay rehab' with Claire."

    N "In a Stockholm Syndrome sort of sense... I'm grateful for it all."

    menu:
        "what.":
            p "what."
            jump finale_3
        "whaaat.":
            p "whaaat."
            jump finale_3
        "whaaaaaaaaaaaaaaat.":
            p "whaaaaaaaaaaaaaaat."
            jump finale_3

label finale_3:

    play sound laptop_pack
    show coffee_nicky_packup_3 as nicky

    N "Yes, really!"
    N "I wouldn't have been so motivated to forge my own life... if my previous life wasn't total utter shit."

    play sound laptop_pack_2
    show coffee_nicky_packup_4 as nicky

    N "Later in 2010, Dan Savage launched the It Gets Better™ campaign."
    N "My three stories... Lie, Truth, Half-Truth... they're all at least true about one thing."
    N "It does get better."

    p ". . ."

    N "And..."
    N "At the end..."
    N "Of this long, stupid, painful game..."
    N "Where I played against people who should have been on my side..."

    p ". . ."

    N "I won."
    N ". . ."
    N "I won."

    # HACK - Just clear dialogue & stuff.
    pause 1
    nvl clear

    # CUTSCENE -- MY NEW BOYFRIEND
    pause 1

    play sound laptop_pack
    show coffee_nicky_date_1 as nicky
    pause 1

    play sound step_2
    show coffee_nicky_date_2 as nicky
    pause 1

    play sound step_1
    show coffee_nicky_date_3 as nicky
    pause 1

    play sound step_2 volume .75
    show coffee_nicky_date_4 as nicky
    pause 1

    play sound step_1 volume .5
    hide nicky
    pause 1

    play sound step_2 volume .25
    menu:
        "REPLAY?":
            p "REPLAY?"
            jump finale_4

label finale_4:

    N "Real life has no replays."

    pause .8
    $ ShowMenu("about")()
label finale_5:
    $ MainMenu()()
    pause
    jump finale_5
