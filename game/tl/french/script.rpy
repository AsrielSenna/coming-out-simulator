﻿# game/script.rpy:89
translate french start_64c18a14:

    # nvl clear
    # N "{b}COMING OUT SIMULATOR {s}2014{/s} 2021{/b}"
    nvl clear
    N "{b}COMING OUT SIMULATOR {s}2014{/s} 2021{/b}"

# game/script.rpy:91
translate french start_547b924d:

    # N "A half-true game about half-truths."
    N "Un jeu à moitié vrai sur des demi-vérités."

# game/script.rpy:92
translate french start_b75534fb:

    # N "Hey there, player. Welcome to this game, I guess."
    N "Salut à toi, nouveau joueur. Bienvenue dans le jeu !"

# game/script.rpy:93
translate french start_775807dd:

    # N "What would you like to do now?"
    N "Qu'est-ce que tu veux faire, maintenant ?"

# game/script.rpy:122
translate french play_ee15b6a1:

    # N "Jumping right into it! Great!"
    N "On y va tout de suite ! Super !"

# game/script.rpy:123
translate french play_35b21494:

    # N "No messing around with reading the Credits or the About This Game sections or--"
    N "Pas le temps de lire les Crédits ou la section À Propos ou--"

# game/script.rpy:124
translate french play_37d1db02:

    # p "Shush."
    p "Chut."

# game/script.rpy:125
translate french play_a6ac1297:

    # N "Fine, fine."
    N "Très bien, très bien."

# game/script.rpy:128
translate french play_875ce35a:

    # p ". . ."
    p ". . ."

# game/script.rpy:129
translate french play_6be3e892:

    # p "Why did you make that a clickable option, when it was the only option left."
    p "Pourquoi en avoir fait une option cliquable, quand c'était la seule qui reste."

# game/script.rpy:130
translate french play_46ececcc:

    # N "NO IDEA"
    N "AUCUNE IDÉE"

# game/script.rpy:133
translate french play_507084f6:

    # N "Yes, let's!"
    N "Oui, on y va !"

# game/script.rpy:135
translate french play_ef5e696e:

    # N "Let's travel back four years ago, to 2010..."
    N "Revenons il y a quatre ans, en 2010..."

# game/script.rpy:136
translate french play_248ed642:

    # p "That was FOUR years ago?!"
    p "C'était il y a QUATRE ans ?!"

# game/script.rpy:137
translate french play_b8a6eaa8:

    # N "...to the evening that changed my life forever."
    N "...jusqu'au soir qui a changé ma vie pour toujours."

# game/script.rpy:139
translate french play_e1fc347b:

    # N "Tell me, dear player, how do you think this all ends?"
    N "Dis-moi, cher joueur, comment penses-tu que tout ça va finir ?"

# game/script.rpy:145
translate french play_712f2309:

    # p "With flowers and rainbows and gay unicorns?"
    p "Avec des fleurs, des arc-en-ciels et des licornes gayes ?"

# game/script.rpy:146
translate french play_f6ff4c83:

    # N "Yes. That is exactly how this game ends."
    N "Oui. C'est exactement comme ça que le jeu se termine."

# game/script.rpy:147
translate french play_43cc34db:

    # p "Really?"
    p "Vraiment ?"

# game/script.rpy:148
translate french play_5e36508b:

    # N "No."
    N "Non."

# game/script.rpy:153
translate french play_1074f1a9:

    # p "Apparently, with you redditing at Starbucks."
    p "Apparemment, avec toi, sur Reddit, au Starbucks."

# game/script.rpy:154
translate french play_3e4eb8b7:

    # N "Hey, I'm coding on this laptop. Turning my coming-of-age story into the game you're playing right now."
    N "Hey, je code sur cet ordi. Je transforme l'histoire de mon adolescence pour faire le jeu auquel t'es en train de jouer."

# game/script.rpy:155
translate french play_91725e2e:

    # p "Naw, you're probably procrastinating."
    p "Nan, t'es sûrement en train de procrastiner."

# game/script.rpy:156
translate french play_28b34e2d:

    # N "Look who's talking."
    N "Regarde qui parle."

# game/script.rpy:157
translate french play_b21eabc3:

    # p "Touché, douché."
    p "Touché, douché."

# game/script.rpy:158
translate french play_f1ea3804:

    # N "Anyway..."
    N "Enfin bref..."

# game/script.rpy:163
translate french play_247981c9:

    # p "IT ALL ENDS IN BLOOD"
    p "TOUT ÇA FINIT DANS LE SANG"

# game/script.rpy:164
translate french play_3f5478ec:

    # N "Uh, compared to that, I guess my story isn't that tragic."
    N "Euh, comparé à ça, j'imagine que mon histoire n'est pas si tragique."

# game/script.rpy:165
translate french play_dc97c732:

    # N "Although that's kind of a glass one-hundredths-full interpretation."
    N "Même si c'est une interprétation au verre-au-centième-plein."

# game/script.rpy:166
translate french play_ba8a272f:

    # p "blooooood."
    p "Du saaaaaaang."

# game/script.rpy:167
translate french play_f1ea3804_1:

    # N "Anyway..."
    N "Enfin bref..."

# game/script.rpy:171
translate french play_2_2b9fe5f6:

    # N "If you didn't skip the About This Game section, you'd know this is a very personal story."
    N "Si tu n'avais pas passé la section À Propos, tu saurais que c'est une histoire très personnelle."

# game/script.rpy:172
translate french play_2_37d1db02:

    # p "Shush."
    p "Chut."

# game/script.rpy:174
translate french play_2_9ba9b32f:

    # N "This game includes dialogue that I, my parents, and my ex-boyfriend actually said."
    N "Ce jeu inclut des dialogues que moi, mes parents et mon ex-copain ont réellement dit."

# game/script.rpy:175
translate french play_2_bd35b396:

    # N "As well as all the things we could have, should have, and never would have said."
    N "Ainsi que tout ce que nous aurions pu, aurions dû, et n'aurions jamais dit."

# game/script.rpy:176
translate french play_2_bee21b4e:

    # N "It doesn't matter which is which."
    N "Peu importe lequel est lequel."

# game/script.rpy:177
translate french play_2_d3869453:

    # N "Not anymore."
    N "Ça n'a plus d'importance."

# game/script.rpy:183
translate french play_2_d592ff07:

    # p "How can I win a game with no right answers?"
    p "Comment je peux gagner à un jeu sans bonne réponse ?"

# game/script.rpy:184
translate french play_2_2eeb6693:

    # N "Exactly."
    N "Exactement."

# game/script.rpy:185
translate french play_2_875ce35a:

    # p ". . ."
    p ". . ."

# game/script.rpy:190
translate french play_2_e2940ec0:

    # p "You're a bit of a downer, aren't you?"
    p "T'es du genre pessimiste, non ?"

# game/script.rpy:191
translate french play_2_e0601fbb:

    # N "LIFE is a bit of a downer."
    N "LA VIE a de quoi rendre pessimiste."

# game/script.rpy:192
translate french play_2_ce04e9c0:

    # p "So that's a yes."
    p "C'est un oui, donc."

# game/script.rpy:197
translate french play_2_f50eb484:

    # p "This 'true' game is full of lies?"
    p "Ce jeu 'vrai' est plein de mensonges ?"

# game/script.rpy:198
translate french play_2_1c723297:

    # N "Even if the dialogue was 100%% accurate, it'd still be 100%% lies."
    N "Même si les dialogues étaient 100%% vrais, ce serait toujours 100%% de mensonges."

# game/script.rpy:199
translate french play_2_875ce35a_1:

    # p ". . ."
    p ". . ."

# game/script.rpy:202
translate french play_3_d1f562c8:

    # N "You'll be playing as me, circa 2010."
    N "Tu vas jouer mon rôle, vers 2010."

# game/script.rpy:204
translate french play_3_2ce1e2b9:

    # N "Because you skipped the Credits, my (not-yet-legal) name is Nicky Case. Just so you know."
    N "Comme tu as passé les Crédits, mon nom (pas encore officiel) c'est Nicky Case. Comme ça tu le sais."

# game/script.rpy:205
translate french play_3_37d1db02:

    # p "Shush."
    p "Chut."

# game/script.rpy:208
translate french play_3_213a7fcf:

    # N "This game doesn't end with gay unicorns. {nw}"
    N "Ce jeu ne se termine pas par des licornes gayes. {nw}"

# game/script.rpy:210
translate french play_3_14649a8f:

    # N "This game is a coming-out, a coming-of-age, a coming-to-terms. {nw}"
    N "Ce jeu est un coming-out, un passage à l'age adulte, un passage aux choses sérieuses. {nw}"

# game/script.rpy:212
translate french play_3_ce489ac1:

    # N "This game ends not in blood, but in tears. {nw}"
    N "Ce jeu ne se finit pas dans le sang, mais dans les larmes. {nw}"

# game/script.rpy:214
translate french play_3_69a4f92b:

    # extend "Sorry for being a bit of a downer."
    extend "Désolé d'être un peu pessimiste."

# game/script.rpy:216
translate french play_3_278303ec:

    # extend "And there are no right answers."
    extend "Et il n'y a pas de bonne réponse."

# game/script.rpy:218
translate french play_3_8372ccd6:

    # extend "And it's full of lies."
    extend "Et c'est plein de mensonges."

# game/script.rpy:213
translate french play_3_0e875a55:

    # p "Hey, I just said that!"
    p "Eh, mais c'est ce que je viens de dire !"

# game/script.rpy:217
translate french play_3_76b2fe88:

    # nvl clear
    nvl clear

# game/script.rpy:227
translate french play_3_abbdb3e5:

    # N "When you play..."
    N "Quand tu joues..."

# game/script.rpy:228
translate french play_3_3a926226:

    # N "Choose your words wisely."
    N "Choisis bien tes mots."

# game/script.rpy:229
translate french play_3_9b618320:

    # N "Every character will remember everything you say. Or don't say."
    N "Chaque personnage se souviendra de tout ce que tu dis. Ou ne dis pas."

# game/script.rpy:230
translate french play_3_34eb719d:

    # p "Yeah. You even brought up my choices in this MAIN MENU."
    p "Ouais. Tu t'es même souvenu de mes choix dans ce MENU."

# game/script.rpy:231
translate french play_3_2eeb6693:

    # N "Exactly."
    N "Exactement."

# game/script.rpy:233
translate french play_3_e3ba31fc:

    # N ". . ."
    N ". . ."

# game/script.rpy:234
translate french play_3_2dfc5d55:

    # N "Some things are hard not to remember."
    N "Certaines choses sont difficiles à oublier."

# game/script.rpy:236
translate french play_3_76b2fe88_1:

    # nvl clear
    nvl clear

# game/script.rpy:248
translate french credits_05323939:

    # N "Ah, how rude of me! Let me introduce myself."
    N "Ah, je n'ai aucune éducation ! Laisse-moi me présenter."

# game/script.rpy:249
translate french credits_3ff854d7:

    # N "Hi, I'm Nicky Case."
    N "Je suis Nicky Case."

# game/script.rpy:250
translate french credits_9a8423c6:

    # N "That's not my legal name, it's just my REAL name."
    N "Ce n'est pas mon nom légal, mais c'est mon VRAI nom."

# game/script.rpy:252
translate french credits_4a0019ee:

    # p "That's totes weird, dude."
    p "Trop bizarre, mec."

# game/script.rpy:254
translate french credits_78f65615:

    # p "And like you just told me, this is your personal story?"
    p "Et comme tu viens de le dire, c'est ton histoire personnelle ?"

# game/script.rpy:256
translate french credits_8030f1a1:

    # p "And you made this game?"
    p "Et tu as fait ce jeu ?"

# game/script.rpy:258
translate french credits_0696972f:

    # N "Yep, I am the sole writer / programmer / artist of Coming Out Simulator 2014."
    N "Yep, je suis le seul auteur / programmeur / artiste de Coming Out Simulator 2014."

# game/script.rpy:261
translate french credits_297aadf9:

    # p "All of this yourself?"
    p "Tout ça tout seul ?"

# game/script.rpy:262
translate french credits_f4291c53:

    # p "I said it before and I'll say it again..."
    p "Je l'ai déjà dit et je le redis..."

# game/script.rpy:263
translate french credits_776bebe8:

    # p "Of course. You narcissist."
    p "Bien sûr. Quel narcissique."

# game/script.rpy:264
translate french credits_c5242349:

    # N "Well it's not ALL me."
    N "Bon, ce n'est pas TOUT de moi."

# game/script.rpy:265
translate french credits_abdaced1:

    # N "The sounds & audio are from various public domain sources."
    N "Les sons et l'audio proviennent de diverses sources du domaine public."

# game/script.rpy:267
translate french credits_c478c821:

    # N "The sounds & audio, though, are from various public domain sources."
    N "Enfin, les sons et l'audio proviennent de diverses sources du domaine public."

# game/script.rpy:269
translate french credits_02db5868:

    # N "But although it's mostly just me behind this game..."
    N "Mais même si c'est surtout moi qui suis derrière ce jeu..."

# game/script.rpy:270
translate french credits_1ad23822:

    # N "...there's a lot of people behind this game's story."
    N "...il y a beaucoup de monde derrière l'histoire de ce jeu."

# game/script.rpy:293
translate french about_e73fd4a7:

    # N "I wanted to tell my story."
    N "Je voulais raconter mon histoire."

# game/script.rpy:295
translate french about_f10dc5f8:

    # N "This game..."
    N "Ce jeu..."

# game/script.rpy:296
translate french about_d8f4e8cf:

    # N "...more like a conversation simulator, really..."
    N "...Qui est un simulateur de conversation, en fait..."

# game/script.rpy:297
translate french about_66aaca74:

    # N "...is a very personal story."
    N "...est une histoire très personnelle."

# game/script.rpy:299
translate french about_776bebe8:

    # p "Of course. You narcissist."
    p "Bien sûr. Quel narcissique."

# game/script.rpy:300
translate french about_e69a085e:

    # N "Ha, of course."
    N "Ha, bien sûr."

# game/script.rpy:303
translate french about_21f34a30:

    # p "Actually no, a narcissist would use their real name."
    p "Enfin non, un narcissique utiliserait son vrai nom."

# game/script.rpy:304
translate french about_32b140b8:

    # N "I told you, it IS my real na--"
    N "Je t'ai dit, c'EST mon vrai n--"

# game/script.rpy:305
translate french about_0251bb7f:

    # p "Aight, aight. Weirdo."
    p "Ouais, ouais..."

# game/script.rpy:307
translate french about_f4e8ced4:

    # N "I made this game for the #Nar8 Game Jam. Gave me an excuse. And a deadline!"
    N "J'ai fait ce jeu pour la game jam #Nar8. Ça m'a donné une excuse. Et une deadline !"

# game/script.rpy:308
translate french about_49e277e7:

    # p "You procrastinated until the last day to enter, didn't you."
    p "T'as procrastiné jusqu'au dernier jour, avoue."

# game/script.rpy:309
translate french about_d50d1c38:

    # N "Yes."
    N "Oui."

# game/script.rpy:310
translate french about_3d191eb1:

    # N "Also! This game is uncopyrighted. Dedicated to the public domain."
    N "Ah, aussi ! Ce jeu n'est pas copyrighté. Il est dédié au domaine public."

# game/script.rpy:311
translate french about_b4a1b824:

    # N "I'm as open with my source code as I am with my sexuality."
    N "Je suis aussi ouvert avec mon code source qu'avec ma sexualité."

# game/script.rpy:313
translate french about_c3368a73:

    # p "Ugh, that's a terrible pun."
    p "Aeh, on a vu mieux, comme jeu de mots."

# game/script.rpy:314
translate french about_5881b31e:

    # N "Howzabout a 'Fork Me' programming pun?"
    N "Et pourquoi pas un 'Partage Moi', pour rester libre de droits ?"

# game/script.rpy:315
translate french about_650cdfe0:

    # p "noooooo."
    p "noooooon."

translate french strings:

    # game/script.rpy:95
    old "Let's play this thing!"
    new "Allez, on joue !"

    # game/script.rpy:95
    old "Who are you? (Credits)"
    new "Qui es-tu ? (Crédits)"

    # game/script.rpy:95
    old "Hm, tell me more. (About This Game)"
    new "Hm, dis-m'en plus. (À Propos)"

    # game/script.rpy:100
    old "Who are you?"
    new "Qui es-tu ?"

    # game/script.rpy:103
    old "Hm, tell me more."
    new "Hm, dis-m'en plus."

    # game/script.rpy:141
    old "With flowers and rainbows and gay unicorns?"
    new "Avec des fleurs, des arc-en-ciels et des licornes gayes ?"

    # game/script.rpy:141
    old "Apparently, with you redditing at Starbucks."
    new "Apparemment, avec toi, sur Reddit, au Starbucks."

    # game/script.rpy:141
    old "IT ALL ENDS IN BLOOD"
    new "TOUT ÇA FINIT DANS LE SANG"

    # game/script.rpy:179
    old "How can I win a game with no right answers?"
    new "Comment je peux gagner à un jeu sans bonne réponse ?"

    # game/script.rpy:179
    old "You're a bit of a downer, aren't you?"
    new "T'es du genre pessimiste, non ?"

    # game/script.rpy:179
    old "This 'true' game is full of lies?"
    new "Ce jeu 'vrai' est plein de mensonges ?"

    # game/script.rpy:273
    old "Speaking of which, let's play that! Now!"
    new "En parlant de ça, on joue ! Maintenant !"

    # game/script.rpy:278
    old "Speaking of that, can we play it now?"
    new "En parlant de ça, on peut jouer, maintenant ?"

    # game/script.rpy:278
    old "Why'd you make this? (About This Game)"
    new "Pourquoi t'as fait ça ? (À Propos)"

    # game/script.rpy:283
    old "Why'd you make this?"
    new "Why'd you make this?"

    # game/script.rpy:318
    old "Let's just play this game already."
    new "Allez, on joue."

    # game/script.rpy:323
    old "Bad puns aside, can we play now?"
    new "Si on a fini avec les jeux de mots, on peut commencer à jouer ?"

    # game/script.rpy:323
    old "So who ARE you? (Credits)"
    new "Qui t'es, en fait ? (Crédits)"

    # game/script.rpy:328
    old "So who ARE you?"
    new "Qui t'es, en fait ?"
