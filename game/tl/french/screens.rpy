translate french strings:

    # game/screens.rpy:269
    old "Back"
    new "Retour"

    # game/screens.rpy:270
    old "History"
    new "Historique"

    # game/screens.rpy:271
    old "Skip"
    new "Avance rapide"

    # game/screens.rpy:272
    old "Auto"
    new "Auto"

    # game/screens.rpy:273
    old "Save"
    new "Sauvegarde"

    # game/screens.rpy:274
    old "Q.Save"
    new "Sauvegarde R."

    # game/screens.rpy:275
    old "Q.Load"
    new "Chargement R."

    # game/screens.rpy:276
    old "Prefs"
    new "Préf."

    # game/screens.rpy:317
    old "Start"
    new "Nouvelle partie"

    # game/screens.rpy:325
    old "Load"
    new "Charger"

    # game/screens.rpy:327
    old "Preferences"
    new "Préférences"

    # game/screens.rpy:331
    old "End Replay"
    new "Fin de la rediffusion"

    # game/screens.rpy:335
    old "Restart"
    new "Redémarrer"

    # game/screens.rpy:337
    old "About"
    new "À propos"

    # game/screens.rpy:342
    old "Help"
    new "Aide"

    # game/screens.rpy:348
    old "Quit"
    new "Quitter"

    # game/screens.rpy:489
    old "Return"
    new "Retour"

    # game/screens.rpy:573
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # game/screens.rpy:579
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Conçu avec {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:615
    old "Page {}"
    new "Page {}"

    # game/screens.rpy:615
    old "Automatic saves"
    new "Sauvegardes automatiques"

    # game/screens.rpy:615
    old "Quick saves"
    new "Sauvegardes rapides"

    # game/screens.rpy:657
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A %d %B %Y, %H:%M"

    # game/screens.rpy:657
    old "empty slot"
    new "emplacement vide"

    # game/screens.rpy:674
    old "<"
    new "<"

    # game/screens.rpy:677
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:680
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:686
    old ">"
    new ">"

    # game/screens.rpy:743
    old "Display"
    new "Affichage"

    # game/screens.rpy:744
    old "Native"
    new "Native"

    # game/screens.rpy:745
    old "Medium"
    new "Medium"

    # game/screens.rpy:746
    old "Large"
    new "Large"

    # game/screens.rpy:747
    old "Fullscreen"
    new "Plein écran"

    # game/screens.rpy:751
    old "Rollback Side"
    new "Rembobinage côté"

    # game/screens.rpy:752
    old "Disable"
    new "Désactivé"

    # game/screens.rpy:753
    old "Left"
    new "Gauche"

    # game/screens.rpy:754
    old "Right"
    new "Droite"

    # game/screens.rpy:759
    old "Unseen Text"
    new "Texte non lu"

    # game/screens.rpy:760
    old "After Choices"
    new "Après les choix"

    # game/screens.rpy:761
    old "Transitions"
    new "Transitions"

    # game/screens.rpy:774
    old "Text Speed"
    new "Vitesse du texte"

    # game/screens.rpy:778
    old "Auto-Forward Time"
    new "Avance automatique"

    # game/screens.rpy:785
    old "Ambient Volume"
    new "Volume ambiant"

    # game/screens.rpy:792
    old "Sound Volume"
    new "Volume des sons"

    # game/screens.rpy:798
    old "Test"
    new "Test"

    # game/screens.rpy:802
    old "Voice Volume"
    new "Volume des voix"

    # game/screens.rpy:813
    old "Mute All"
    new "Couper tous les sons"

    # game/screens.rpy:932
    old "The dialogue history is empty."
    new "L'historique des dialogues est vide."

    # game/screens.rpy:1002
    old "Keyboard"
    new "Clavier"

    # game/screens.rpy:1003
    old "Mouse"
    new "Souris"

    # game/screens.rpy:1006
    old "Gamepad"
    new "Manette"

    # game/screens.rpy:1019
    old "Enter"
    new "Entrée"

    # game/screens.rpy:1020
    old "Advances dialogue and activates the interface."
    new "Avance dans les dialogues et active l’interface (effectue un choix)."

    # game/screens.rpy:1023
    old "Space"
    new "Espace"

    # game/screens.rpy:1024
    old "Advances dialogue without selecting choices."
    new "Avance dans les dialogues sans effectuer de choix."

    # game/screens.rpy:1027
    old "Arrow Keys"
    new "Flèches directionnelles"

    # game/screens.rpy:1028
    old "Navigate the interface."
    new "Permet de se déplacer dans l’interface."

    # game/screens.rpy:1031
    old "Escape"
    new "Echap."

    # game/screens.rpy:1032
    old "Accesses the game menu."
    new "Ouvre le menu du jeu."

    # game/screens.rpy:1035
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1036
    old "Skips dialogue while held down."
    new "Fait défiler les dialogues tant que la touche est pressée."

    # game/screens.rpy:1039
    old "Tab"
    new "Tab"

    # game/screens.rpy:1040
    old "Toggles dialogue skipping."
    new "Active ou désactives les «sauts des dialogues»."

    # game/screens.rpy:1043
    old "Page Up"
    new "Page Haut"

    # game/screens.rpy:1044
    old "Rolls back to earlier dialogue."
    new "Retourne au précédent dialogue."

    # game/screens.rpy:1047
    old "Page Down"
    new "Page Bas"

    # game/screens.rpy:1048
    old "Rolls forward to later dialogue."
    new "Avance jusqu'au prochain dialogue."

    # game/screens.rpy:1052
    old "Hides the user interface."
    new "Cache l’interface utilisateur."

    # game/screens.rpy:1056
    old "Takes a screenshot."
    new "Prend une capture d’écran."

    # game/screens.rpy:1060
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Active la {a=https://www.renpy.org/l/voicing}{size=24}vocalisation automatique{/size}{/a}."

    # game/screens.rpy:1066
    old "Left Click"
    new "Bouton gauche"

    # game/screens.rpy:1070
    old "Middle Click"
    new "Bouton central"

    # game/screens.rpy:1074
    old "Right Click"
    new "Bouton droit"

    # game/screens.rpy:1078
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Molette vers le haut\nClic sur le côté du Rollback"

    # game/screens.rpy:1082
    old "Mouse Wheel Down"
    new "Molette vers le bas"

    # game/screens.rpy:1089
    old "Right Trigger\nA/Bottom Button"
    new "Bouton R1\nA/Bouton du bas"

    # game/screens.rpy:1093
    old "Left Trigger\nLeft Shoulder"
    new "Gâchettes gauche"

    # game/screens.rpy:1097
    old "Right Shoulder"
    new "Bouton R1"

    # game/screens.rpy:1102
    old "D-Pad, Sticks"
    new "Boutons directionnels, stick gauche"

    # game/screens.rpy:1106
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1110
    old "Y/Top Button"
    new "Y/Bouton du haut"

    # game/screens.rpy:1113
    old "Calibrate"
    new "Calibrage"

    # game/screens.rpy:1178
    old "Yes"
    new "Oui"

    # game/screens.rpy:1179
    old "No"
    new "Non"

    # game/screens.rpy:1226
    old "Skipping"
    new "Avance rapide"

    # game/screens.rpy:1469
    old "Menu"
    new "Menu"

    # game/screens.rpy:768
    old "Language"
    new "Langue"

    # game/screens.rpy:769
    old "English"
    new "Anglais"

    # game/screens.rpy:770
    old "French"
    new "Français"

translate french style quick_button_text:
    size 8
