﻿# game/jack_2.rpy:26
translate french start_jack_2_cab1fd0c:

    # n "Hey Jack."
    n "Hey Jack."

# game/jack_2.rpy:28
translate french start_jack_2_03a6a8bc:

    # j "Hello, Nicky darling. Still a sad sack of sadness?"
    j "Salut, Nicky chéri. Toujours un paquet de tristesse ?"

# game/jack_2.rpy:30
translate french start_jack_2_9a18afb7:

    # j "Hello, Nicky darling."
    j "Salut, Nicky chéri."

# game/jack_2.rpy:31
translate french start_jack_2_fbd050b3:

    # j "How was coming out to your parents? Did I tell you so, or did I tell you so?"
    j "Comment ça s'est passé ? Je te l'avais dit, ou je te l'avais dit ?"

# game/jack_2.rpy:35
translate french start_jack_2_bd71b2c2:

    # n "Jack... we messed up big time, Jack."
    n "Jack... on a fait une connerie, Jack."

# game/jack_2.rpy:36
translate french start_jack_2_e781c384:

    # j "No... no, no."
    j "Non... non, non."

# game/jack_2.rpy:37
translate french start_jack_2_ed792d84:

    # j "You're kidding me, right? What happened?"
    j "Tu déconnes ? Qu'est-ce qui s'est passé ?"

# game/jack_2.rpy:40
translate french start_jack_2_d32c3b48:

    # n "Things could have been worse."
    n "Ça aurait pu être pire."

# game/jack_2.rpy:41
translate french start_jack_2_e6a178d7:

    # j "Oh. Oh no."
    j "Oh. Oh non."

# game/jack_2.rpy:42
translate french start_jack_2_e5b0a73c:

    # j "I didn't expect that they'd... what... what happened?"
    j "Je ne m'attendais pas... que... qu'est-ce qui s'est passé ?"

# game/jack_2.rpy:45
translate french start_jack_2_a1b478f4:

    # n "Shut up, Jack."
    n "Ferme-la, Jack."

# game/jack_2.rpy:46
translate french start_jack_2_0e655ef5:

    # j "Ha, yes, I knew I was right!"
    j "Ha, yes, je savais que j'avais raison !"

# game/jack_2.rpy:47
translate french start_jack_2_392198a2:

    # n "No. Jack, we can't see each other ever again."
    n "Non. Jack, on ne peut plus se revoir."

# game/jack_2.rpy:48
translate french start_jack_2_39327c78:

    # j "Wait."
    j "Attend."

# game/jack_2.rpy:49
translate french start_jack_2_f593fc2c:

    # j "No, no, no. You're kidding me, right? What happened?"
    j "Non, non, non. Tu déconnes ? Qu'est-ce qui s'est passé ?"

# game/jack_2.rpy:57
translate french what_happened_d25eac77:

    # n "My dad punched me in the face."
    n "Mon père m'a frappé dans le visage."

# game/jack_2.rpy:60
translate french what_happened_e8163d18:

    # n "They're making me change schools."
    n "Ils vont me faire changer d'école."

# game/jack_2.rpy:63
translate french what_happened_968191e3:

    # n "They read all our texts."
    n "Ils ont lu tous nos messages."

# game/jack_2.rpy:68
translate french what_happened_3488ba7c:

    # n "My parents got verbally violent with each other."
    n "Mes parents se sont engueulés."

# game/jack_2.rpy:71
translate french what_happened_e8163d18_1:

    # n "They're making me change schools."
    n "Ils vont me faire changer d'école."

# game/jack_2.rpy:74
translate french what_happened_968191e3_1:

    # n "They read all our texts."
    n "Ils ont lu tous nos messages."

# game/jack_2.rpy:77
translate french what_happened_fb4b894b:

    # n "Well, my dad's oblivious. For now. But my mom..."
    n "Mon père n'a rien compris. Pour le moment. Mais ma mère..."

# game/jack_2.rpy:81
translate french what_happened_24d52e1f:

    # n "She's making me change schools."
    n "Elle va me faire changer d'école."

# game/jack_2.rpy:84
translate french what_happened_bf750c9b:

    # n "She's setting me up with a girl I've never met."
    n "Elle essaye de me caser avec une fille que j'ai jamais vu."

# game/jack_2.rpy:87
translate french what_happened_9553f1bf:

    # n "She read all our texts."
    n "Elle a lu tous nos messages."

# game/jack_2.rpy:92
translate french what_happened_20d6e1ed:

    # n "She got a tutor to kill all my after-school hours."
    n "Elle m'a pris du soutien scolaire pour monopoliser mon temps libre."

# game/jack_2.rpy:95
translate french what_happened_bf750c9b_1:

    # n "She's setting me up with a girl I've never met."
    n "Elle essaye de me caser avec une fille que j'ai jamais vu."

# game/jack_2.rpy:98
translate french what_happened_9553f1bf_1:

    # n "She read all our texts."
    n "Elle a lu tous nos messages."

# game/jack_2.rpy:104
translate french what_happened_abuse_8441b6f5:

    # j "Oh my god!"
    j "Oh non !"

# game/jack_2.rpy:105
translate french what_happened_abuse_3723d9f5:

    # j "Nicky, you need to call Child Protective Services."
    j "Nicky, il faut que t'appelles la Protection de l'Enfance."

# game/jack_2.rpy:106
translate french what_happened_abuse_92a75744:

    # n "What?! No. That's way too much."
    n "Quoi ?! Non. Pas à ce point-là."

# game/jack_2.rpy:107
translate french what_happened_abuse_013ce3da:

    # j "Just... okay, but at least promise me you'll visit the school counselor tomorrow?"
    j "Juste... ok, mais alors promet-moi que tu iras voir le conseiller de l'école, demain ?"

# game/jack_2.rpy:108
translate french what_happened_abuse_a341c2fc:

    # n "Fine."
    n "D'accord."

# game/jack_2.rpy:109
translate french what_happened_abuse_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:115
translate french what_happened_school_224497b4:

    # j "No!"
    j "Non !"

# game/jack_2.rpy:116
translate french what_happened_school_1b2a000b:

    # j "Why?! Why are they doing that?"
    j "Pourquoi ?! Pourquoi est-ce qu'ils font ça ?"

# game/jack_2.rpy:117
translate french what_happened_school_85aef970:

    # n "Because 'Jack and the school is a bad influence on me', or something. They just want to break us up."
    n "Parce que 'Jack et l'école ont une mauvaise influence sur moi', apparemment. Ils veulent juste nous séparer."

# game/jack_2.rpy:118
translate french what_happened_school_9273fafb:

    # j "That's horrible..."
    j "C'est horrible..."

# game/jack_2.rpy:124
translate french what_happened_girl_48e3be96:

    # j "Ew, seriously?"
    j "Euh, sérieusement ?"

# game/jack_2.rpy:125
translate french what_happened_girl_131f53cf:

    # n "Her name's Claire Something. She'll also be tutoring me."
    n "Elle s'appelle Claire. Elle va aussi me faire du soutien scolaire."

# game/jack_2.rpy:126
translate french what_happened_girl_b3f70102:

    # j "Ew squared, they're also hooking you up with your own tutor?"
    j "Euh, ils essayent aussi de te caser avec celle qui te fait du soutien ?"

# game/jack_2.rpy:127
translate french what_happened_girl_694ac062:

    # n "Yup."
    n "Yup."

# game/jack_2.rpy:133
translate french what_happened_texts_bdcb5835:

    # j "That is just plain rude!"
    j "Mais c'est cruel !"

# game/jack_2.rpy:134
translate french what_happened_texts_48562875:

    # j "Wait, what will you do with these texts right now, then?"
    j "Attend, mais tu vas en faire quoi de ces messages, de maintenant ?"

# game/jack_2.rpy:135
translate french what_happened_texts_4818ed1a:

    # n "I can hide them better. My parents aren't exactly a tech-savvy bunch."
    n "Je peux mieux les cacher. Mes parents ne sont pas vraiment des pros de la technologie."

# game/jack_2.rpy:136
translate french what_happened_texts_383c3f85:

    # j "...just plain rude."
    j "...juste cruel."

# game/jack_2.rpy:142
translate french what_happened_2_21abce55:

    # n "And that's just one out of three crappy things that happened."
    n "Et c'est qu'une seule de choses nulles qui sont arrivées."

# game/jack_2.rpy:143
translate french what_happened_2_1dbbc473:

    # j "Nicky..."
    j "Nicky..."

# game/jack_2.rpy:144
translate french what_happened_2_9920b663:

    # j "I am truly, truly apologetic."
    j "Je suis vraiment, vraiment désolé."

# game/jack_2.rpy:145
translate french what_happened_2_92346c56:

    # j "This is my fault. I urged you to come out to your parents. Stupid me."
    j "C'est de ma faute. Je t'ai poussé à en parler avec tes parents. Je suis stupide."

# game/jack_2.rpy:151
translate french what_happened_2_c43ae364:

    # n "Yeah, stupid you."
    n "Ouais, t'es stupide."

# game/jack_2.rpy:152
translate french what_happened_2_9b2ac80c:

    # n "If you hadn't been all so 'ohhhh Nicky coming out is good for the soul' and shit, this never would have..."
    n "Si tu ne m'avais pas dit 'ohhhh Nicky le coming out c'est tellement bieeeeeen', ça ne se serait jamais passé..."

# game/jack_2.rpy:153
translate french what_happened_2_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:154
translate french what_happened_2_a93f545d:

    # n "I'm sorry. You're the only person I can lash out on."
    n "Désolé. T'es la seule personne sur laquelle je puisse me défouler."

# game/jack_2.rpy:155
translate french what_happened_2_3b2911ee:

    # n "Isn't that just fucked up?"
    n ". . ."

# game/jack_2.rpy:160
translate french what_happened_2_fd3637f9:

    # n "No, it's THEIR fault."
    n "Non, c'est LEUR faute."

# game/jack_2.rpy:161
translate french what_happened_2_e4035e21:

    # n "They already read our texts. Anything I would have said after that couldn't change what happened."
    n "Ils avaient déjà lu nos messages. Peu importe ce que je disais, ça n'aurait rien changé."

# game/jack_2.rpy:163
translate french what_happened_2_f8ab0bcd:

    # j "What! You didn't tell me they also read your texts!"
    j "Quoi ! Tu ne m'avais pas dit qu'ils avaient lu tes messages !"

# game/jack_2.rpy:165
translate french what_happened_2_6f428314:

    # j "And they're stuck in their old-fashioned moralities, the poor things."
    j "Et ils restent bloqués dans leur mentalité de vieux."

# game/jack_2.rpy:166
translate french what_happened_2_f6b0b208:

    # n "I wouldn't go so far as to pity them."
    n "Je n'irai pas jusqu'à avoir pitié d'eux."

# game/jack_2.rpy:171
translate french what_happened_2_2d584472:

    # n "No, this is all my fault."
    n "Non, tout est de ma faute."

# game/jack_2.rpy:172
translate french what_happened_2_482a90cd:

    # n "I should have passcode-locked my phone, or use encrypted text, or hid it better..."
    n "J'aurais dû mettre un mot de passe sur mon téléphone, ou chiffrer mes messages, ou mieux les cacher..."

# game/jack_2.rpy:174
translate french what_happened_2_fb4cd4a5:

    # j "They read your texts, too?..."
    j "Ils ont aussi lu tes messages ?..."

# game/jack_2.rpy:175
translate french what_happened_2_fc5fa8a4:

    # j "Nicky, you had every right to trust them, they're your parents. They abused that trust. It's not your fault."
    j "Nicky, tu avais toutes les raisons de leur faire confiance, ce sont tes parents. Ils ont abusé de ta confiance. Ce n'est pas ta faute."

# game/jack_2.rpy:176
translate french what_happened_2_90369dcf:

    # n "Yeah..."
    n "Ouais..."

# game/jack_2.rpy:181
translate french what_now_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:183
translate french what_now_118961c9:

    # n "You know... talking with my parents, it's like..."
    n "Tu sais... Parler avec mes parents, c'est..."

# game/jack_2.rpy:184
translate french what_now_3e083b6d:

    # n "That mode of communication?"
    n "Ce mode de communication ?"

# game/jack_2.rpy:185
translate french what_now_bc72a345:

    # n "It's imprecise, impersonal, impossible to truly connect."
    n "C'est imprécis, impersonnel, impossible de vraiment se comprendre."

# game/jack_2.rpy:187
translate french what_now_93f8e717_1:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:188
translate french what_now_5dd10623:

    # j "What now?"
    j "Et maintenant ?"

# game/jack_2.rpy:192
translate french what_now_4e58723f:

    # n "I'm going to sabotage my parents' plans."
    n "Je vais saboter les plans de mes parents."

# game/jack_2.rpy:195
translate french what_now_3adffe37:

    # n "I'll set up a new email and virtual phone number to talk with you."
    n "Je vais créer un nouvel email et un numéro de téléphone virtuel pour te parler."

# game/jack_2.rpy:196
translate french what_now_a06f8432:

    # n "This way they can't spy on our communications anymore."
    n "Comme ça, ils ne pourront plus nous espionner."

# game/jack_2.rpy:198
translate french what_now_b55bf8c4:

    # n "I'll tell Claire everything. With any luck, she'll help me fight back."
    n "Je vais tout dire à Claire. Avec un peu de chance, elle m'aidera à me défendre."

# game/jack_2.rpy:200
translate french what_now_b5428952:

    # n "I'll figure out a way, somehow..."
    n "Je trouverai bien un moyen..."

# game/jack_2.rpy:204
translate french what_now_dfdb5688:

    # n "I'll visit the school counselor tomorrow."
    n "J'irai voir le conseiller de l'école demain."

# game/jack_2.rpy:207
translate french what_now_e74c8ba1:

    # n "Like I promised. Like you made me promise."
    n "Comme je t'ai promis."

# game/jack_2.rpy:209
translate french what_now_e9b2ac86:

    # n "My current school, that is. I don't know how soon they'll be transferring me."
    n "De mon école actuelle. Je ne sais pas quand ils vont me faire changer."

# game/jack_2.rpy:211
translate french what_now_cdfa6e39:

    # n "At least they'll be someone else I can lash out on."
    n "Au moins ça fera quelqu'un d'autre sur qui je pourrai me défouler."

# game/jack_2.rpy:215
translate french what_now_726589bc:

    # n "I'm getting out of this house."
    n "Je vais partir de cette maison."

# game/jack_2.rpy:217
translate french what_now_476da40f:

    # n "Not running away, I mean. Although if I did I could crash at your place."
    n "Je ne vais pas m'enfuir. Quoique je pourrais toujours squatter chez toi."

# game/jack_2.rpy:218
translate french what_now_cfd98df8:

    # n "But anyway. I'm going to try to get an internship or scholarship in the US."
    n "Mais peu importe. Je vais essayer de trouver un stage aux USA."

# game/jack_2.rpy:219
translate french what_now_a001a9b3:

    # n "And get far, far away from these people."
    n "Et partir loin, très loin de ces gens-là."

# game/jack_2.rpy:224
translate french what_now_2_017a835d:

    # j "No, I mean... what now, between us?"
    j "Non, je veux dire... et maintenant, entre nous ?"

# game/jack_2.rpy:225
translate french what_now_2_46074bd9:

    # n "Jack..."
    n "Jack..."

# game/jack_2.rpy:226
translate french what_now_2_a3a18360:

    # j "What do we do? What... What happens?"
    j "Qu'est-ce qu'on fait ?... Qu'est-ce qui se passe ?"

# game/jack_2.rpy:227
translate french what_now_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/jack_2.rpy:233
translate french what_now_2_3ecfd185:

    # n "We have to break up."
    n "On doit se séparer."

# game/jack_2.rpy:235
translate french what_now_2_96370f78:

    # j "No, no no..."
    j "Non, non non..."

# game/jack_2.rpy:236
translate french what_now_2_ba6d250f:

    # n "I can't do this to you, Jack. I can't pull you down with me."
    n "Je ne peux pas te faire ça, Jack. Je ne peux pas te trainer avec moi."

# game/jack_2.rpy:237
translate french what_now_2_3b34da50:

    # j "At least, don't type 'we can still be friends'."
    j "Au moins, ne m'écris pas 'on peut toujours rester amis'."

# game/jack_2.rpy:238
translate french what_now_2_69c9195c:

    # n "we can still be frie"
    n "on peut toujours rester a"

# game/jack_2.rpy:239
translate french what_now_2_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/jack_2.rpy:240
translate french what_now_2_4858b360:

    # j "Because, of course we're friends. Of course we are."
    j "Parce qu'on est amis. Évidemment."

# game/jack_2.rpy:241
translate french what_now_2_24796dbe_2:

    # n ". . ."
    n ". . ."

# game/jack_2.rpy:245
translate french what_now_2_0795c7e4:

    # n "We stick together as long as we can."
    n "On reste ensemble autant qu'on peut."

# game/jack_2.rpy:246
translate french what_now_2_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:247
translate french what_now_2_9f7bc8aa:

    # j "As long as we can."
    j "Autant qu'on peut."

# game/jack_2.rpy:248
translate french what_now_2_24796dbe_3:

    # n ". . ."
    n ". . ."

# game/jack_2.rpy:253
translate french what_now_2_f5fd36c3:

    # n "I don't know."
    n "Je ne sais pas."

# game/jack_2.rpy:255
translate french what_now_2_93f8e717_1:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:260
translate french what_now_3_a666b817:

    # n "It's late."
    n "Il est tard."

# game/jack_2.rpy:261
translate french what_now_3_984b85ad:

    # n "There's a lot I need to sleep on, now."
    n "J'ai beaucoup de choses à ruminer."

# game/jack_2.rpy:262
translate french what_now_3_53d71353:

    # j "Okay."
    j "Ok."

# game/jack_2.rpy:263
translate french what_now_3_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_2.rpy:264
translate french what_now_3_0c6392c2:

    # j "I love you, Nicky."
    j "Je t'aime, Nicky."

# game/jack_2.rpy:265
translate french what_now_3_97f3700c:

    # n "I love you too, Jack."
    n "Je t'aime, Jack."

# game/jack_2.rpy:270
translate french what_now_3_85b2924a:

    # n "You goof."
    n "Petit bouffon."

translate french strings:

    # game/jack_2.rpy:33
    old "Jack... we messed up big time, Jack."
    new "Jack... on a fait une connerie, Jack."

    # game/jack_2.rpy:33
    old "Things could have been worse."
    new "Ça aurait pu être pire."

    # game/jack_2.rpy:33
    old "Shut up, Jack."
    new "Ferme-la, Jack."

    # game/jack_2.rpy:55
    old "My dad punched me in the face."
    new "Mon père m'a frappé dans le visage."

    # game/jack_2.rpy:55
    old "They're making me change schools."
    new "Ils vont me faire changer d'école."

    # game/jack_2.rpy:55
    old "They read all our texts."
    new "Ils ont lu tous nos messages."

    # game/jack_2.rpy:66
    old "My parents got verbally violent with each other."
    new "Mes parents se sont engueulés."

    # game/jack_2.rpy:79
    old "She's making me change schools."
    new "Elle va me faire changer d'école."

    # game/jack_2.rpy:79
    old "She's setting me up with a girl I've never met."
    new "Elle essaye de me caser avec une fille que j'ai jamais vu."

    # game/jack_2.rpy:79
    old "She read all our texts."
    new "Elle a lu tous nos messages."

    # game/jack_2.rpy:90
    old "She got a tutor to kill all my after-school hours."
    new "Elle m'a pris du soutien scolaire pour monopoliser mon temps libre."

    # game/jack_2.rpy:147
    old "Yeah, stupid you."
    new "Ouais, t'es stupide."

    # game/jack_2.rpy:147
    old "No, it's THEIR fault."
    new "Non, c'est LEUR faute."

    # game/jack_2.rpy:147
    old "No, this is all my fault."
    new "Non, tout est de ma faute."

    # game/jack_2.rpy:190
    old "I'm going to sabotage my parents' plans."
    new "Je vais saboter les plans de mes parents."

    # game/jack_2.rpy:190
    old "I'll visit the school counselor tomorrow."
    new "J'irai voir le conseiller de l'école demain."

    # game/jack_2.rpy:190
    old "I'm getting out of this house."
    new "Je vais partir de cette maison."

    # game/jack_2.rpy:229
    old "We have to break up."
    new "On doit se séparer."

    # game/jack_2.rpy:229
    old "We stick together as long as we can."
    new "On reste ensemble autant qu'on peut."

    # game/jack_2.rpy:229
    old "I don't know."
    new "I don't know."

# game/jack_2.rpy:269
translate french what_now_3_deba5cdb:

    # n "You amateur poet."
    n "Petit poête amateur."

# game/jack_2.rpy:271
translate french what_now_3_db812dca:

    # n "You new-age hippie."
    n "Petit hippie new-age."
