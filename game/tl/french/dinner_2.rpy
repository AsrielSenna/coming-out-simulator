# game/dinner_2.rpy:8
translate french start_dinner_2_9788cb93:

    # m "Hi sweetie."
    m "Salut chéri."

# game/dinner_2.rpy:12
translate french start_dinner_2_f0fabe74:

    # m "Oh, you started eating without me. You're very impatient."
    m "Oh, tu as commencé sans moi. Tu es très impatient."

# game/dinner_2.rpy:13
translate french start_dinner_2_1d98e7c4:

    # n "...right."
    n "...oui."

# game/dinner_2.rpy:15
translate french start_dinner_2_98e67ae0:

    # m "You could have started without me. No need to let your food get cold."
    m "Tu aurais pu commencer sans moi. Pas besoin de laisser refroidir."

# game/dinner_2.rpy:16
translate french start_dinner_2_a0428bd0:

    # n "...sure."
    n "...d'accord."

# game/dinner_2.rpy:18
translate french start_dinner_2_356e2e58:

    # m "It's immature to play with your food, you know."
    m "C'est immature de jouer avec la nourriture, tu sais."

# game/dinner_2.rpy:19
translate french start_dinner_2_0d2c2727:

    # n "Yeah, yeah."
    n "Oui, oui."

# game/dinner_2.rpy:21
translate french start_dinner_2_1dad3d18:

    # m "Your father's running late. He'll be joining us for dinner in an hour's time."
    m "Ton père arrivera en retard. Il nous rejoindra pour le dîner dans une heure."

# game/dinner_2.rpy:25
translate french start_dinner_2_57e8085d:

    # n "Cool. Let's eat."
    n "Cool. On mange, alors."

# game/dinner_2.rpy:26
translate french start_dinner_2_c37be822:

    # n "*nom nom nom*"
    n "*miam miam miam*"

# game/dinner_2.rpy:27
translate french start_dinner_2_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:28
translate french start_dinner_2_6762ce18:

    # m "What's your plans for tomorrow?"
    m "Qu'est-ce que tu as prévu de faire, demain ?"

# game/dinner_2.rpy:30
translate french start_dinner_2_81e3c88e:

    # n "I have something to tell both of you."
    n "Il y a quelque chose que je dois vous dire, à tous les deux."

# game/dinner_2.rpy:31
translate french start_dinner_2_e317537b:

    # m "Alright. Tell us both later when he comes back."
    m "D'accord. Dis-nous plus tard, quand il rentrera."

# game/dinner_2.rpy:32
translate french start_dinner_2_620902ba:

    # n "Oh. Okay."
    n "Oh. Okay."

# game/dinner_2.rpy:33
translate french start_dinner_2_4733cca1_1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:34
translate french start_dinner_2_c37be822_1:

    # n "*nom nom nom*"
    n "*miam miam miam*"

# game/dinner_2.rpy:35
translate french start_dinner_2_e38b6c17:

    # m "So, what's your plans for tomorrow?"
    m "Donc, qu'est-ce que tu as prévu de faire pour demain ?"

# game/dinner_2.rpy:37
translate french start_dinner_2_5cd915d8:

    # n "There's something I need to tell just you first."
    n "Il y a quelque chose que je dois te dire."

# game/dinner_2.rpy:38
translate french start_dinner_2_e96e4e10:

    # m "Hold on Nick, I haven't asked about your day yet!"
    m "Attend Nick, je ne t'ai pas demandé comment s'était passée ta journée !"

# game/dinner_2.rpy:39
translate french start_dinner_2_39c59583:

    # n "Today was fine."
    n "Ça s'est bien passé."

# game/dinner_2.rpy:40
translate french start_dinner_2_605386df:

    # m "Okay. And what's your plans for tomorrow?"
    m "D'accord. Et qu'est-ce que tu prévois de faire, demain ?"

# game/dinner_2.rpy:44
translate french start_dinner_2_1_f7bbe505:

    # n "Oh. Uh... studying."
    n "Oh. Euh... réviser."

# game/dinner_2.rpy:45
translate french start_dinner_2_1_64b3b71d:

    # n "Yeah. Tomorrow I'm studying."
    n "Ouais. Demain, je révise."

# game/dinner_2.rpy:46
translate french start_dinner_2_1_048b1e3d:

    # m "What subject?"
    m "Quelle matière ?"

# game/dinner_2.rpy:47
translate french start_dinner_2_1_52b9d41c:

    # n "Er..."
    n "Euh..."

# game/dinner_2.rpy:63
translate french start_dinner_2_2_c08bc8f4:

    # m "Good."
    m "Bien."

# game/dinner_2.rpy:64
translate french start_dinner_2_2_69573637:

    # m "You really, really could improve your grades in your [studying_subject] class."
    m "Tu as vraiment, vraiment besoin d'améliorer tes notes en [studying_subject!tl]."

# game/dinner_2.rpy:65
translate french start_dinner_2_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_2.rpy:66
translate french start_dinner_2_2_1ba0e04a:

    # m "So, I'll be at the library tomorrow."
    m "Du coup, je serai à la bibliothèque, demain."

# game/dinner_2.rpy:67
translate french start_dinner_2_2_b5e68720:

    # m "Will I see you studying there?"
    m "Je te verrai y travailler ?"

# game/dinner_2.rpy:68
translate french start_dinner_2_2_8f68c797:

    # n "Actually, I'm gonna study at Jack's place."
    n "En fait, je vais travailler chez Jack."

# game/dinner_2.rpy:69
translate french start_dinner_2_2_939ff9bd:

    # m "Again?"
    m "Encore ?"

# game/dinner_2.rpy:70
translate french start_dinner_2_2_0a6f28bb:

    # m "You spend a lot of time with him."
    m "Tu passes beaucoup de temps avec lui."

# game/dinner_2.rpy:80
translate french start_dinner_2_2_af8866e5:

    # n "Mom, Jack is... more than a friend."
    n "Maman, Jack est... plus qu'un ami."

# game/dinner_2.rpy:83
translate french start_dinner_2_2_fd9edbda:

    # m "Oh, like best friends?"
    m "Oh, comme un meilleur ami ?"

# game/dinner_2.rpy:84
translate french start_dinner_2_2_96986040:

    # n "Um. Well--"
    n "Hum. Eh bien--"

# game/dinner_2.rpy:85
translate french start_dinner_2_2_6ae66a86:

    # m "So you're just hanging out, not studying."
    m "Donc vous traînez ensemble, vous ne révisez pas."

# game/dinner_2.rpy:86
translate french start_dinner_2_2_e663e4bc:

    # n "We ARE studying!"
    n "Mais SI on révise !"

# game/dinner_2.rpy:87
translate french start_dinner_2_2_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:88
translate french start_dinner_2_2_7ad44fd7:

    # m "Alright, just don't lie to me."
    m "Ok, mais ne me mens pas."

# game/dinner_2.rpy:89
translate french start_dinner_2_2_13be1e5b:

    # n "I'm not."
    n "Mais non."

# game/dinner_2.rpy:107
translate french buddy_1_a3a95d07:

    # m "Oh. So you're just hanging out, not studying."
    m "Oh. Donc vous traînez ensemble, vous ne révisez pas."

# game/dinner_2.rpy:108
translate french buddy_1_e663e4bc:

    # n "We ARE studying!"
    n "Mais SI on révise !"

# game/dinner_2.rpy:109
translate french buddy_1_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:110
translate french buddy_1_7ad44fd7:

    # m "Alright, just don't lie to me."
    m "Ok, mais ne me mens pas."

# game/dinner_2.rpy:111
translate french buddy_1_13be1e5b:

    # n "I'm not."
    n "Mais non."

# game/dinner_2.rpy:113
translate french buddy_1_adbcede6:

    # m "Okay. I'm just making sure."
    m "Ok. Je vérifie."

# game/dinner_2.rpy:114
translate french buddy_1_1903bd0e:

    # n "Of... what?"
    n "Tu vérifies... quoi ?"

# game/dinner_2.rpy:120
translate french buddy_caught_lying_1_7772bcfa:

    # m "Wait..."
    m "Attend..."

# game/dinner_2.rpy:121
translate french buddy_caught_lying_1_316ae09b:

    # m "I thought you said you 'just study together'."
    m "Je croyais que vous alliez 'juste réviser ensemble'."

# game/dinner_2.rpy:122
translate french buddy_caught_lying_1_13307054:

    # m "You didn't tell me you were friends."
    m "Tu ne m'as pas dit que vous étiez amis."

# game/dinner_2.rpy:135
translate french buddy_1_point_5_eae369b6:

    # m "Just... don't hang around him too much."
    m "Juste... ne traîne pas trop avec lui."

# game/dinner_2.rpy:136
translate french buddy_1_point_5_de480074:

    # m "People might get the wrong idea."
    m "Les gens pourraient croire des choses."

# game/dinner_2.rpy:155
translate french buddy_2_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_2.rpy:157
translate french buddy_2_75f584d6:

    # m "Just don't lie to me."
    m "Mais ne me mens pas."

# game/dinner_2.rpy:158
translate french buddy_2_34c35deb:

    # n "I won't."
    n "Mais non."

# game/dinner_2.rpy:159
translate french buddy_2_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:160
translate french buddy_2_9fbe9371:

    # m "But... about you hanging out with Jack."
    m "Mais... à propos de toi qui traînes avec Jack."

# game/dinner_2.rpy:161
translate french buddy_2_5aa7a659:

    # m "It's just that some people might assume things, since..."
    m "C'est juste que certaines personnes pourraient en déduire des choses, comme..."

# game/dinner_2.rpy:162
translate french buddy_2_83d7c1ff:

    # m "You know... he looks like..."
    m "Tu sais... on dirait..."

# game/dinner_2.rpy:163
translate french buddy_2_0ddfa674:

    # m "A gay?"
    m "Un gay ?"

# game/dinner_2.rpy:168
translate french buddy_3_c98597e2:

    # m "Just between mother and son, I think he might be... you know..."
    m "Juste entre toi et moi, je pense qu'il pourrait être... tu sais..."

# game/dinner_2.rpy:169
translate french buddy_3_4f77d59d:

    # n "No, what?"
    n "Non, quoi ?"

# game/dinner_2.rpy:170
translate french buddy_3_6908c54a:

    # m "A gay!"
    m "Un gay !"

# game/dinner_2.rpy:171
translate french buddy_3_1ec4befa:

    # m "He looks and talks like a gay."
    m "Il parle comme un gay et il y ressemble."

# game/dinner_2.rpy:176
translate french buddy_4_eadf8768:

    # m "Oh, that's like a zen thing, right?"
    m "Oh, tu parles des légendes urbaines, c'est ça ?"

# game/dinner_2.rpy:177
translate french buddy_4_1f312532:

    # n "Um."
    n "Hum."

# game/dinner_2.rpy:178
translate french buddy_4_77e430d4:

    # m "Zen is also about nature, and your classmate Jack, he..."
    m "Les légendes urbaines c'est naturel, et ton camarade de classe, Jack, il..."

# game/dinner_2.rpy:179
translate french buddy_4_87efd561:

    # m "...you know, doesn't seem natural?"
    m "...tu sais, il n'a pas l'air naturel ?"

# game/dinner_2.rpy:182
translate french buddy_4_106c0569:

    # n "You think he's gay."
    n "Tu penses qu'il est gay."

# game/dinner_2.rpy:183
translate french buddy_4_424fdc9a:

    # m "Yes!"
    m "Oui !"

# game/dinner_2.rpy:184
translate french buddy_4_c13f7f2e:

    # m "You suspect it, too!"
    m "Tu le suspectes aussi !"

# game/dinner_2.rpy:193
translate french buddy_4_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_2.rpy:194
translate french buddy_4_75f584d6:

    # m "Just don't lie to me."
    m "Mais ne me mens pas."

# game/dinner_2.rpy:195
translate french buddy_4_34c35deb:

    # n "I won't."
    n "Mais non."

# game/dinner_2.rpy:196
translate french buddy_4_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:198
translate french buddy_4_38a8f6f2:

    # m "But yes, even you agree that it's bad to be seen as 'not natural'."
    m "Mais oui, même toi tu es d'accord que c'est pas bien d'être vu comme 'contre nature'."

# game/dinner_2.rpy:199
translate french buddy_4_6de770d3:

    # n "I never said--"
    n "Je n'ai jamais dit--"

# game/dinner_2.rpy:200
translate french buddy_4_b9ab7e9e:

    # m "And I'm just looking out for you! Because he acts like, you know..."
    m "Et je m'inquiète juste pour toi ! Parce qu'il se comporte comme, tu sais..."

# game/dinner_2.rpy:201
translate french buddy_4_6908c54a:

    # m "A gay!"
    m "Un gay !"

# game/dinner_2.rpy:207
translate french buddy_4_a4590495:

    # m "I'm just being honest."
    m "Je suis juste honnête."

# game/dinner_2.rpy:208
translate french buddy_4_38a8f6f2_1:

    # m "But yes, even you agree that it's bad to be seen as 'not natural'."
    m "Mais oui, même toi tu es d'accord que c'est pas bien d'être vu comme 'contre nature'."

# game/dinner_2.rpy:209
translate french buddy_4_6de770d3_1:

    # n "I never said--"
    n "Je n'ai jamais dit--"

# game/dinner_2.rpy:210
translate french buddy_4_b9ab7e9e_1:

    # m "And I'm just looking out for you! Because he acts like, you know..."
    m "Et je m'inquiète juste pour toi ! Parce qu'il se comporte comme, tu sais..."

# game/dinner_2.rpy:211
translate french buddy_4_6908c54a_1:

    # m "A gay!"
    m "Un gay !"

# game/dinner_2.rpy:220
translate french buddy_choice_3d1c83bf:

    # m "And since you say he's a 'good pal'..."
    m "Et comme tu dis que c'est un 'bon copain'..."

# game/dinner_2.rpy:221
translate french buddy_choice_50254e9a:

    # m "People might think you're a gay like him, too."
    m "Les gens pourraient croire que tu es un gay comme lui."

# game/dinner_2.rpy:223
translate french buddy_choice_6775b38e:

    # m "And since you say he's your BEST friend..."
    m "Et comme tu dis que c'est ton MEILLEUR ami..."

# game/dinner_2.rpy:224
translate french buddy_choice_50254e9a_1:

    # m "People might think you're a gay like him, too."
    m "Les gens pourraient croire que tu es un gay comme lui."

# game/dinner_2.rpy:227
translate french buddy_choice_69e78ae5:

    # n "Ha, he sure acts gay. Luckily, he's not."
    n "Ha oui, on dirait, mais heureusement il est pas gay."

# game/dinner_2.rpy:228
translate french buddy_choice_cbfb3b2d:

    # m "See? You also think there's something not right about it."
    m "Tu vois ? Toi aussi tu penses qu'il y a quelque chose de pas clair avec tout ça."

# game/dinner_2.rpy:229
translate french buddy_choice_a0428bd0:

    # n "...sure."
    n "...bien sûr."

# game/dinner_2.rpy:232
translate french buddy_choice_6f8e022f:

    # n "What's wrong with being gay?!"
    n "Où est le mal dans le fait d'être gay ?!"

# game/dinner_2.rpy:233
translate french buddy_choice_c519d137:

    # m "Nothing! Nothing."
    m "Rien ! Rien."

# game/dinner_2.rpy:240
translate french buddy_choice_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_2.rpy:241
translate french buddy_choice_75f584d6:

    # m "Just don't lie to me."
    m "Mais ne me mens pas."

# game/dinner_2.rpy:242
translate french buddy_choice_34c35deb:

    # n "I won't."
    n "Mais non."

# game/dinner_2.rpy:243
translate french buddy_choice_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:252
translate french buddy_aftermath_33ebc4dd:

    # m "Don't get me wrong."
    m "Comprends-moi bien."

# game/dinner_2.rpy:253
translate french buddy_aftermath_cfa4555a:

    # m "I'm not saying those kind of people are bad!"
    m "Je ne dis pas que ces gens sont mauvais !"

# game/dinner_2.rpy:254
translate french buddy_aftermath_3eb4a9b6:

    # m "I just think... you should be careful around one of them."
    m "Je oense juste... que tu devrais faire attention quand tu es autour d'eux."

# game/dinner_2.rpy:255
translate french buddy_aftermath_9faf3829:

    # m "Jack might, you know, try to recruit you."
    m "Jack pourrait, tu sais, essayer de te recruter."

# game/dinner_2.rpy:272
translate french buddy_aftermath_2_182078cd:

    # n "How do you even..."
    n "Comment est-ce que..."

# game/dinner_2.rpy:273
translate french buddy_aftermath_2_e87c3a68:

    # n "Ugh, nevermind."
    n "Ugh, oublie ça."

# game/dinner_2.rpy:274
translate french buddy_aftermath_2_5c44a03c:

    # m "Nick, I'm sorry you find me annoying."
    m "Nick, je susi désolée si tu me trouves ennuyeuse."

# game/dinner_2.rpy:275
translate french buddy_aftermath_2_53906854:

    # n "No, mom, stop doing th--"
    n "Non, maman, arrête de--"

# game/dinner_2.rpy:276
translate french buddy_aftermath_2_5d738309:

    # m "Let's go back to talking about your grades."
    m "Revenons à tes notes."

# game/dinner_2.rpy:277
translate french buddy_aftermath_2_884e900d:

    # m "Now, what did you say you were studying tomorrow?"
    m "Qu'est-ce que t'as dit que tu révisais, demain ?"

# game/dinner_2.rpy:280
translate french buddy_aftermath_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_2.rpy:281
translate french buddy_aftermath_2_18f93e1b:

    # n "Errrmmmmm..."
    n "Heuuuuuu..."

# game/dinner_2.rpy:298
translate french grades_start_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:305
translate french grades_start_1_9367b59c:

    # m "You first told me it was [studying_subject]."
    m "Tu m'avais parlé de révisions en [studying_subject!tl]."

# game/dinner_2.rpy:306
translate french grades_start_1_9cdd6049:

    # m "Now you tell me it's [studying_subject_2]?"
    m "Maintenant c'est des révisions en [studying_subject_2!tl] ?"

# game/dinner_2.rpy:308
translate french grades_start_1_86028b03:

    # n "Mom, I was just confus--"
    n "Maman, je me suis juste trom--"

# game/dinner_2.rpy:310
translate french grades_start_1_77d768d3:

    # m "This is TWICE you've lied to me during this dinner."
    m "C'est la DEUXIÈME fois que tu m'as menti ce soir."

# game/dinner_2.rpy:311
translate french grades_start_1_43353a2c:

    # n "I didn't lie about--"
    n "Je n'ai pas menti sur--"

# game/dinner_2.rpy:312
translate french grades_start_1_4bd39f58:

    # m "Either way, your grades in both subjects are terrible."
    m "De toute façon, tes notes sont mauvaises pour les deux."

# game/dinner_2.rpy:313
translate french grades_start_1_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_2.rpy:317
translate french grades_start_2_9d218a36:

    # m "You hesitated for a moment there."
    m "Tu as hésité."

# game/dinner_2.rpy:318
translate french grades_start_2_2d5c5a7d:

    # n "I was eating."
    n "Je mange."

# game/dinner_2.rpy:319
translate french grades_start_2_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_2.rpy:321
translate french grades_start_2_d17fbb11:

    # m "I wonder if you're studying with Jack at all, or just always hanging out."
    m "Je me demande si tu révises vraiment avec Jack, ou si vous ne faites que traîner ensemble."

# game/dinner_2.rpy:322
translate french grades_start_2_15d84e14:

    # n "We study."
    n "On révise."

# game/dinner_2.rpy:323
translate french grades_start_2_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_2.rpy:324
translate french grades_start_2_5bbd9e76:

    # m "Still, your grades in your [studying_subject_2] class are terrible."
    m "De toute façon, tes notes en [studying_subject_2] sont très mauvaises."

# game/dinner_2.rpy:325
translate french grades_start_2_24796dbe:

    # n ". . ."
    n ". . ."

translate french strings:

    # game/dinner_2.rpy:23
    old "Cool. Let's eat."
    new "Cool. On mange, alors."

    # game/dinner_2.rpy:23
    old "I have something to tell both of you."
    new "Il y a quelque chose que je dois vous dire, à tous les deux."

    # game/dinner_2.rpy:23
    old "There's something I need to tell just you first."
    new "Il y a quelque chose que je dois te dire."

    # game/dinner_2.rpy:49
    old "Chemistry."
    new "Chimie."

    # game/dinner_2.rpy:49
    old "Calculus."
    new "Calcul."

    # game/dinner_2.rpy:49
    old "Compsci."
    new "Info."

    # game/dinner_2.rpy:72
    old "We just study together, that's all."
    new "On révise ensemble, c'est tout."

    # game/dinner_2.rpy:72
    old "Mom, Jack is... more than a friend."
    new "Maman, Jack est... plus qu'un ami."

    # game/dinner_2.rpy:72
    old "Well yeah, that's what good pals do."
    new "Ouais, c'est ça qu'on fait avec des amis."

    # game/dinner_2.rpy:124
    old "Oops, I meant he's just a studymate."
    new "Oups, je veux dire, c'est juste un camarade de classe."

    # game/dinner_2.rpy:124
    old "Well, he can also be my friend..."
    new "Euh, ça ne l'empêche pas d'être mon ami..."

    # game/dinner_2.rpy:124
    old "No, I always said we were friends."
    new "Non, j'ai toujours dit qu'on était amis."

    # game/dinner_2.rpy:138
    old "Oh. No, yeah, we're just friends."
    new "Oh. Non, non, on est juste amis."

    # game/dinner_2.rpy:138
    old "The wrong idea might be the right idea."
    new "Des choses qui pourraient être vraies."

    # game/dinner_2.rpy:138
    old "What do you mean by... wrong idea?"
    new "Comment ça... des choses ?"

    # game/dinner_2.rpy:180
    old "You think he's gay."
    new "Tu penses qu'il est gay."

    # game/dinner_2.rpy:180
    old "Don't say that about my friend!"
    new "Ne dis pas ça de mon ami !"

    # game/dinner_2.rpy:180
    old "What do you mean, he's not natural?"
    new "Comment ça, pas naturel ?"

    # game/dinner_2.rpy:225
    old "Ha, he sure acts gay. Luckily, he's not."
    new "Ha oui, on dirait, mais heureusement il est pas gay."

    # game/dinner_2.rpy:225
    old "What's wrong with being gay?!"
    new "Où est le mal dans le fait d'être gay ?!"

    # game/dinner_2.rpy:225
    old "Maybe... my friend might be gay."
    new "Peut-être... que mon ami est gay."

    # game/dinner_2.rpy:260
    old "what."
    new "quoi."

    # game/dinner_2.rpy:260
    old "whaaat."
    new "quoiii."

    # game/dinner_2.rpy:260
    old "whaaaaaaaaaaaaaaat."
    new "quoiiiiiiiiiiiiiii."

    # game/dinner_2.rpy:283
    old "Compsci?"
    new "Info ?"

    # game/dinner_2.rpy:283
    old "Chemistry?"
    new "Chimie ?"

    # game/dinner_2.rpy:283
    old "Calculus?"
    new "Calcul ?"

    # game/dinner_2.rpy:51
    old "Chemistry"
    new "Chimie"

    # game/dinner_2.rpy:53
    old "Calculus"
    new "Calcul"

    # game/dinner_2.rpy:55
    old "Computer Science"
    new "Info"
