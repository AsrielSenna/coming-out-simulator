# game/outro.rpy:9
translate french start_outro_76b2fe88:

    # nvl clear
    nvl clear

# game/outro.rpy:23
translate french start_outro_dad8a777:

    # N "And then we broke up three days later."
    N "On a rompu trois jours plus tard."

# game/outro.rpy:25
translate french start_outro_07c27814:

    # N "And then we broke up three weeks later."
    N "On a rompu trois semaines plus tard."

# game/outro.rpy:29
translate french start_outro_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:30
translate french start_outro_9bd56d5e:

    # N "Told you this didn't end in gay unicorns."
    N "Je t'ai dit que ça ne finissait pas avec des licornes gayes."

# game/outro.rpy:32
translate french start_outro_875ce35a_1:

    # p ". . ."
    p ". . ."

# game/outro.rpy:33
translate french start_outro_8bc2b463:

    # N "Told you. Not blood, but tears."
    N "Je te l'avais dit. Pas du sang, des larmes."

# game/outro.rpy:35
translate french start_outro_875ce35a_2:

    # p ". . ."
    p ". . ."

# game/outro.rpy:36
translate french start_outro_b79b0c19:

    # N "You were right. I'm a bit of a downer."
    N "T'avais raison, je suis un peu pessimiste."

# game/outro.rpy:40
translate french start_outro_52bbb990:

    # p "MY FEELS."
    p "MES ÉMOTIONS."

# game/outro.rpy:41
translate french start_outro_24032598:

    # N "Let the feels flow, my friend."
    N "Eh oui."

# game/outro.rpy:44
translate french start_outro_49bb6b97:

    # p "Aw, come on, that's cold dude."
    p "Oh, franchement, c'est triste, mec."

# game/outro.rpy:45
translate french start_outro_e4bbfd64:

    # N "I don't deny that."
    N "Je ne le nie pas."

# game/outro.rpy:48
translate french start_outro_459fb1e5:

    # p "Can't say I didn't see that coming..."
    p "Je ne peux pas dire que je ne l'avais pas vu venir..."

# game/outro.rpy:49
translate french start_outro_6b56f091:

    # N "Yeah... Jack and I saw it coming, too."
    N "Ouais... Jack et moi, on l'a vu venir aussi."

# game/outro.rpy:58
translate french closure_30f9bb1a:

    # p "Ugh."
    p "Ugh."

# game/outro.rpy:59
translate french closure_ee0febae:

    # p "I feel gross just using the same-coloured dialogue balloons as the Father character."
    p "Je me sens sale d'avoir des dialogues de la même couleur que le personnage du père."

# game/outro.rpy:64
translate french closure_3fa56ce7:

    # N "Which reminds me. Many of the characters have been swapped around."
    N "Ce qui me fait penser. Beaucoup des personnages ont été mélangés."

# game/outro.rpy:65
translate french closure_af1d1f08:

    # N "All names have been changed, except mine."
    N "Tous les noms ont été changés, sauf le mien."

# game/outro.rpy:66
translate french closure_d746dbe0:

    # N "I left my little brother out entirely, because he's innocent."
    N "J'ai écarté mon petit frère, parce qu'il est innocent."

# game/outro.rpy:67
translate french closure_b6a54992:

    # N "And I put my Father back in, even though he'd left the family long before 2010."
    N "Et j'ai ajouté mon père, même si il avait quitté la famille bien avant 2010."

# game/outro.rpy:70
translate french closure_a6eb3b54:

    # N "Like you said, this 'true' game is full of lies."
    N "Comme tu l'as dit, ce 'vrai' jeu est plein de mensonges."

# game/outro.rpy:72
translate french closure_85070830:

    # p "You could have at least given me a different colour."
    p "Tu aurais au moins pu me donner une différente couleur."

# game/outro.rpy:73
translate french closure_376cfb5e:

    # N "It's been four years since that night..."
    N "Ça fait déjà quatre ans depuis ce soir-là..."

# game/outro.rpy:74
translate french closure_03582906:

    # N "What do you think happened afterwards?"
    N "Qu'est-ce que tu penses qu'il s'est passé après ?"

# game/outro.rpy:77
translate french closure_12c11f0c:

    # N "Don't worry. Like we said in the Main Menu, there are no right answers."
    N "Ne t'inquiète pas. Comme je te l'ai dit dans le menu principal, il n'y a pas de bonne réponse."

# game/outro.rpy:83
translate french closure_7843a3da:

    # p "Dude, I dunno, just freaking tell me."
    p "Je sais pas, mec, dis-moi."

# game/outro.rpy:84
translate french closure_cc5ceecf:

    # N "Alright, I will tell you what happened."
    N "Ok, je vais te dire ce qui s'est passé."

# game/outro.rpy:85
translate french closure_4d15693d:

    # N "...and what happened, and what happened."
    N "...et ce qui s'est passé, et ce qui s'est passé."

# game/outro.rpy:86
translate french closure_65933bc6:

    # p "What."
    p "Quoi."

# game/outro.rpy:89
translate french closure_e71cf15a:

    # p "Let me guess, It Gets Better™?"
    p "Laisse-moi deviner, Ça Va Mieux™ ?"

# game/outro.rpy:90
translate french closure_1a7f905d:

    # N "Yes, actually! In all three versions of what happened."
    N "Eh oui ! Dans les trois versions de ce qui s'est passé."

# game/outro.rpy:91
translate french closure_65933bc6_1:

    # p "What."
    p "Quoi."

# game/outro.rpy:94
translate french closure_3cd73952:

    # p "Flowers and rainbows and gay unicorns?"
    p "Des fleurs et des licornes gayes ?"

# game/outro.rpy:95
translate french closure_e12d870c:

    # N "Yes, actually! At least, in one of my three versions of what happened."
    N "Eh oui ! Enfin, dans une des trois versions."

# game/outro.rpy:96
translate french closure_d8a3ffd9:

    # p "Of course."
    p "Bien sûr."

# game/outro.rpy:102
translate french closure_story_4ed65aaa:

    # N "Which post-coming-out story do you want to hear first?"
    N "Quelle histoire post-coming-out veux-tu entendre en premier ?"

# game/outro.rpy:103
translate french closure_story_e110ad88:

    # N "Don't worry, you'll get to hear all three of them."
    N "Ne t'inquiète pas, tu auras accès aux trois."

# game/outro.rpy:105
translate french closure_story_02d9562b:

    # N "Now, which version do you want to hear next?"
    N "Maintenant, quelle version veux-tu entendre ?"

# game/outro.rpy:107
translate french closure_story_41229321:

    # N "Finally, let's hear the last story..."
    N "Allez, écoute la dernière version, maintenant..."

# game/outro.rpy:125
translate french is_last_story_dfa1bcb1:

    # p "Again, with the making the only option a clickable option..."
    p "Encore ce délire de rendre cliquable la seule option..."

# game/outro.rpy:127
translate french is_last_story_6be3e892:

    # p "Why did you make that a clickable option, when it was the only option left."
    p "Pourquoi est-ce que tu en as fait une option cliquable, alors qu'il n'y en a aucune autre."

# game/outro.rpy:128
translate french is_last_story_e026d464:

    # N "No idea. Moving on."
    N "Aucune idée."

# game/outro.rpy:143
translate french tell_me_a_lie_b101af35:

    # N "Very well."
    N "Très bien."

# game/outro.rpy:146
translate french tell_me_a_lie_3994de86:

    # N "I ran away from home, with nothing more than a luggage bag full of edible underwear."
    N "Je me suis enfui de chez moi, avec rien d'autre qu'une valise pleine de sous-vêtements comestibles."

# game/outro.rpy:148
translate french tell_me_a_lie_a28f0bec:

    # N "I roamed the Great White North. Supporting myself by writing amateur poetry for strangers."
    N "J'ai traversé le Grand Nord. En gagnant ma vie en écrivant de la poésie amateur aux étrangers."

# game/outro.rpy:150
translate french tell_me_a_lie_0f5f366a:

    # N "I roamed the Great White North. Supporting myself by making not-fun web games."
    N "J'ai traversé le Grand Nord. En gagnant ma vie en faisant des jeux pas drôles."

# game/outro.rpy:151
translate french tell_me_a_lie_161cad31:

    # N "I ate flowers. Followed the rainbows. And befriended a homosexual unicorn."
    N "J'ai mangé des fleurs. Suivi les arcs-en-ciel. Et devenu ami avec une licorne homosexuelle."

# game/outro.rpy:152
translate french tell_me_a_lie_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:153
translate french tell_me_a_lie_a1972e18:

    # N "Eventually I made it to Alaska, where I met an adult bisexual couple named Bonnie & Clyde."
    N "J'ai fini en Alaska, où j'ai rencontré un couple d'adultes bisexuels appelés Bonnie & Clyde."

# game/outro.rpy:154
translate french tell_me_a_lie_fe48c5b7:

    # N "Bonnie was a mid-30s cougar, and Clyde was an early-40s manther."
    N "Bonnie était une cougar avec la trentaine, et Clyde un manther avec la quarantaine."

# game/outro.rpy:162
translate french tell_me_a_lie_b4c72c52:

    # p "I guess edible undies are both food & clothing."
    p "Des sous-vêtements comestibles..."

# game/outro.rpy:163
translate french tell_me_a_lie_d300a4eb:

    # N "And thanks to my flexibility, the luggage bag doubles as housing!"
    N "Et grâce à ma flexibilité, la valise fait aussi maison !"

# game/outro.rpy:167
translate french tell_me_a_lie_ab0fe3b1:

    # p "This story is a fractal of fracked up."
    p "Cette histoire est une fractale de n'imp."

# game/outro.rpy:168
translate french tell_me_a_lie_33ef7059:

    # N "MY STORY. MY RULES."
    N "MON HISTOIRE. MOI QUI DÉCIDE."

# game/outro.rpy:172
translate french tell_me_a_lie_e4ab1048:

    # p "...\"manther\"."
    p "...\"manther\"."

# game/outro.rpy:173
translate french tell_me_a_lie_fed1942a:

    # N "Also known as a faguar."
    N "Aussi appelé faguar."

# game/outro.rpy:178
translate french tell_me_a_lie_2_ad835409:

    # N "They took me in as their foster child, and I was their full-time boytoy."
    N "Il m'ont pris comme fils adoptif, et comme fuckboy à plein temps."

# game/outro.rpy:181
translate french tell_me_a_lie_2_ca8ac6a1:

    # p "...Thanks again to your, uh, flexibility."
    p "...Grâce à ta, euh, flexibilité."

# game/outro.rpy:184
translate french tell_me_a_lie_2_1f3a7467:

    # N "As we know, I like having my partners be 'the woman' of a relationship."
    N "Comme nous le savons, j'aime que mes partenaires soient 'la femme' dans une relation."

# game/outro.rpy:186
translate french tell_me_a_lie_2_3f5e7a05:

    # N "As we know, I'm usually 'the woman' of a relationship."
    N "Comme nous le savons, je suis généralement 'la femme' dans une relation."

# game/outro.rpy:188
translate french tell_me_a_lie_2_f3eab5f5:

    # N "As we know, I like taking turns at being 'the woman' of a relationship."
    N "Comme nous le savons, j'aime qu'on se relaie pour être 'la femme' dans une relation."

# game/outro.rpy:190
translate french tell_me_a_lie_2_1d2e649c:

    # N "They raised me, showed me love, and I grew up to be a productive member of society."
    N "Ils m'ont élevé, m'ont montré l'amour, et j'ai grandi en devenant un élément productif de la société."

# game/outro.rpy:193
translate french tell_me_a_lie_2_5940a340:

    # p "And when you zoom in on this fractal, there's MORE fracked-up-ness."
    p "Et quand tu zoomes sur cette fractale, il y a encore PLUS de n'imp."

# game/outro.rpy:195
translate french tell_me_a_lie_2_649512c5:

    # p "...\"MANTHER\"."
    p "...\"MANTHER\"."

# game/outro.rpy:197
translate french tell_me_a_lie_2_4242db1d:

    # N "They were my new family."
    N "Ils étaient ma nouvelle famille."

# game/outro.rpy:198
translate french tell_me_a_lie_2_af242bb2:

    # N "Family... with benefits."
    N "Une famille... avec des choses en plus."

# game/outro.rpy:200
translate french tell_me_a_lie_2_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:219
translate french tell_me_a_truth_8cd9b214:

    # N "Here it goes."
    N "C'est parti."

# game/outro.rpy:222
translate french tell_me_a_truth_1e6f68c2:

    # N "I took Jack's advice and parodied Inception in my 'odd web game', Reimagine :The Game:."
    N "J'ai suivi le conseil de Jack en parodiant Inception dans mon 'jeu en ligne bizarre', Reimagine :The Game:."

# game/outro.rpy:224
translate french tell_me_a_truth_c4a32375:

    # N "Didn't say that Cobbs was awake in the ending, though."
    N "Enfin, je n'ai pas dit que Cobbs était réveillé à la fin."

# game/outro.rpy:226
translate french tell_me_a_truth_22c6a594:

    # N "Didn't say that the movie was all just a dream, though."
    N "Enfin, je n'ai pas dit que le film n'était qu'un rêve."

# game/outro.rpy:228
translate french tell_me_a_truth_bef99e8d:

    # N "Still think it doesn't matter if Cobbs was still dreaming."
    N "Et je pense toujours que ça ne change rien si Cobbs était en train de dormir ou pas."

# game/outro.rpy:229
translate french tell_me_a_truth_5034b63b:

    # N "Reimagine :The Game: got internet-famous-ish! A good portfolio piece."
    N "Reimagine :The Game: est devenu un peu connu sur internet ! De quoi remplir mon CV."

# game/outro.rpy:230
translate french tell_me_a_truth_3dfbf359:

    # N "A few months later, I landed an internship at Electronic Arts in the Bay Area. Far away from my family in Canada."
    N "Quelques mois plus tard, j'ai décroché un stage à Electronic Arts, bien loin de ma famille au Canada."

# game/outro.rpy:235
translate french tell_me_a_truth_4e91299c:

    # p "Eww, Electronic Arts...?"
    p "Euh, Electronic Arts...?"

# game/outro.rpy:237
translate french tell_me_a_truth_02f21d06:

    # N "Yeah, I know, I know."
    N "Ouais, je sais, je sais."

# game/outro.rpy:238
translate french tell_me_a_truth_dc18797c:

    # N "I'm now repenting for my sins by making artsy-fartsy indie games like this one."
    N "Maintenant je compense mes péchés en faisant des jeux indés comme celui-là."

# game/outro.rpy:239
translate french tell_me_a_truth_e9f152b7:

    # p "Repent harder, dammit."
    p "Compense mieux."

# game/outro.rpy:243
translate french tell_me_a_truth_c120cd05:

    # p "And the Bay Area is very LGBT friendly."
    p "Et la région était très LGBT-friendly."

# game/outro.rpy:245
translate french tell_me_a_truth_4242f24d:

    # N "That's why they call it the Gay Area!"
    N "Oui !"

# game/outro.rpy:246
translate french tell_me_a_truth_65372c72:

    # p "Uh.. nobody calls it that."
    pass

# game/outro.rpy:250
translate french tell_me_a_truth_65f238ba:

    # p "Oh, I love EA! They make The Sims, right?"
    p "Oh, j'adore EA ! Ils ont fait Les Sims, non ?"

# game/outro.rpy:252
translate french tell_me_a_truth_8688167a:

    # N "Yup! I didn't work on those, though. Our team was making a web game version of--"
    N "Yup ! Mais je n'ai pas travaillé dessus. Notre équipe faisait une version web de--"

# game/outro.rpy:253
translate french tell_me_a_truth_983e51e2:

    # N "[[LITERALLY CANNOT DISCLOSE]"
    N "[[CLAUSE DE CONFIDENTIALITÉ]"

# game/outro.rpy:254
translate french tell_me_a_truth_fd5f55ba:

    # p "Oh."
    p "Oh."

# game/outro.rpy:259
translate french tell_me_a_truth_2_35fcf395:

    # N "After EA, I went on to go indie."
    N "Après EA, je suis parti en indé."

# game/outro.rpy:260
translate french tell_me_a_truth_2_77bf8f8c:

    # N "But I stayed in touch with friends at EA, and stayed in the Bay Area."
    N "Mas je suis resté en contact avec des amis à EA, et je suis resté dans la région."

# game/outro.rpy:262
translate french tell_me_a_truth_2_b378a962:

    # N "My technical skills grew."
    N "Mes compétences techniques se sont améliorées."

# game/outro.rpy:263
translate french tell_me_a_truth_2_caebf79d:

    # N "My social skills grew."
    N "Mes compétences sociales se sont améliorées."

# game/outro.rpy:264
translate french tell_me_a_truth_2_f437b274:

    # N "And here... I'm finally starting to figure out my identity."
    N "Et là... je commence à me questionner sur mon identité."

# game/outro.rpy:267
translate french tell_me_a_truth_2_691ce03b:

    # p "Well, I'm looking forward to Literally Cannot Disclose: The Game."
    p "Eh bien, j'ai hâte de jouer à Clause de Confidentialité : Le Jeu."

# game/outro.rpy:269
translate french tell_me_a_truth_2_717aaa22:

    # p "But seriously, no one calls it the Gay Area."
    pass

# game/outro.rpy:271
translate french tell_me_a_truth_2_f2cc4f17:

    # p "But seriously, ew. Electronic Arts."
    p "Mais sérieux, ew. Electronic Arts."

# game/outro.rpy:289
translate french tell_me_a_half_truth_17c77b94:

    # N "As you wish."
    N "Comme tu veux."

# game/outro.rpy:292
translate french tell_me_a_half_truth_4c9d3567:

    # N "Claire, in an ironic twist of fate, was also bisexual."
    N "Claire, par une ironie du sort, était aussi bisexuelle."

# game/outro.rpy:293
translate french tell_me_a_half_truth_0db3ea55:

    # N "We told each other about it during a [studying_subject] study session."
    N "On se l'est dit pendant une révision en [studying_subject!tl]."

# game/outro.rpy:295
translate french tell_me_a_half_truth_c0e12604:

    # p "What a twist!"
    p "Quel twist !"

# game/outro.rpy:297
translate french tell_me_a_half_truth_f1dca464:

    # N "Claire was insecure about her sexual orientation, like me."
    N "Claire était mal à l'aise sur son orientation sexuelle, comme moi."

# game/outro.rpy:298
translate french tell_me_a_half_truth_b02622f9:

    # N "We were both somewhat inexperienced. Claire's only been with women, and I've only been with Jack."
    N "On manquait tous les deux d'expérience. Claire avait seulement été avec des filles, et moi seulement avec Jack."

# game/outro.rpy:306
translate french tell_me_a_half_truth_abaa3d3e:

    # p "A mirror version of you, but reversed..."
    p "Une version miroir de toi, mais à l'envers..."

# game/outro.rpy:307
translate french tell_me_a_half_truth_7cb59c93:

    # N "Well, uh, all mirror images are reversed."
    N "Le principe d'un miroir, c'est d'être à l'envers."

# game/outro.rpy:308
translate french tell_me_a_half_truth_bfde4c00:

    # p "You know what I mean."
    p "Tu comprends ce que je veux dire."

# game/outro.rpy:309
translate french tell_me_a_half_truth_f969cf18:

    # N "But yeah, Claire and I shared our experiences with one another."
    N "Mais oui, Claire et moi, on s'est partagé nos expériences."

# game/outro.rpy:313
translate french tell_me_a_half_truth_f94c7664:

    # p "So, you taught each other the other side?"
    p "Donc, vous vous êtes donné des conseils ?"

# game/outro.rpy:317
translate french tell_me_a_half_truth_ed325587:

    # p "Did you end up having sexytimes together?"
    p "Vous avez fini par faire des trucs ensemble ?"

# game/outro.rpy:318
translate french tell_me_a_half_truth_6b20017d:

    # N "No. She's like a sister to me. A sister I would not have sex with."
    N "Non. C'est comme une soeur pour moi. Une soeur avec qui je ne voudrais pas faire ça."

# game/outro.rpy:319
translate french tell_me_a_half_truth_d2608d20:

    # p "You... did not need to clarify that."
    p "Tu... n'avais pas besoin de clarifier ça"

# game/outro.rpy:320
translate french tell_me_a_half_truth_f969cf18_1:

    # N "But yeah, Claire and I shared our experiences with one another."
    N "Mais oui, Claire et moi, on a échangé des conseils."

# game/outro.rpy:325
translate french tell_me_a_half_truth_2_d6937684:

    # N "And exchanged tips!"
    N "Plein de conseils !"

# game/outro.rpy:326
translate french tell_me_a_half_truth_2_894c732b:

    # N "Like... do a 'come hither' motion with your fingers, or, rub the head against the roof of your mouth."
    N "Comme... faire 'viens par ici' avec tes doigts, ou, frotter le haut sur ton palais."

# game/outro.rpy:327
translate french tell_me_a_half_truth_2_badab66e:

    # p "T.M.I, dude..."
    p "Trop d'infos, mec..."

# game/outro.rpy:330
translate french tell_me_a_half_truth_2_b70a49f3:

    # N "I did move to her school, in the end."
    N "Je suis allé dans son école, au final."

# game/outro.rpy:332
translate french tell_me_a_half_truth_2_bf675268:

    # N "We were best friends. We still are! We've now both moved to the US, far away from our hateful families."
    N "C'était ma meilleure amie. On l'est toujours ! On a tous les deux déménagé aux US, loin de nos familles horribles."

# game/outro.rpy:333
translate french tell_me_a_half_truth_2_e9bc5840:

    # N "Together, we helped each other overcome our insecurities, and discover who we were..."
    N "Ensemble, on a dépassé ce qui nous rendait mal à l'aise, et on a découvert qui on était vraiment..."

# game/outro.rpy:334
translate french tell_me_a_half_truth_2_6306b45b:

    # N "Proud bisexual sluts."
    N "Des petites salopes bisexuelles, et fières."

# game/outro.rpy:336
translate french tell_me_a_half_truth_2_13c683e7:

    # p "What a touching story. I think."
    p "C'est une histoire touchante. Je crois."

# game/outro.rpy:338
translate french tell_me_a_half_truth_2_244514b5:

    # N "And of course, we wingman/wingwoman for each other."
    pass

# game/outro.rpy:340
translate french tell_me_a_half_truth_2_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:350
translate french finale_1_f8954381:

    # N "And that's the last of the post-coming-out stories!"
    N "Et c'est la dernière de mes histoires post-coming-out !"

# game/outro.rpy:354
translate french finale_1_76b2fe88:

    # nvl clear
    nvl clear

# game/outro.rpy:365
translate french finale_1_3f91174c:

    # N "Dear player, I couldn't help but notice..."
    N "Cher joueur, j'ai bien remarqué que..."

# game/outro.rpy:367
translate french finale_1_d3242cea:

    # N "You went straight for the Truth first."
    N "Tu voulais directement entendre la Vérité."

# game/outro.rpy:369
translate french finale_1_d95ee36a:

    # N "You saved the Truth for last."
    N "Tu as gardé la Vérité pour la fin."

# game/outro.rpy:371
translate french finale_1_44110c23:

    # N "You wanted to hear the Lie first."
    N "Tu voulais directement entendre le Mensonge."

# game/outro.rpy:373
translate french finale_1_25d1f507:

    # N "You saved the Lie for last."
    N "Tu as gardé le Mensonge pour la fin."

# game/outro.rpy:374
translate french finale_1_fba79215:

    # N "What does that say about you?..."
    N "Qu'est-ce que ça veut dire sur toi ?..."

# game/outro.rpy:375
translate french finale_1_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:377
translate french finale_1_d8ebf6d0:

    # p "You know... usually when a game gives you multiple endings, they don't do them ALL AT ONCE."
    p "Tu sais... d'habitude, quand un jeu a plusieurs, on ne te les propose pas TOUTES EN MÊME TEMPS."

# game/outro.rpy:378
translate french finale_1_3e6975de:

    # N "Hah! You thought these were ENDINGS?"
    N "Hah ! Tu penses que c'était des FINS ?"

# game/outro.rpy:382
translate french finale_1_ab35cfee:

    # p "Let me guess... This Is Just The Beginning?"
    p "Laisse-moi deviner... Ce n'Est Que Le Début ?"

# game/outro.rpy:383
translate french finale_1_aa831f90:

    # N "This is just the begi-- oh. Okay, yeah."
    N "Ce n'est que le déb-- oh. Ok, ouais."

# game/outro.rpy:386
translate french finale_1_22edde43:

    # p "Well yeah. This game's over, right?"
    p "Bah oui. Le jeu est fini, non ?"

# game/outro.rpy:387
translate french finale_1_f2fa8af2:

    # N "True... but the story, which is my story, my life, continues."
    N "Oui... Mais l'histoire, ma vie, elle continue."

# game/outro.rpy:390
translate french finale_1_be2d2542:

    # p "oh god how long IS this damn game."
    p "Oh non, il finit quand ce jeu."

# game/outro.rpy:391
translate french finale_1_bbe2def8:

    # N "Don't worry. Your next choice is the very last one, I swear."
    N "Ne t'inquiète pas. Le prochain choix est le dernier, promis."

# game/outro.rpy:398
translate french finale_2_e3ba31fc:

    # N ". . ."
    N ". . ."

# game/outro.rpy:399
translate french finale_2_5f5ff254:

    # N "You know, if I could go back and relive all my other possible choices..."
    N "Tu sais, si je pouvais revenir en arrière et revivre tous les choix possibles..."

# game/outro.rpy:400
translate french finale_2_cd3f53de:

    # N "... which in a sense, I did, by writing this game..."
    N "... ce que j'ai fait, quelque part, en écrivant ce jeu..."

# game/outro.rpy:401
translate french finale_2_50cc4cb1:

    # N "... I wouldn't change a thing."
    N "... je ne changerais rien."

# game/outro.rpy:409
translate french finale_2_e91f7334:

    # p "? ? ?"
    p "? ? ?"

# game/outro.rpy:412
translate french finale_2_a343674b:

    # N "My texts getting read. Being forced to change schools. Getting punched in the face."
    N "Mes messages lus. Devoir changer d'école. Me faire frapper."

# game/outro.rpy:414
translate french finale_2_fc2b8b8d:

    # N "My texts getting read. Being forced to change schools. All the verbal abuse."
    N "Mes messages lus. Devoir changer d'école. La violence verbale."

# game/outro.rpy:416
translate french finale_2_de80866f:

    # N "My texts getting read. Being forced to change schools. The attempted 'gay rehab' with Claire."
    N "Mes messages lus. Devoir changer d'école. Ma 'cure de désintox' avec Claire."

# game/outro.rpy:418
translate french finale_2_517161f4:

    # N "My texts getting read. No more after-school hours to myself. The attempted 'gay rehab' with Claire."
    N "Mes messages lus. Plus de temps libre après les cours. Ma 'cure de désintox' avec Claire."

# game/outro.rpy:420
translate french finale_2_a87b7cc9:

    # N "In a Stockholm Syndrome sort of sense... I'm grateful for it all."
    N "Une sorte de syndrome de Stockholm, quelque part... Mais je suis content."

# game/outro.rpy:424
translate french finale_2_9dc9366e:

    # p "what."
    p "quoi."

# game/outro.rpy:427
translate french finale_2_8417b81f:

    # p "whaaat."
    p "quoiii."

# game/outro.rpy:430
translate french finale_2_3a02b42d:

    # p "whaaaaaaaaaaaaaaat."
    p "quoiiiiiiiiiiiiiii."

# game/outro.rpy:438
translate french finale_3_ecadc2e1:

    # N "Yes, really!"
    N "Oui, vraiment !"

# game/outro.rpy:439
translate french finale_3_796981b8:

    # N "I wouldn't have been so motivated to forge my own life... if my previous life wasn't total utter shit."
    N "Je n'aurais pas été si motivé pour refaire ma vie... si ma vie précédente n'avait pas été un échec total."

# game/outro.rpy:444
translate french finale_3_ecd4ca3c:

    # N "Later in 2010, Dan Savage launched the It Gets Better™ campaign."
    N "Plus tard en 2010, Dan Savage a lancé la campagne It Gets Better™."

# game/outro.rpy:445
translate french finale_3_c2305bbb:

    # N "My three stories... Lie, Truth, Half-Truth... they're all at least true about one thing."
    N "Mes trois histoires... Mensonge, Vérité, Demi-Vérité... elles sont au moins vraies sur un point."

# game/outro.rpy:446
translate french finale_3_f2f94339:

    # N "It does get better."
    N "Ça s'arrange vraiment."

# game/outro.rpy:448
translate french finale_3_875ce35a:

    # p ". . ."
    p ". . ."

# game/outro.rpy:450
translate french finale_3_d8045160:

    # N "And..."
    N "Et..."

# game/outro.rpy:451
translate french finale_3_92c520a8:

    # N "At the end..."
    N "À la fin..."

# game/outro.rpy:452
translate french finale_3_8222fc51:

    # N "Of this long, stupid, painful game..."
    N "De ce jeu long, stupide et douloureux..."

# game/outro.rpy:453
translate french finale_3_c13640ec:

    # N "Where I played against people who should have been on my side..."
    N "Où j'ai joué contre des gens qui auraient dû être de mon côté..."

# game/outro.rpy:455
translate french finale_3_875ce35a_1:

    # p ". . ."
    p ". . ."

# game/outro.rpy:457
translate french finale_3_e807b918:

    # N "I won."
    N "J'ai gagné."

# game/outro.rpy:458
translate french finale_3_e3ba31fc:

    # N ". . ."
    N ". . ."

# game/outro.rpy:459
translate french finale_3_e807b918_1:

    # N "I won."
    N "J'ai gagné."

# game/outro.rpy:463
translate french finale_3_76b2fe88:

    # nvl clear
    nvl clear

# game/outro.rpy:491
translate french finale_3_31e83ddd:

    # p "REPLAY?"
    p "REJOUER ?"

# game/outro.rpy:496
translate french finale_4_669143cf:

    # N "Real life has no replays."
    N "Dans la vraie vie, on ne peut pas rejouer."

translate french strings:

    # game/outro.rpy:38
    old "MY FEELS."
    new "MES ÉMOTIONS."

    # game/outro.rpy:38
    old "Aw, come on, that's cold dude."
    new "Oh, franchement, c'est triste, mec."

    # game/outro.rpy:38
    old "Can't say I didn't see that coming..."
    new "Je ne peux pas dire que je ne l'avais pas vu venir..."

    # game/outro.rpy:81
    old "Dude, I dunno, just freaking tell me."
    new "Je sais pas, mec, dis-moi."

    # game/outro.rpy:81
    old "Let me guess, It Gets Better™?"
    new "Laisse-moi deviner, Ça Va Mieux™ ?"

    # game/outro.rpy:81
    old "Flowers and rainbows and gay unicorns?"
    new "Des fleurs et des licornes gayes ?"

    # game/outro.rpy:111
    old "The Lie."
    new "Le Mensonge."

    # game/outro.rpy:111
    old "The Truth."
    new "La Vérité."

    # game/outro.rpy:111
    old "The Half-Truth."
    new "La Demi-Vérité."

    # game/outro.rpy:159
    old "I guess edible undies are both food & clothing."
    new "Des sous-vêtements comestibles..."

    # game/outro.rpy:159
    old "This story is a fractal of fracked up."
    new "Cette histoire est une fractale de n'imp."

    # game/outro.rpy:159
    old "...\"manther\"."
    new "...\"manther\"."

    # game/outro.rpy:232
    old "Eww, Electronic Arts...?"
    new "Euh, Electronic Arts...?"

    # game/outro.rpy:232
    old "And the Bay Area is very LGBT friendly."
    new "And the Bay Area is very LGBT friendly."

    # game/outro.rpy:232
    old "Oh, I love EA! They make The Sims, right?"
    new "Et la région était très LGBT-friendly."

    # game/outro.rpy:303
    old "A mirror version of you, but reversed..."
    new "Une version miroir de toi, mais à l'envers..."

    # game/outro.rpy:303
    old "So, you taught each other the other side?"
    new "Donc, vous vous êtes donné des conseils ?"

    # game/outro.rpy:303
    old "Did you end up having sexytimes together?"
    new "Vous avez fini par faire des trucs ensemble ?"

    # game/outro.rpy:380
    old "Let me guess... This Is Just The Beginning?"
    new "Laisse-moi deviner... Ce n'Est Que Le Début ?"

    # game/outro.rpy:380
    old "Well yeah. This game's over, right?"
    new "Bah oui. Le jeu est fini, non ?"

    # game/outro.rpy:380
    old "oh god how long IS this damn game."
    new "Oh non, il finit quand ce jeu."

    # game/outro.rpy:489
    old "REPLAY?"
    new "REJOUER ?"
