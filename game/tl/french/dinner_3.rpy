﻿# game/dinner_3.rpy:10
translate french start_dinner_3_dbb9e736:

    # n "Mom."
    n "Maman."

# game/dinner_3.rpy:14
translate french start_dinner_3_f8dadc27:

    # n "That's why I'm studying more with Jack."
    n "C'est pour ça que je travaille avec Jack."

# game/dinner_3.rpy:16
translate french start_dinner_3_282cbe3d:

    # n "Look, I'm trying. I really am."
    n "J'essaye. Je fais ce que je peux."

# game/dinner_3.rpy:18
translate french start_dinner_3_9204d693:

    # n "My grades are fine."
    n "Mes notes sont bien."

# game/dinner_3.rpy:22
translate french tutor_f2f36d8d:

    # m "I'm worried for you. Jack's not a good influence."
    m "Je m'inquiète pour toi. Jack n'est pas une bonne influence."

# game/dinner_3.rpy:25
translate french tutor_51d826e6:

    # m "I think his parents might even be drug addicts..."
    m "Je pense que ses parents sont drogués..."

# game/dinner_3.rpy:26
translate french tutor_cf40ef1d:

    # n "What makes you say th--"
    n "Qu'est-ce qui te fait dire--"

# game/dinner_3.rpy:28
translate french tutor_5fdeef12:

    # m "All he does is do poetry."
    m "Tout ce qu'il fait c'est de la poésie."

# game/dinner_3.rpy:29
translate french tutor_cf40ef1d_1:

    # n "What makes you say th--"
    n "Qu'est-ce qui te fait dire--"

# game/dinner_3.rpy:31
translate french tutor_13135e86:

    # m "I'm getting you a home tutor."
    m "Je vais te trouver quelqu'un pour te faire du soutien scolaire."

# game/dinner_3.rpy:32
translate french tutor_702cc02a:

    # n "...what?"
    n "...quoi ?"

# game/dinner_3.rpy:35
translate french tutor_3c635b2f:

    # m "She'll be tutoring you in [studying_subject] and [studying_subject_2]."
    m "Elle t'aidera en [studying_subject!tl] et en [studying_subject_2!tl]."

# game/dinner_3.rpy:37
translate french tutor_a3c56307:

    # m "She'll be tutoring you in [studying_subject]."
    m "Elle t'aidera en [studying_subject!tl]."

# game/dinner_3.rpy:39
translate french tutor_eb1407d8:

    # m "Her name is Claire. She's smart, pretty, and Caucasian. She's your age, too."
    m "Elle s'appelle Claire. Elle est intelligente, elle est jolie, et elle est Blanche. Et elle a ton âge."

# game/dinner_3.rpy:54
translate french tutor_seeing_955e2c6c:

    # m "I'm sorry, {i}seeing{/i} Jack?"
    m "attend, {i}aller voir{/i} Jack ?"

# game/dinner_3.rpy:55
translate french tutor_seeing_b298ff61:

    # m "Be careful how you say that. You make it sound like..."
    m "Attention comment tu dis ça. On dirait que..."

# game/dinner_3.rpy:59
translate french tutor_seeing_aee7f333:

    # n "Like we're dating? Yeah. We are."
    n "Qu'on sort ensemble ? Oui. C'est ça."

# game/dinner_3.rpy:60
translate french tutor_seeing_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:61
translate french tutor_seeing_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:62
translate french tutor_seeing_e30c1907:

    # n "...Hello?"
    n "...Allô ?"

# game/dinner_3.rpy:63
translate french tutor_seeing_4733cca1_1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:64
translate french tutor_seeing_5eb795a2:

    # n "Anyone there?"
    n "Il y a quelqu'un ?"

# game/dinner_3.rpy:65
translate french tutor_seeing_4733cca1_2:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:68
translate french tutor_seeing_dddad465:

    # n "I just meant meeting Jack."
    n "Je veux juste dire aller le voir."

# game/dinner_3.rpy:69
translate french tutor_seeing_c9805d96:

    # m "Okay. Just being clear about some things."
    m "Ok. Je voulais juste mettre les choses au clair."

# game/dinner_3.rpy:70
translate french tutor_seeing_2fe1bf98:

    # n "Yeah."
    n "Ouais."

# game/dinner_3.rpy:71
translate french tutor_seeing_4733cca1_3:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:72
translate french tutor_seeing_c5c1bb23:

    # m "Claire's really cute."
    m "Claire est très jolie."

# game/dinner_3.rpy:73
translate french tutor_seeing_463e0024:

    # n "Sure."
    n "Sûrement."

# game/dinner_3.rpy:74
translate french tutor_seeing_ff1321f4:

    # m "She has perky breasts."
    m "Elle a une belle poitrine."

# game/dinner_3.rpy:77
translate french tutor_seeing_81ff76c9:

    # n "We're. Not. Boyfriends."
    n "On. Ne sort pas. Ensemble."

# game/dinner_3.rpy:78
translate french tutor_seeing_4733cca1_4:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:79
translate french tutor_seeing_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_3.rpy:80
translate french tutor_seeing_5e1c97f7:

    # m "I never said you were, but... okay."
    m "Je n'ai pas dit que c'était le cas, mais... ok."

# game/dinner_3.rpy:81
translate french tutor_seeing_4b0689e9:

    # n "We're friends."
    n "On est amis."

# game/dinner_3.rpy:84
translate french tutor_seeing_024a0fe0:

    # m "\"Good pals\"..."
    m "\"Bon copains\"..."

# game/dinner_3.rpy:86
translate french tutor_seeing_8de50f3f:

    # m "\"BEST friends\"..."
    m "\"MEILLEURS amis\"..."

# game/dinner_3.rpy:92
translate french tutor_matchmake_a46e766b:

    # m "Well, if that's what you want, I could!"
    m "C'est possible, si c'est ce que tu veux !"

# game/dinner_3.rpy:93
translate french tutor_matchmake_0f2adcfa:

    # n "nooooo."
    n "nooooon."

# game/dinner_3.rpy:94
translate french tutor_matchmake_7528de50:

    # m "Don't be shy! You're growing up to be a man."
    m "Ne sois pas timide ! Tu grandis, tu vas devenir un homme."

# game/dinner_3.rpy:95
translate french tutor_matchmake_97711e71:

    # m "And you're going to give me lots of grandkids."
    m "Et tu vas me donner plein de petits-enfants."

# game/dinner_3.rpy:99
translate french tutor_matchmake_20289e40:

    # n "Stop it! I haven't even met Claire yet!"
    n "Stop ! Je n'ai même pas encore rencontré Claire !"

# game/dinner_3.rpy:100
translate french tutor_matchmake_74292204:

    # m "Yet!"
    m "Pas encore !"

# game/dinner_3.rpy:101
translate french tutor_matchmake_6df64eda:

    # m "She's coming over tomorrow!"
    m "Elle vient demain !"

# game/dinner_3.rpy:102
translate french tutor_matchmake_e98132cf:

    # n "What? But I promised Jack--"
    n "Quoi ? Mais j'ai promis à Jack--"

# game/dinner_3.rpy:103
translate french tutor_matchmake_9e804ed4:

    # m "I ironed your best clothes. You'll make a good first impression."
    m "J'ai repassé tes meilleurs vêtements. Tu lui feras une super première impression."

# game/dinner_3.rpy:109
translate french tutor_matchmake_403322c6:

    # n "The odds of that are 50-50, coz I'm bi."
    n "Pour ça, c'est un peu 50-50, parce que je suis bi."

# game/dinner_3.rpy:110
translate french tutor_matchmake_0804893a:

    # m "Um. Bi?..."
    m "Hum. Bi ?..."

# game/dinner_3.rpy:114
translate french tutor_matchmake_89ba8a6f:

    # n "Yes. As in BISEXUAL."
    n "Oui. Comme dans BISEXUEL."

# game/dinner_3.rpy:115
translate french tutor_matchmake_28042555:

    # n "As in I AM SEXUALLY ATTRACTED TO BOTH MEN AND WOMEN."
    n "Comme dans JE SUIS ATTIRÉ SEXUELLEMENT PAR LES HOMMES ET PAR LES FEMMES."

# game/dinner_3.rpy:116
translate french tutor_matchmake_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:117
translate french tutor_matchmake_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:120
translate french tutor_matchmake_685851cb:

    # n "No. I don't ever want to have kids."
    n "Non. Je ne veux jamais avoir d'enfants."

# game/dinner_3.rpy:121
translate french tutor_matchmake_e4d06447:

    # m "You'll change your mind when you grow up."
    m "Tu changeras d'avis en grandissant."

# game/dinner_3.rpy:122
translate french tutor_matchmake_7add27e8:

    # m "Raising a child is wonderful. Your children will look up to you!"
    m "C'est fantastique, d'élever un enfant. Tes enfants vont t'adorer !"

# game/dinner_3.rpy:123
translate french tutor_matchmake_1f2522d7:

    # n "...of course, you narcissist."
    n "...bien sûr, quel narcissique."

# game/dinner_3.rpy:124
translate french tutor_matchmake_896177e5:

    # m "Excuse me?"
    m "Pardon ?"

# game/dinner_3.rpy:125
translate french tutor_matchmake_ff0d382f:

    # n "Nothing."
    n "Non, rien."

# game/dinner_3.rpy:126
translate french tutor_matchmake_4733cca1_1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:131
translate french tutor_forget_9f5976a7:

    # m "No, because I've already scheduled Claire to come over tomorrow."
    m "Non, parce que j'ai déjà dit à Claire de venir demain."

# game/dinner_3.rpy:132
translate french tutor_forget_6a908319:

    # n "What?!"
    n "Quoi ?!"

# game/dinner_3.rpy:133
translate french tutor_forget_673f71df:

    # n "No. I promised to study with Jack tomorrow."
    n "Non. J'ai promis à Jack de réviser avec lui demain."

# game/dinner_3.rpy:134
translate french tutor_forget_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:135
translate french tutor_forget_edd83de1:

    # m "How long did you want to stay over at his place?"
    m "Tu veux rester chez Jack pendant combien de temps ?"

# game/dinner_3.rpy:139
translate french tutor_forget_ad446cc8:

    # n "Overnight."
    n "Passer la nuit."

# game/dinner_3.rpy:140
translate french tutor_forget_4733cca1_1:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:141
translate french tutor_forget_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:142
translate french tutor_forget_e30c1907:

    # n "...Hello?"
    n "...Allô ?"

# game/dinner_3.rpy:143
translate french tutor_forget_1c0af1fa:

    # n "It's not weird. Friends have sleepovers all the time."
    n "C'est normal. Les amis dorment toujours les uns chez les autres."

# game/dinner_3.rpy:144
translate french tutor_forget_4733cca1_2:

    # m ". . ."
    m ". . ."

# game/dinner_3.rpy:147
translate french tutor_forget_bbdc19b2:

    # n "Just the afternoon."
    n "Juste l'après-midi."

# game/dinner_3.rpy:149
translate french tutor_forget_a95d4aa0:

    # m "I knew it. I caught your lie earlier."
    m "Je le savais. J'ai percé ton mensonge de tout à l'heure."

# game/dinner_3.rpy:150
translate french tutor_forget_33d36d5f:

    # n "Huh?"
    n "Hein ?"

# game/dinner_3.rpy:152
translate french tutor_forget_7ba1a788:

    # m "...I knew it."
    m "...Je le savais."

# game/dinner_3.rpy:153
translate french tutor_forget_75686278:

    # m "You're just hanging out with him."
    m "Tu traines juste avec lui."

# game/dinner_3.rpy:156
translate french tutor_forget_f75edd43:

    # n "Maybe an hour or so."
    n "Peut-être une heure."

# game/dinner_3.rpy:157
translate french tutor_forget_5c69a7e7:

    # m "That's not enough to really get studying done."
    m "C'est pas assez pour vraiment réviser."

# game/dinner_3.rpy:159
translate french tutor_forget_a95d4aa0_1:

    # m "I knew it. I caught your lie earlier."
    m "Je le savais. J'ai percé ton mensonge de tout à l'heure."

# game/dinner_3.rpy:160
translate french tutor_forget_33d36d5f_1:

    # n "Huh?"
    n "Hein ?"

# game/dinner_3.rpy:161
translate french tutor_forget_75686278_1:

    # m "You're just hanging out with him."
    m "Tu traines juste avec lui."

# game/dinner_3.rpy:168
translate french threat_tutor_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:169
translate french threat_tutor_0c8ffc9a:

    # m "Claire will be tutoring you every day after school, starting tomorrow."
    m "Claire te fera du soutien tous les jours après les cours, à partir de demain."

# game/dinner_3.rpy:173
translate french threat_tutor_c714274e:

    # n "Every day?! What about my friends?!"
    n "Tous les jours ?! Et mes amis ?!"

# game/dinner_3.rpy:174
translate french threat_tutor_febf123d:

    # m "Sweetie, I'm your friend!"
    m "Chéri, je suis ton amie !"

# game/dinner_3.rpy:175
translate french threat_tutor_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:176
translate french threat_tutor_e0426815:

    # m "Also Claire can be your friend. Maybe more than friends."
    m "Et Claire aussi peut être ton amie. Peut-être même un peu plus."

# game/dinner_3.rpy:177
translate french threat_tutor_24796dbe_2:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:178
translate french threat_tutor_fa00db6a:

    # n "Are we done?"
    n "C'est bon, on en a fini ?"

# game/dinner_3.rpy:179
translate french threat_tutor_adba735a:

    # m "Just... one more thing."
    m "Juste... une dernière chose."

# game/dinner_3.rpy:181
translate french threat_tutor_bc46f036:

    # n "Okay, but my weekends are free, right?"
    n "Ok, mais j'ai quand même mes weekends pour moi ?"

# game/dinner_3.rpy:182
translate french threat_tutor_8cb62253:

    # m "Yes."
    m "Oui."

# game/dinner_3.rpy:183
translate french threat_tutor_a72d7eb7:

    # n "Okay. Good that this is all settled now."
    n "Ok. C'est bien que tout ça soit réglé."

# game/dinner_3.rpy:184
translate french threat_tutor_6d21708d:

    # m "...Yes."
    m "...Oui."

# game/dinner_3.rpy:185
translate french threat_tutor_24796dbe_3:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:186
translate french threat_tutor_adba735a_1:

    # m "Just... one more thing."
    m "Juste... une dernière chose."

# game/dinner_3.rpy:188
translate french threat_tutor_e244307b:

    # n "What if I just DON'T study with Claire?"
    n "Et si je ne veux PAS réviser avec Claire ?"

# game/dinner_3.rpy:189
translate french threat_tutor_b984a728:

    # m "Well, if you also want to hang out with her, that's good too."
    m "Eh bien, si tu veux traîner avec elle, c'est bien aussi."

# game/dinner_3.rpy:190
translate french threat_tutor_2875d402:

    # m "Anything to make you more manly."
    m "Tout ce qui pourra te rendre plus viril."

# game/dinner_3.rpy:191
translate french threat_tutor_9b7bcfcd:

    # n "ugh."
    n "ugh."

# game/dinner_3.rpy:192
translate french threat_tutor_8e62db32:

    # m "Oh."
    m "Oh."

# game/dinner_3.rpy:193
translate french threat_tutor_2756013c:

    # m "One more thing."
    m "Une dernière chose."

# game/dinner_3.rpy:200
translate french threat_school_13f7b7e7:

    # m "You're changing schools."
    m "Tu vas changer d'école."

# game/dinner_3.rpy:204
translate french threat_school_9af8ff5b:

    # n "WHAT?!"
    n "QUOI ?!"

# game/dinner_3.rpy:205
translate french threat_school_74e859fd:

    # m "I think it's not just Jack, it's the entire school that's a bad influence on you."
    m "Je pense que ce n'est pas juste Jack, c'est toute l'école qui a une mauvaise influence sur toi."

# game/dinner_3.rpy:206
translate french threat_school_0d140975:

    # n "ARE YOU SERIOUS."
    n "T'ES SÉRIEUSE."

# game/dinner_3.rpy:207
translate french threat_school_d0fb893c:

    # m "The whole Canadian culture is making you confused about who you are."
    m "Toute la culture canadienne te rend confus sur qui tu es vraiment."

# game/dinner_3.rpy:213
translate french threat_school_6ee3c569:

    # n "No, it's YOUR Asian culture that's backwards!"
    n "Non, c'est TA culture asiatique qui est arriérée !"

# game/dinner_3.rpy:214
translate french threat_school_fe83aa24:

    # m "Don't be so rude!"
    m "Ne sois pas si vilain !"

# game/dinner_3.rpy:215
translate french threat_school_26628c5f:

    # m "It's YOUR culture, too!"
    m "C'est TA culture aussi !"

# game/dinner_3.rpy:216
translate french threat_school_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:218
translate french threat_school_48f1dcfa:

    # n "You can't do this to your CHILD!"
    n "Tu ne peux pas faire ça à ton ENFANT !"

# game/dinner_3.rpy:219
translate french threat_school_fe83aa24_1:

    # m "Don't be so rude!"
    m "Ne sois pas si vilain !"

# game/dinner_3.rpy:220
translate french threat_school_68d685a2:

    # m "I'm your MOTHER, it's my right to do whatever I want with you!"
    m "Je suis ta MÈRE, j'ai le droit de faire de toi ce que je veux !"

# game/dinner_3.rpy:221
translate french threat_school_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:223
translate french threat_school_5d73bbfe:

    # n "Whatever, ALL schools have queer people."
    n "Peu importe, TOUTES les écoles ont des queers."

# game/dinner_3.rpy:224
translate french threat_school_fe83aa24_2:

    # m "Don't be so rude!"
    m "Ne sois pas si vilain !"

# game/dinner_3.rpy:225
translate french threat_school_629f6458:

    # m "And watch it, I could change my mind and start homeschooling you."
    m "Et attention, je pourrais changer d'avis et commencer à te faire l'école à la maison."

# game/dinner_3.rpy:226
translate french threat_school_24796dbe_2:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:231
translate french plot_twist_67cf7817:

    # m "Yesterday, when you were supposedly studying with Jack?"
    m "Hier, quand tu étais censé réviser avec Jack ?"

# game/dinner_3.rpy:232
translate french plot_twist_2d9fb054:

    # m "I know you secretly went off to watch a movie."
    m "Je sais que tu es parti regarder un film."

# game/dinner_3.rpy:235
translate french plot_twist_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:241
translate french plot_twist_c7587fb7:

    # n "Oh my god. You read my texts."
    n "Mais non. Tu as lu mes textos."

# game/dinner_3.rpy:242
translate french plot_twist_8b87b845:

    # m "Yes. See how smart you can be when you're not with Jack?"
    m "Oui. Tu vois comment tu es intelligent quand tu n'es pas avec Jack ?"

# game/dinner_3.rpy:244
translate french plot_twist_943bb4bb:

    # n "No, we didn't. We studied."
    n "Non, c'est pas vrai. On a révisé."

# game/dinner_3.rpy:245
translate french plot_twist_ab3614a3:

    # m "You are a very stubborn boy."
    m "Tu es un garçon très têtu."

# game/dinner_3.rpy:246
translate french plot_twist_49591517:

    # m "I read your text messages."
    m "J'ai lu tes SMS."

# game/dinner_3.rpy:248
translate french plot_twist_5b2653a7:

    # n "What makes you think that?"
    n "Qu'est-ce qui te fait croire ça ?"

# game/dinner_3.rpy:249
translate french plot_twist_1d1f5be8:

    # m "Because I read your text messages."
    m "J'ai lu tes SMS."

# game/dinner_3.rpy:254
translate french plot_twist_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:255
translate french plot_twist_2_0c0daf75:

    # m "Before dinner. I was in your room."
    m "Avant le dîner. J'étais dans ta chambre."

# game/dinner_3.rpy:258
translate french plot_twist_2_07003844:

    # m "You yelled out '[what_you_called_out]' from downstairs, while I unlocked your phone..."
    m "Tu as crié '[what_you_called_out]' d'en bas, pendant que je regardais ton téléphone..."

# game/dinner_3.rpy:259
translate french plot_twist_2_63ba2bab:

    # m "And read what you and Jack have been sending to each other."
    m "Et que je lisais ce que vous vous êtes dit entre toi et Jack."

# game/dinner_3.rpy:260
translate french plot_twist_2_719ea543:

    # m "I'm your mother. I have the right."
    m "Je susi ta mère. J'ai le droit."

# game/dinner_3.rpy:262
translate french plot_twist_2_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_3.rpy:266
translate french plot_twist_2_70bb0454:

    # m "Weird poetry?"
    pass

# game/dinner_3.rpy:268
translate french plot_twist_2_af75e4da:

    # m "Talking about smoking marijuana?"
    m "Parler de fumer du cannabis ?"

# game/dinner_3.rpy:269
translate french plot_twist_2_11c79696:

    # m "Helping you lie to your own mother?"
    m "T'aider à mentir à ta propre mère ?"

# game/dinner_3.rpy:270
translate french plot_twist_2_2753a459:

    # m "What else have you been doing behind my back?"
    m "Qu'estce que tu m'as caché d'autre ?"

# game/dinner_3.rpy:274
translate french plot_twist_2_e1bfe0ca:

    # n "This has to be a bad dream."
    n "C'est un cauchemar."

# game/dinner_3.rpy:275
translate french plot_twist_2_fcc13e5b:

    # m "Like that 'Deception' movie?"
    m "Comme ce film, 'Déception' ?"

# game/dinner_3.rpy:276
translate french plot_twist_2_2d1eab39:

    # n "It's... it's 'Inception'."
    n "C'est... c'est 'Inception'."

# game/dinner_3.rpy:277
translate french plot_twist_2_456b6f74:

    # m "Don't talk back to me."
    m "Ne me répond pas."

# game/dinner_3.rpy:279
translate french plot_twist_2_7523792b:

    # n "I'm sorry. I'm so sorry."
    n "Je suis désolé. Tellement."

# game/dinner_3.rpy:280
translate french plot_twist_2_7c3fd171:

    # m "I forgive you."
    m "Je te pardonne."

# game/dinner_3.rpy:281
translate french plot_twist_2_bff602c0:

    # m "You're my child, of course I forgive you."
    m "Tu es mon enfant, je te pardonne."

# game/dinner_3.rpy:283
translate french plot_twist_2_9828d21a:

    # n "I hate you."
    n "Je te hais."

# game/dinner_3.rpy:284
translate french plot_twist_2_ed446ecf:

    # m "That's okay."
    m "D'accord."

# game/dinner_3.rpy:285
translate french plot_twist_2_b4ce07a7:

    # m "I still love you, Nick."
    m "Je t'aime quand même, Nick."

translate french strings:

    # game/dinner_3.rpy:12
    old "That's why I'm studying more with Jack."
    new "C'est pour ça que je travaille avec Jack."

    # game/dinner_3.rpy:12
    old "Look, I'm trying. I really am."
    new "J'essaye. Je fais ce que je peux."

    # game/dinner_3.rpy:12
    old "My grades are fine."
    new "Mes notes sont bien."

    # game/dinner_3.rpy:41
    old "Are you trying to stop me from seeing Jack?"
    new "Tu essayes de m'empêcher d'aller voir Jack ?"

    # game/dinner_3.rpy:41
    old "Are you trying to matchmake me with her?"
    new "Tu essayes de me trouver une copine ?"

    # game/dinner_3.rpy:41
    old "Can we talk about tutors another time?"
    new "On peut parler de soutien scolaire une autre fois ?"

    # game/dinner_3.rpy:57
    old "Like we're dating? Yeah. We are."
    new "Qu'on sort ensemble ? Oui. C'est ça."

    # game/dinner_3.rpy:57
    old "I just meant meeting Jack."
    new "Je veux juste dire aller le voir."

    # game/dinner_3.rpy:57
    old "We're. Not. Boyfriends."
    new "On. Ne sort pas. Ensemble."

    # game/dinner_3.rpy:97
    old "Stop it! I haven't even met Claire yet!"
    new "Stop ! Je n'ai même pas encore rencontré Claire !"

    # game/dinner_3.rpy:97
    old "The odds of that are 50-50, coz I'm bi."
    new "Pour ça, c'est un peu 50-50, parce que je suis bi."

    # game/dinner_3.rpy:97
    old "No. I don't ever want to have kids."
    new "Non. Je ne veux jamais avoir d'enfants."

    # game/dinner_3.rpy:137
    old "Overnight."
    new "Passer la nuit."

    # game/dinner_3.rpy:137
    old "Just the afternoon."
    new "Juste l'après-midi."

    # game/dinner_3.rpy:137
    old "Maybe an hour or so."
    new "Peut-être une heure."

    # game/dinner_3.rpy:171
    old "Every day?! What about my friends?!"
    new "Tous les jours ?! Et mes amis ?!"

    # game/dinner_3.rpy:171
    old "Okay, but my weekends are free, right?"
    new "Ok, mais j'ai quand même mes weekends pour moi ?"

    # game/dinner_3.rpy:171
    old "What if I just DON'T study with Claire?"
    new "Et si je ne veux PAS réviser avec Claire ?"

    # game/dinner_3.rpy:211
    old "No, it's YOUR Asian culture that's backwards!"
    new "Non, c'est TA culture asiatique qui est arriérée !"

    # game/dinner_3.rpy:211
    old "You can't do this to your CHILD!"
    new "Tu ne peux pas faire ça à ton ENFANT !"

    # game/dinner_3.rpy:211
    old "Whatever, ALL schools have queer people."
    new "Peu importe, TOUTES les écoles ont des queers."

    # game/dinner_3.rpy:239
    old "Oh my god. You read my texts."
    new "Mais non. Tu as lu mes textos."

    # game/dinner_3.rpy:239
    old "No, we didn't. We studied."
    new "Non, c'est pas vrai. On a révisé."

    # game/dinner_3.rpy:239
    old "What makes you think that?"
    new "Qu'est-ce qui te fait croire ça ?"

    # game/dinner_3.rpy:272
    old "This has to be a bad dream."
    new "C'est un cauchemar."

    # game/dinner_3.rpy:272
    old "I'm sorry. I'm so sorry."
    new "Je suis désolé. Tellement."

    # game/dinner_3.rpy:272
    old "I hate you."
    new "Je te hais."
