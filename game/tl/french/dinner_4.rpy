﻿# game/dinner_4.rpy:7
translate french start_dinner_4_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:8
translate french start_dinner_4_98d19d52:

    # m "It's because your dad's almost never home, isn't it?"
    m "C'est à cause de ton père qui n'est jamais là, hein ?"

# game/dinner_4.rpy:9
translate french start_dinner_4_a36d056a:

    # m "Without a strong male role model, you become confused..."
    m "Sans un modèle masculin, tu t'es tout mélangé..."

# game/dinner_4.rpy:13
translate french start_dinner_4_e2ae70e8:

    # n "Yeah, coz Dad's SUCH a great role model."
    n "C'est vrai que papa est tellement un bon modèle."

# game/dinner_4.rpy:14
translate french start_dinner_4_5ed5c40d:

    # m "Nick, no matter what, he's your father. You should love him."
    m "Nick, c'est ton père. Tu devrais l'aimer."

# game/dinner_4.rpy:16
translate french start_dinner_4_1f9656b7:

    # n "That's not how it works. I'd be bi anyway."
    n "C'est pas comme ça que ça marche, je serais quand même bi."

# game/dinner_4.rpy:17
translate french start_dinner_4_7fecdf45:

    # m "How do you know?! Are you an expert in psychology?!"
    m "Comment tu le sais ?! Tu es un expert en psychologie ?!"

# game/dinner_4.rpy:19
translate french start_dinner_4_cf0a3e0c:

    # n "You know what? Maybe you're right."
    n "Tu as peut-être raison."

# game/dinner_4.rpy:20
translate french start_dinner_4_1b27f9d3:

    # m "I know..."
    m "Je sais..."

# game/dinner_4.rpy:27
translate french my_fault_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:28
translate french my_fault_00747189:

    # m "This is all my fault..."
    m "Tout est de ta faute..."

# game/dinner_4.rpy:29
translate french my_fault_59aa30fc:

    # m "I told you to be careful around those kinds of people, but I told you too late..."
    m "Je t'ai dit de faire attention avec ce genre de personnes, mais je t'ai prévenu trop tard..."

# game/dinner_4.rpy:33
translate french my_fault_9fc45229:

    # m "[[sob]"
    m "[[sanglots]"

# game/dinner_4.rpy:34
translate french my_fault_769e8276:

    # m "Oh Nick! My poor baby!"
    m "Oh Nick ! Mon pauvre bébé !"

# game/dinner_4.rpy:54
translate french cry_1_52b37338:

    # m "huu... huu... huu..."
    m "huu... huu... huu..."

# game/dinner_4.rpy:55
translate french cry_1_15a0f1b7:

    # n "I'm sorry. About Jack, the lies, everything."
    n "Je suis désolé. Pour Jack, les mensonges, tout."

# game/dinner_4.rpy:56
translate french cry_1_8c6a2026:

    # m "owww... owww..."
    m "owww... owww..."

# game/dinner_4.rpy:57
translate french cry_1_1c968bb7:

    # n "I take it all back."
    n "Je retire tout ce que j'ai dit."

# game/dinner_4.rpy:58
translate french cry_1_9288a760:

    # m "sniff..."
    m "sniff..."

# game/dinner_4.rpy:59
translate french cry_1_a4d0bdd3:

    # n "...please..."
    n "...s'il te plait..."

# game/dinner_4.rpy:68
translate french cry_2_52b37338:

    # m "huu... huu... huu..."
    m "huu... huu... huu..."

# game/dinner_4.rpy:69
translate french cry_2_4db60655:

    # n "Seriously, it is SO fake."
    n "Sérieux, c'est tellement fake."

# game/dinner_4.rpy:70
translate french cry_2_8c6a2026:

    # m "owww... owww..."
    m "owww... owww..."

# game/dinner_4.rpy:71
translate french cry_2_3a9f6335:

    # n "Will you shut up?!"
    n "Tu vas la fermer ?!"

# game/dinner_4.rpy:72
translate french cry_2_9288a760:

    # m "sniff..."
    m "sniff..."

# game/dinner_4.rpy:73
translate french cry_2_75828cdf:

    # n "SHUT. UP."
    n "FERME. LA."

# game/dinner_4.rpy:81
translate french cry_3_c5ee56ec:

    # n "BAWWWWW"
    n "BAWWWWW"

# game/dinner_4.rpy:82
translate french cry_3_52b37338:

    # m "huu... huu... huu..."
    m "huu... huu... huu..."

# game/dinner_4.rpy:83
translate french cry_3_0372c09d:

    # n "WAH WAH WAH WAH WAHHH"
    n "WAH WAH WAH WAH WAHHH"

# game/dinner_4.rpy:84
translate french cry_3_8c6a2026:

    # m "owww... owww..."
    m "owww... owww..."

# game/dinner_4.rpy:85
translate french cry_3_397c0165:

    # n "BRRrrRR-BRR-BRbR BWAH BWAHRR rrrRRR-WaahHH WO WO WO RaaahhH"
    n "BRRrrRR-BRR-BRbR BWAH BWAHRR rrrRRR-WaahHH WO WO WO RaaahhH"

# game/dinner_4.rpy:86
translate french cry_3_9288a760:

    # m "sniff..."
    m "sniff..."

# game/dinner_4.rpy:89
translate french cry_3_00dadace:

    # n "Okay, we done?"
    n "C'est bon, c'est fini ?"

# game/dinner_4.rpy:94
translate french what_are_you_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_4.rpy:95
translate french what_are_you_cfcc57a3:

    # m "Nick... what are you?"
    m "Nick... qu'est-ce que tu es ?"

# game/dinner_4.rpy:96
translate french what_are_you_4933ba2b:

    # n "Excuse me?"
    n "Pardon ?"

# game/dinner_4.rpy:101
translate french what_are_you_aa03292e:

    # m "What {i}are{/i} you?"
    m "Qu'est-ce que tu {i}es{/i} ?"

# game/dinner_4.rpy:108
translate french what_are_you_58f49886:

    # n "I'm bisexual."
    n "Je suis bisexuel."

# game/dinner_4.rpy:110
translate french what_are_you_bdc472d7:

    # m "...and you said that means..."
    m "...et tu as dit que ça veut dire..."

# game/dinner_4.rpy:111
translate french what_are_you_d03196be:

    # n "Sexually attracted to both men and women."
    n "Attiré sexuellement par les hommes et les femmes."

# game/dinner_4.rpy:112
translate french what_are_you_3dec774d:

    # m "You can't be both."
    m "Tu ne peux pas être les deux."

# game/dinner_4.rpy:113
translate french what_are_you_7f361554:

    # m "You have to pick one."
    m "Il faut choisir."

# game/dinner_4.rpy:114
translate french what_are_you_d95e6080:

    # n "That's... not how it works. At all."
    n "C'est... pas comme ça que ça marche. Du tout."

# game/dinner_4.rpy:119
translate french what_are_you_aaa0a1f1:

    # n "I'm just confused."
    n "Je suis juste un peu perdu."

# game/dinner_4.rpy:120
translate french what_are_you_b2553871:

    # m "...I know."
    m "...Je sais."

# game/dinner_4.rpy:121
translate french what_are_you_88f04326:

    # m "I'm sorry Jack confused you."
    m "Je suis désolée que Jack t'aie un peu perdu."

# game/dinner_4.rpy:122
translate french what_are_you_a613af90:

    # m "You're just going through a phase, it's okay."
    m "C'est juste un passage dans ta vie, ça passera."

# game/dinner_4.rpy:123
translate french what_are_you_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:124
translate french what_are_you_38078eea:

    # m "It's okay. It's okay..."
    m "Ça ira, ça ira..."

# game/dinner_4.rpy:129
translate french what_are_you_12d0923a:

    # n "I'm your son, dammit."
    n "Je suis ton fils, enfin."

# game/dinner_4.rpy:130
translate french what_are_you_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:131
translate french what_are_you_1cd5f869:

    # n "Isn't that enough?"
    n "Ça ne te suffit pas ?"

# game/dinner_4.rpy:135
translate french have_you_had_sex_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_4.rpy:136
translate french have_you_had_sex_3ed06b65:

    # m "Did you have sex with Jack."
    m "As-tu fait du sexe avec Jack."

# game/dinner_4.rpy:139
translate french have_you_had_sex_53c1cef0:

    # n "Yes."
    n "Oui."

# game/dinner_4.rpy:140
translate french have_you_had_sex_da5e68e6:

    # m "[[DRY HEAVE]"
    m ". . ."

# game/dinner_4.rpy:142
translate french have_you_had_sex_82bb9a08:

    # n "No."
    n "Non."

# game/dinner_4.rpy:143
translate french have_you_had_sex_b428e1f7:

    # m "Please stop lying... I saw your texts..."
    m "Arrête de mentir, s'il te plaît... J'ai vu vos SMS..."

# game/dinner_4.rpy:144
translate french have_you_had_sex_329ff7bb:

    # n "We were just sexting, we didn't actually--"
    n "C'était juste des sextos, on n'a pas--"

# game/dinner_4.rpy:145
translate french have_you_had_sex_a5372f4f:

    # m "...and your photos..."
    m "...et vos photos..."

# game/dinner_4.rpy:147
translate french have_you_had_sex_55abebf1:

    # n "I'm not saying."
    n "Je ne te le dirai pas."

# game/dinner_4.rpy:148
translate french have_you_had_sex_d84cd746:

    # m "oh my god... you did."
    m "Oh mon dieu... vous l'avez fait."

# game/dinner_4.rpy:153
translate french have_you_had_sex_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:154
translate french have_you_had_sex_2_c66fc2f9:

    # m "Which... one of you is the woman?"
    m "Lequel... d'entre vous est la femme ?"

# game/dinner_4.rpy:158
translate french have_you_had_sex_2_f6546dde:

    # n "OH COME ON!"
    n "OH FRANCHEMENT !"

# game/dinner_4.rpy:159
translate french have_you_had_sex_2_e0082d6c:

    # n "That's like asking which chopstick is the spoo--"
    n "C'est comme demander quelle baguette est la fourchet--"

# game/dinner_4.rpy:160
translate french have_you_had_sex_2_c9a7a283:

    # m "Which one of you?..."
    m "Lequel d'entre vous ?..."

# game/dinner_4.rpy:168
translate french have_you_had_sex_2_803ee522:

    # n "I'm usually the bottom."
    n "Je suis généralement en-dessous."

# game/dinner_4.rpy:172
translate french have_you_had_sex_2_75871441:

    # n "Jack is, mostly."
    n "C'est Jack, généralement."

# game/dinner_4.rpy:173
translate french have_you_had_sex_2_d00aac1c:

    # m "Th-that... means you could still be straight! R-right?..."
    m "Ç-ça... veut dire que tu pourrais toujours être hétéro ! H-hein ?..."

# game/dinner_4.rpy:174
translate french have_you_had_sex_2_4592d99d:

    # m "If... you know... you're the one who puts your..."
    m "Si... euh... c'est toi qui mets ton..."

# game/dinner_4.rpy:175
translate french have_you_had_sex_2_b2d1c8c4:

    # m "your..."
    m "ton..."

# game/dinner_4.rpy:179
translate french have_you_had_sex_2_26d81c51:

    # n "We take turns."
    n "On le fait un peu chacun son tour."

# game/dinner_4.rpy:193
translate french throw_up_d2cebc5b:

    # n "what."
    n "quoi."

# game/dinner_4.rpy:195
translate french throw_up_3cdbf806:

    # n "whaaat."
    n "quoiii."

# game/dinner_4.rpy:197
translate french throw_up_71f12e4e:

    # n "whaaaaaaaaaaaaaaat."
    n "quoiiiiiiiiiiiiiii."

# game/dinner_4.rpy:204
translate french father_soon_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_4.rpy:205
translate french father_soon_ff226860:

    # m "Your father will be back soon."
    m "Ton père va bientôt rentrer."

# game/dinner_4.rpy:206
translate french father_soon_9aeadaf2:

    # n "The food's cold. Well, except for the spot you just uh, reversed, on."
    n "Les plats sont froids. Enfin, à part ce que tu as, heu, renversé."

# game/dinner_4.rpy:207
translate french father_soon_626adee0:

    # m "Your dad's late. Must have been a stressful day at work."
    m "Ton père est en retard. Ça a dû être une journée difficile au travail."

# game/dinner_4.rpy:208
translate french father_soon_83a5de35:

    # m "So... please... when he's back..."
    m "Donc... s'il te plaît... quand il reviendra..."

# game/dinner_4.rpy:209
translate french father_soon_0da8aaf5:

    # m "Promise me you'll keep all this secret?"
    m "Tu me promets que tu garderas tout ça secret ?"

# game/dinner_4.rpy:210
translate french father_soon_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_4.rpy:212
translate french father_soon_d59d42c7:

    # m "Don't tell him about Jack."
    m "Ne lui dis pas pour Jack."

# game/dinner_4.rpy:215
translate french father_soon_082ae3f9:

    # m "Don't tell him you think you're bisexual."
    m "Ne lui dis pas que t penses être bisexuel."

# game/dinner_4.rpy:217
translate french father_soon_eaa09e07:

    # m "Don't tell him you're confused about your sexuality."
    m "Ne lui dis pas que tu es perdu sur ta sexualité."

# game/dinner_4.rpy:219
translate french father_soon_40cc3d26:

    # m "Don't tell him you lied to us so you could... do things with Jack."
    m "Ne lui dis pas que tu nous a menti pour pouvoir... faire des choses avec Jack."

# game/dinner_4.rpy:222
translate french father_soon_b1dd7d25:

    # m "Don't tell him you make Jack a woman."
    m "Ne lui dis pas que tu traites Jack comme une femme."

# game/dinner_4.rpy:224
translate french father_soon_94b645f7:

    # m "Don't tell him you act like a woman with Jack."
    m "Ne lui dis pas que tu fais la femme avec Jack."

# game/dinner_4.rpy:226
translate french father_soon_1c7aa2e4:

    # m "Don't tell him you and Jack both act like women."
    m "Ne lui dis pas que vous faites la femme, toi et Jack."

# game/dinner_4.rpy:228
translate french father_soon_40727cd5:

    # m "Okay?..."
    m "Ok ?..."

# game/dinner_4.rpy:234
translate french father_soon_093c6a2c:

    # n "Okay."
    n "Ok."

# game/dinner_4.rpy:235
translate french father_soon_30a66545:

    # m "Okay."
    m "Ok."

# game/dinner_4.rpy:236
translate french father_soon_4733cca1_1:

    # m ". . ."
    m ". . ."

# game/dinner_4.rpy:237
translate french father_soon_6e783de2:

    # m "Your father's here."
    m "Ton père est là."

# game/dinner_4.rpy:241
translate french father_soon_b80fef68:

    # n "No. Not okay."
    n "Non. Pas d'accord."

# game/dinner_4.rpy:242
translate french father_soon_ab21741a:

    # m "Nick, no no no, please--"
    m "Nick, non non non, s'il te plaît--"

# game/dinner_4.rpy:243
translate french father_soon_96fa8615:

    # m "Oh no. Your father's here."
    m "Oh non. Ton père est là."

# game/dinner_4.rpy:247
translate french father_soon_37424cd4:

    # n "As long as you don't tell him, either."
    n "Tant que tu ne lui dis rien non plus."

# game/dinner_4.rpy:248
translate french father_soon_2ac32e80:

    # m "I won't."
    m "D'accord."

# game/dinner_4.rpy:249
translate french father_soon_2be2d4ac:

    # n "Promise me you won't."
    n "Promets-le moi."

# game/dinner_4.rpy:250
translate french father_soon_211925fe:

    # m "I pr--"
    m "Je te le pr--"

# game/dinner_4.rpy:251
translate french father_soon_f3aacb76:

    # m "Shhh. Your father's here."
    m "Chut. Ton père est là."

translate french strings:

    # game/dinner_4.rpy:11
    old "Yeah, coz Dad's SUCH a great role model."
    new "C'est vrai que papa est tellement un bon modèle."

    # game/dinner_4.rpy:11
    old "That's not how it works. I'd be bi anyway."
    new "C'est pas comme ça que ça marche, je serais quand même bi."

    # game/dinner_4.rpy:11
    old "You know what? Maybe you're right."
    new "Tu as peut-être raison."

    # game/dinner_4.rpy:38
    old "Mom... please don't cry..."
    new "Maman... ne pleure pas..."

    # game/dinner_4.rpy:38
    old "Quit your fake crying."
    new "Arrête de faire semblant."

    # game/dinner_4.rpy:38
    old "[[cry]"
    new "[[pleure]"

    # game/dinner_4.rpy:103
    old "I'm bisexual."
    new "Je suis bisexuel."

    # game/dinner_4.rpy:103
    old "I'm just confused."
    new "Je suis juste un peu perdu."

    # game/dinner_4.rpy:103
    old "I'm your son, dammit."
    new "Je suis ton fils, enfin."

    # game/dinner_4.rpy:137
    old "Yes."
    new "Oui."

    # game/dinner_4.rpy:137
    old "No."
    new "Non."

    # game/dinner_4.rpy:137
    old "I'm not saying."
    new "Je ne te le dirai pas."

    # game/dinner_4.rpy:164
    old "I'm usually the bottom."
    new "Je suis généralement en-dessous."

    # game/dinner_4.rpy:164
    old "Jack is, mostly."
    new "C'est Jack, généralement."

    # game/dinner_4.rpy:164
    old "We take turns."
    new "On le fait un peu chacun son tour."

    # game/dinner_4.rpy:230
    old "Okay."
    new "Ok."

    # game/dinner_4.rpy:230
    old "No. Not okay."
    new "Non. Pas d'accord."

    # game/dinner_4.rpy:230
    old "As long as you don't tell him, either."
    new "Tant que tu ne lui dis rien non plus."
