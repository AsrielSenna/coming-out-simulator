# game/jack_1.rpy:21
translate french start_jack_1_2f53bfbc:

    # j "And when he simply announces,"
    j "Et puis quand il annonce,"

# game/jack_1.rpy:22
translate french start_jack_1_3ce9b9fc:

    # j "'I bought the airline.'"
    j "'J'ai acheté la compagnie aérienne.'"

# game/jack_1.rpy:23
translate french start_jack_1_84d8885a:

    # j "That was positively priceless!"
    j "C'était génial !"

# game/jack_1.rpy:24
translate french start_jack_1_e48b3901:

    # n "Is that what he said?"
    n "C'est ça qu'il a dit ?"

# game/jack_1.rpy:25
translate french start_jack_1_8b9f3f4c:

    # n "I missed out what everyone in the theater was laughing about."
    n "J'ai pas compris pourquoi tout le monde rigolait dans le cinéma."

# game/jack_1.rpy:26
translate french start_jack_1_0a158edc:

    # j "You either need subtitles, or to clean your ears more often."
    j "Il te faut des sous-titres, ou alors il faut te nettoyer les oreilles."

# game/jack_1.rpy:27
translate french start_jack_1_e12f1913:

    # j "So how did you interpret the ending?"
    j "Comment t'as interprété la fin ?"

# game/jack_1.rpy:42
translate french Inception_Dream_9ec26e16:

    # j "So his entire redemption story was a lie?"
    j "Donc toute sa rédemption, c'est un mensonge ?"

# game/jack_1.rpy:43
translate french Inception_Dream_86725833:

    # n "A big fat lie."
    n "Un bon gros mensonge."

# game/jack_1.rpy:44
translate french Inception_Dream_468d6507:

    # j "You're a bit of a downer, aren't you?"
    j "Un peu pessimiste, hein ?"

# game/jack_1.rpy:53
translate french Inception_Dream_b4c767d4:

    # n "Sometimes... but not when I'm with you."
    n "Parfois... mais pas avec toi."

# game/jack_1.rpy:54
translate french Inception_Dream_897c682f:

    # j "Ah Nicky, you amateur poet."
    j "Ah Nicky, poête amateur."

# game/jack_1.rpy:55
translate french Inception_Dream_adcfd23b:

    # n "Get me some french breads and wine,"
    n "Prépare les trompettes,"

# game/jack_1.rpy:56
translate french Inception_Dream_a98b6f7f:

    # n "Coz that's got to be the cheesiest thing I've ever said."
    n "Parce que ce qu'être pouêt, c'est l'histoire de ma vie."

# game/jack_1.rpy:57
translate french Inception_Dream_66d3c3b0:

    # j "Apologize for nothing."
    j "Incroyable."

# game/jack_1.rpy:58
translate french Inception_Dream_f5e4f763:

    # n "Anywho..."
    n "Enfin bref..."

# game/jack_1.rpy:63
translate french Inception_Dream_cd450a11:

    # n "I'm just a realist."
    n "Je suis juste réaliste."

# game/jack_1.rpy:64
translate french Inception_Dream_adcf0f27:

    # j "You need more positive thinking in your life."
    j "Tu as besoin de plus de pensées positives."

# game/jack_1.rpy:65
translate french Inception_Dream_b6bfb68c:

    # n "And YOU need to stop being such a new-age hippie."
    n "Et TOI il faut que t'arrêtes d'être un hippie new-age."

# game/jack_1.rpy:66
translate french Inception_Dream_f5e4f763_1:

    # n "Anywho..."
    n "Enfin bref..."

# game/jack_1.rpy:75
translate french Inception_Awake_2b242fe0:

    # n "Otherwise, the whole movie would've all just been a lie."
    n "Sinon, tout le film ne serait qu'un mensonge."

# game/jack_1.rpy:76
translate french Inception_Awake_6d63a398:

    # n "What's the point of living a lie?"
    n "Quel est l'intéret de vivre dans un faux rêve ?"

# game/jack_1.rpy:77
translate french Inception_Awake_897c682f:

    # j "Ah Nicky, you amateur poet."
    j "Ah Nicky, poête amateur."

# game/jack_1.rpy:78
translate french Inception_Awake_52deb368:

    # j "I take it you liked the film?"
    j "J'en déduis que t'as aimé le film ?"

# game/jack_1.rpy:82
translate french Inception_Awake_a70e7e0a:

    # n "Aw yiss. Yes I did."
    n "Aw ui. Oui oui."

# game/jack_1.rpy:84
translate french Inception_Awake_17e58cab:

    # n "Mehhh, it was a tad confusing at times."
    n "Mehhh, c'était un peu confus par moments."

# game/jack_1.rpy:85
translate french Inception_Awake_3b163bf8:

    # j "I believe that was the purpose."
    j "Je pense que c'était un peu fait exprès."

# game/jack_1.rpy:86
translate french Inception_Awake_22ec1d93:

    # n "Mission accomplished, then."
    n "Mission accomplie, alors."

# game/jack_1.rpy:87
translate french Inception_Awake_f5e4f763:

    # n "Anywho..."
    n "Enfin bref..."

# game/jack_1.rpy:89
translate french Inception_Awake_f4d32e4f:

    # n "BWOOOOOOOOOOONG"
    n "BWOOOOOOOOOOONG"

# game/jack_1.rpy:90
translate french Inception_Awake_2f77b697:

    # j "I'll interpret that as a yes."
    j "Je prend ça pour un oui."

# game/jack_1.rpy:98
translate french Inception_Neither_07b8adc7:

    # j "Oh?"
    j "Oh ?"

# game/jack_1.rpy:99
translate french Inception_Neither_d55a54df:

    # n "He didn't even bother looking to see if the top fell!"
    n "Il n'a même pas regardé si la toupie tombait !"

# game/jack_1.rpy:100
translate french Inception_Neither_5666913a:

    # n "Lies, truths, or half-truths... Cobbs no longer cares."
    n "Mensonges, vérités, demi-vérités... Cobbs s'en fout."

# game/jack_1.rpy:101
translate french Inception_Neither_594c6317:

    # n "He's finally happy, and that's all that matters."
    n "Il est heureux, et c'est tout ce qui compte."

# game/jack_1.rpy:102
translate french Inception_Neither_f1acdf1b:

    # j "You either are being quite poetic, or quite depressing."
    j "Soit tu es très poétique, soit très déprimant."

# game/jack_1.rpy:109
translate french Inception_Neither_f0a97967:

    # n "I'm a poet,"
    n "Je suis un poête,"

# game/jack_1.rpy:110
translate french Inception_Neither_ea7d7655:

    # n "and I wasn't even aware of the fact."
    n "et je ne le savais même pas."

# game/jack_1.rpy:111
translate french Inception_Neither_69e44782:

    # j "You're a lyrical miracle, the evidence is empircal."
    j "Tu es un miracle lyrique, l'évidence est empirique."

# game/jack_1.rpy:112
translate french Inception_Neither_63318687:

    # n "That's hysterical."
    n "À mourir de rire."

# game/jack_1.rpy:113
translate french Inception_Neither_f5e4f763:

    # n "Anywho..."
    n "Enfin bref..."

# game/jack_1.rpy:123
translate french Inception_Neither_757936aa:

    # n "Or both."
    n "Ou les deux."

# game/jack_1.rpy:124
translate french Inception_Neither_e68ea2d8:

    # n "POETRY IS PAIN. ART IS SUFFERING."
    n "LA POÉSIE EST SOUFFRANCE."

# game/jack_1.rpy:125
translate french Inception_Neither_d8f0ff2a:

    # j "You sound like my mother."
    j "On dirait ma mère."

# game/jack_1.rpy:126
translate french Inception_Neither_4d58452f:

    # n "Your parents are {i}such{/i} new-age hippies."
    n "Tes parents sont {i}tellement{/i} des hippies new-age."

# game/jack_1.rpy:127
translate french Inception_Neither_f5e4f763_1:

    # n "Anywho..."
    n "Enfin bref..."

# game/jack_1.rpy:135
translate french Sadsack_765a0e30:

    # j "Aw, sorry to hear that."
    j "Aw, non..."

# game/jack_1.rpy:136
translate french Sadsack_0ddbb119:

    # j "I hope our little date at the movies cheered you up?"
    j "J'estpère que notre petite sortie au cinéma t'a changé les idées ?"

# game/jack_1.rpy:137
translate french Sadsack_4bf25473:

    # n "Sure did!"
    n "Que oui !"

# game/jack_1.rpy:142
translate french thanks_5c934ba5:

    # n "So yeah! Thanks for taking me out to watch Inception!"
    n "Du coup, merci de m'avoir emmené voir Inception !"

# game/jack_1.rpy:143
translate french thanks_e990b403:

    # j "My pleasure, Nicky."
    j "Avec plaisir, Nicky."

# game/jack_1.rpy:144
translate french thanks_f5552af9:

    # j "You should parody Inception in that odd web game of yours!"
    j "Tu devrais parodier Inception dans ton jeu web !"

# game/jack_1.rpy:145
translate french thanks_c70c26bd:

    # n "Mmm, maybe maybe."
    n "Mmm, peut-être."

# game/jack_1.rpy:146
translate french thanks_a22522e2:

    # n "Let's meet again tomorrow evening!"
    n "On se voit demain soir ?"

# game/jack_1.rpy:148
translate french thanks_550d4e1e:

    # j "Although..."
    j "Enfin..."

# game/jack_1.rpy:149
translate french thanks_65d88f66:

    # n "Hope I can convince the parents to let me out overnight."
    n "J'espère que je peux convaincre mes parents de me laisser sortir le soir."

# game/jack_1.rpy:151
translate french thanks_6e6053ba:

    # j "I wish you didn't tell your mom and dad we were just studying, when we were actually at the cinema."
    j "J'aurais préféré si tu n'avais pas dit à tes parents que tu allais réviser, alors qu'en fait on allait au cinéma."

# game/jack_1.rpy:152
translate french thanks_f27a2ee2:

    # n "I'll pretend we'll be cramming for the midterms all nigh-- huh?"
    n "Je leur dirai qu'on bûche sur les partiels toute la nui-- hein ?"

# game/jack_1.rpy:154
translate french thanks_0b208dea:

    # j "You can't keep hiding like this."
    j "Tu ne peux pas continuer à te cacher comme ça."

# game/jack_1.rpy:155
translate french thanks_46074bd9:

    # n "Jack..."
    n "Jack..."

# game/jack_1.rpy:160
translate french thanks_6a430d88:

    # n "They can never, ever know."
    n "Ils ne peuvent jamais savoir."

# game/jack_1.rpy:161
translate french thanks_1122bc0e:

    # j "Really, never?"
    j "Vraiment, jamais ?"

# game/jack_1.rpy:164
translate french thanks_780c2198:

    # n "I wish I could tell them, too."
    n "J'aimerais bien pouvoir leur dire."

# game/jack_1.rpy:167
translate french thanks_76861156:

    # n "I'm not ready to tell them yet."
    n "Je ne suis pas encore prêt à leur dire."

# game/jack_1.rpy:168
translate french thanks_1f97cfc3:

    # j "I can help you be ready."
    j "Je peux t'aider à être prêt."

# game/jack_1.rpy:172
translate french hiding_c0035b8d:

    # j "Nicky, hiding like this is eating away at your soul."
    j "Nicky, te cacher comme ça va te ronger de l'intérieur."

# game/jack_1.rpy:175
translate french hiding_9217b54d:

    # j "Like you said, what's the point of living a lie?"
    j "Comme t'as dit, quel est l'intéret de vivre un mensonge ?"

# game/jack_1.rpy:177
translate french hiding_d14620f5:

    # j "It's... how'd you put it... 'a big fat lie'?"
    j "C'est... comment t'as dit... 'un gros mensonge' ?"

# game/jack_1.rpy:180
translate french hiding_573fc2f1:

    # j "When you said just now you're a sadsack?"
    j "Quand tu viens de dire que t'es un sac de tristesse ?"

# game/jack_1.rpy:181
translate french hiding_e9891830:

    # j "I know you weren't joking. Not really."
    j "Je sais que c'était pas une blague. Pas vraiment."

# game/jack_1.rpy:183
translate french hiding_9d196be2:

    # n "Jack, come on."
    n "Jack, allez."

# game/jack_1.rpy:184
translate french hiding_ed532ab8:

    # j "I came out to my parents last year."
    j "J'ai tout dit à mes parents l'année dernière."

# game/jack_1.rpy:186
translate french hiding_6188a7ca:

    # n "That's NOT a fair comparison."
    n "Ce n'est PAS une bonne comparaison."

# game/jack_1.rpy:187
translate french hiding_81ad12f2:

    # n "LIKE I SAID, you and your parents are a bunch of new-age hippies."
    n "COMME J'AI DIT, toi et tes parents vous êtes une bande de hippies new-age."

# game/jack_1.rpy:188
translate french hiding_361f68b1:

    # n "When I'm at your place, I can't tell if all the smoke is incense or marijuana."
    n "Quand je suis chez toi, j'arrive pas à séparer l'odeur de l'encens de celle du cannabis."

# game/jack_1.rpy:189
translate french hiding_70c3f915:

    # j "Hey! We only smoke weed every other day!"
    j "Hey ! On ne fume qu'un jour sur deux !"

# game/jack_1.rpy:190
translate french hiding_ceccb846:

    # n "Heh."
    n "Heh."

# game/jack_1.rpy:191
translate french hiding_1e3a88a9:

    # j "The point is, my parents supported my coming out."
    j "Mais mes parents ont soutenu mon coming-out."

# game/jack_1.rpy:193
translate french hiding_6ea4bc10:

    # j "And they were very supportive!"
    j "Ils m'ont vraiment soutenu !"

# game/jack_1.rpy:195
translate french hiding_bd0d5b37:

    # j "You're in Canada now. A lot of people here are LGBT friendly."
    j "T'es au Canada maintenant. Beaucoup de monde est tolérant sur la sexualité des autres."

# game/jack_1.rpy:196
translate french hiding_431b0189:

    # j "How do you know your parents won't be supportive of you, too?"
    j "Comment tu sais que tes parents ne vont pas te soutenir, toi aussi ?"

# game/jack_1.rpy:214
translate french hiding_2_536cafff:

    # n "Again... They can never, ever know."
    n "Vraiment... ils ne peuvent jamais savoir."

# game/jack_1.rpy:216
translate french hiding_2_7ddff659:

    # j "You have trust issues."
    j "T'as des problèmes de confiance en toi."

# game/jack_1.rpy:217
translate french hiding_2_23fa028f:

    # j "You're even texting me instead of calling..."
    j "Tu m'envoies des textos au lieu de m'appeler..."

# game/jack_1.rpy:218
translate french hiding_2_e934d561:

    # j "...because you think your parents might listen in."
    j "...parce que tu as peur que tes parents écoutent."

# game/jack_1.rpy:220
translate french hiding_2_cc25d31d:

    # n "They would!"
    n "Ils le feraient !"

# game/jack_1.rpy:222
translate french hiding_2_ab84956a:

    # j "This mode of communication."
    j "Ce mode de communication."

# game/jack_1.rpy:223
translate french hiding_2_4a21c32e:

    # j "It's imprecise, impersonal, impossible to truly connect."
    j "C'est imprécis, impersonnel, impossible de vraiment se comprendre."

# game/jack_1.rpy:226
translate french hiding_2_059d88e3:

    # n "Heh. You're an amateur poet like me, apparently."
    n "Heh. Un poête amateur, comme moi, apparemment."

# game/jack_1.rpy:228
translate french hiding_2_716fb58e:

    # n "It's not too bad..."
    n "C'est pas si mal..."

# game/jack_1.rpy:231
translate french hiding_2_e080077f:

    # j "You yourself just said you wish you could tell them."
    j "Tu l'as dit toi-même que tu aurais voulu leur dire."

# game/jack_1.rpy:232
translate french hiding_2_37772003:

    # j "Tell them."
    j "Dis-leur."

# game/jack_1.rpy:234
translate french hiding_2_2251f684:

    # j "Nicky."
    j "Nicky."

# game/jack_1.rpy:235
translate french hiding_2_e3893119:

    # j "Tell them about us. Tonight."
    j "Dis-leur pour nous. Ce soir."

# game/jack_1.rpy:251
translate french hiding_3_93f8e717:

    # j ". . ."
    j ". . ."

# game/jack_1.rpy:252
translate french hiding_3_205c342c:

    # n "I don't want to freak them out too much."
    n "Je ne veux pas leur faire peur."

# game/jack_1.rpy:253
translate french hiding_3_a9fa5b6e:

    # n "Still need to convince them to let me stay at your place tomorrow night."
    n "Je dois encore les convaincre de me laisser aller chez toi demain soir."

# game/jack_1.rpy:254
translate french hiding_3_dcb57239:

    # n "I'll tell 'em I'm studying with you again."
    n "Je vais encore leur dire que je révise avec toi."

# game/jack_1.rpy:255
translate french hiding_3_93f8e717_1:

    # j ". . ."
    j ". . ."

# game/jack_1.rpy:256
translate french hiding_3_61e89f0d:

    # n "It's dinnertime. I'm heading downstairs now."
    n "C'est l'heure de manger. Je descend."

# game/jack_1.rpy:258
translate french hiding_3_0808719e:

    # j "Hey... I agree."
    j "Hey... Je suis d'accord."

# game/jack_1.rpy:259
translate french hiding_3_33d36d5f:

    # n "Huh?"
    n "Huh?"

# game/jack_1.rpy:260
translate french hiding_3_9278e217:

    # j "With your thoughts on the movie ending, that is."
    j "Avec ce que tu penses du film."

# game/jack_1.rpy:262
translate french hiding_3_ab417b13:

    # j "I think Cobbs was still dreaming, living a lie."
    j "Je pense que Cobbs rêvait encore, dans un mensonge."

# game/jack_1.rpy:264
translate french hiding_3_5120d4f5:

    # j "I think Cobbs reconnected with his real family, in the real world."
    j "Je pense que Cobbs a rejoint sa vraie famille, dans le monde réel."

# game/jack_1.rpy:266
translate french hiding_3_b05e7261:

    # j "I think it doesn't matter, as long as Cobbs is happy."
    j "Je pense que ça n'a pas vraiment d'importance, tant que Cobbs est heureux."

# game/jack_1.rpy:267
translate french hiding_3_3e7505ab:

    # n "Oh."
    n "Oh."

# game/jack_1.rpy:268
translate french hiding_3_53d71353:

    # j "Okay."
    j "Okay."

# game/jack_1.rpy:270
translate french hiding_3_f296ffbd:

    # j "Hope you changed your mind about being 'not ready to tell them yet'."
    j "J'espère que t'as changé d'avis à propos d'être 'pas encore prêt à leur dire'."

# game/jack_1.rpy:271
translate french hiding_3_0b236fd8:

    # j "Good luck. Text me in an hour."
    j "Bonne chance. Tiens-moi au courant dans une heure."

# game/jack_1.rpy:273
translate french hiding_3_2392d369:

    # n "See ya."
    n "À plus."

# game/jack_1.rpy:277
translate french hiding_3_85b2924a:

    # n "You goof."
    n "Petit bouffon."

# game/jack_1.rpy:282
translate french jack_1_end_76b2fe88:

    # nvl clear
    nvl clear

translate french strings:

    # game/jack_1.rpy:29
    old "It was totally all a dream."
    new "C'était juste un rêve."

    # game/jack_1.rpy:29
    old "He's got to be back in the real world!"
    new "Il est de retour dans la réalité, obligé !"

    # game/jack_1.rpy:29
    old "Doesn't matter. Cobbs just finally let go."
    new "Peu importe. Cobbs laisse tout ça derrière lui."

    # game/jack_1.rpy:46
    old "Yup, I'm just a sad sack of sadness."
    new "Yep, je suis juste un paquet de tristesse."

    # game/jack_1.rpy:46
    old "Sometimes... but not when I'm with you."
    new "Parfois... mais pas quand je susi avec toi."

    # game/jack_1.rpy:46
    old "I'm just a realist."
    new "Je suis juste réaliste."

    # game/jack_1.rpy:80
    old "Aw yiss. Yes I did."
    new "Aw ui. Oui oui."

    # game/jack_1.rpy:80
    old "Mehhh, it was a tad confusing at times."
    new "Mehhh, c'était un peu confus par moments."

    # game/jack_1.rpy:80
    old "BWOOOOOOOOOOONG"
    new "BWOOOOOOOOOOONG"

    # game/jack_1.rpy:104
    old "I'm a poet, and I didn't even know it."
    new "Je suis un poête, et je ne le savais même pas."

    # game/jack_1.rpy:104
    old "Nah, I'm just a sad sack of sadness."
    new "Nan, je suis juste un paquet de tristesse."

    # game/jack_1.rpy:104
    old "Or both."
    new "Ou les deux."

    # game/jack_1.rpy:157
    old "They can never, ever know."
    new "Ils ne peuvent jamais savoir."

    # game/jack_1.rpy:157
    old "I wish I could tell them, too."
    new "J'aimerais bien pouvoir leur dire."

    # game/jack_1.rpy:157
    old "I'm not ready to tell them yet."
    new "Je ne suis pas encore prêt à leur dire."

    # game/jack_1.rpy:198
    old "Asian parents are usually very homophobic."
    new "Les parents asiatiques sont généralement très homophobes."

    # game/jack_1.rpy:198
    old "I don't know... I guess I haven't tried..."
    new "Je sais pas... J'imagine que j'ai pas essayé..."

    # game/jack_1.rpy:198
    old "They don't support anything but STUDYING."
    new "Ils ne soutiennent rien d'autre qu'ÉTUDIER."

    # game/jack_1.rpy:237
    old "Tonight?! Heck no."
    new "Ce soir ?! Jamais de la vie."

    # game/jack_1.rpy:237
    old "Sigh... I'll try my best."
    new "Soupir... Je vais essayer."

    # game/jack_1.rpy:237
    old "I'll just carefully hint at it."
    new "Je vais le sous-entendre tout doucement."

# game/jack_1.rpy:276
translate french hiding_3_deba5cdb:

    # n "You amateur poet."
    n "Petit poête amateur."

# game/jack_1.rpy:278
translate french hiding_3_db812dca:

    # n "You new-age hippie."
    n "Petit hippie new-age."
