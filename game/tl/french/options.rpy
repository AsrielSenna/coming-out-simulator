translate french strings:

    # game/options.rpy:15
    old "Coming Out Simulator"
    new "Coming Out Simulator"

    # game/options.rpy:32
    old "{size=17}Coming Out Simulator {s}2014{/s} 2021{/size}\n{size=14}this game is public domain{/size}\n\n{size=11}original game by {a=http://ncase.me}Nicky Case{/a} | {a=http://twitter.com/ncasenmare}@ncasenmare{/a}{/size}\n{size=11}the open source {a=https://github.com/ncase/coming-out-simulator-2014}code{/a}{/size}\n{size=11}the design {a=http://blog.ncase.me/coming-out-simulator-2014/}document{/a}{/size}\n\n{size=11}ported to Ren'py by Asriel Senna{/size}\n{size=11}the open source {a=https://gitlab.com/AsrielSenna/coming-out-simulator}code{/a}{/size}\n\n{size=11}thank you for playing! :){/size}"
    new "{size=17}Coming Out Simulator {s}2014{/s} 2021{/size}\n{size=14}ce jeu est dans le domaine public{/size}\n\n{size=11}jeu original par {a=http://ncase.me}Nicky Case{/a} | {a=http://twitter.com/ncasenmare}@ncasenmare{/a}{/size}\n{size=11}le {a=https://github.com/ncase/coming-out-simulator-2014}code{/a} open source{/size}\n{size=11}la {a=http://blog.ncase.me/coming-out-simulator-2014/}feuille{/a} de design{/size}\n\n{size=11}porté sur Ren'py par Asriel Senna{/size}\n{size=11}le {a=https://gitlab.com/AsrielSenna/coming-out-simulator}code{/a} open source{/size}\n\n{size=11}merci d'avoir joué ! :){/size}"
