﻿# game/dinner_1.rpy:18
translate french start_dinner_1_bad3aef5:

    # n "Where is everyone?..."
    n "Où est tout le monde ?..."

# game/dinner_1.rpy:19
translate french start_dinner_1_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_1.rpy:37
translate french waiting_1_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_1.rpy:56
translate french waiting_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_1.rpy:68
translate french waiting_2_10c0b45d:

    # n "Cut the crying, cacophonous cat clock!"
    n "Arrête de crier, horloge féline cacophonique !"

# game/dinner_1.rpy:75
translate french waiting_2_40970ac2:

    # m "Did you learn poetry from a friend?"
    m "C'est un ami qui t'a appris la poésie ?"

# game/dinner_1.rpy:77
translate french waiting_2_99209e74:

    # m "Poetic."
    m "Poétique."

# game/dinner_1.rpy:80
translate french waiting_2_8bfd639d:

    # n "Oh, hey mom."
    n "Oh, salut maman."

# game/dinner_1.rpy:83
translate french waiting_2_ef7257d3:

    # n "Ugh, why did we get that thing?"
    n "Ugh, pourquoi est-ce qu'on a acheté ce truc ?"

# game/dinner_1.rpy:89
translate french waiting_2_0a1bbd83:

    # m "Your grandfather gave it to us."
    m "Ton grand-père nous l'a donné."

# game/dinner_1.rpy:92
translate french waiting_2_bda723c0:

    # n "Oh! Hey mom."
    n "Oh ! Salut maman."

# game/dinner_1.rpy:96
translate french waiting_2_edf8a9ab:

    # n "Meow."
    n "Miaou ."

# game/dinner_1.rpy:97
translate french waiting_2_1adacce8:

    # n "Meow!"
    n "Miaou !"

# game/dinner_1.rpy:100
translate french waiting_2_8053f865:

    # n "MEOW!"
    n "MIAOU !"

# game/dinner_1.rpy:104
translate french waiting_2_41ebf468:

    # m "Nick, what are you doing?..."
    m "Nick, qu'est-ce que tu fais ?..."

# game/dinner_1.rpy:110
translate french waiting_2_d307f071:

    # n "MEOOOhhhh didn't see you. Ahem. Hey mom."
    n "MIAOUUuuu t'avais pas vu. Ahem. Salut maman."

translate french strings:

    # game/dinner_1.rpy:21
    old "Moooom?"
    new "Mamaaaan ?"

    # game/dinner_1.rpy:21
    old "Daaaaad?"
    new "Papaaaaa ?"

    # game/dinner_1.rpy:21
    old "Hello, anybody?"
    new "Hého, y'a quelqu'un ?"

    # game/dinner_1.rpy:39
    old "[[start eating]"
    new "[[commence à manger]"

    # game/dinner_1.rpy:39
    old "[[wait some more]"
    new "[[attend un peu plus]"

    # game/dinner_1.rpy:39
    old "[[play with food]"
    new "[[joue avec la nourriture]"

    # game/dinner_1.rpy:66
    old "Cut the crying, cacophonous cat clock!"
    new "Arrête de crier, horloge féline cacophonique !"

    # game/dinner_1.rpy:66
    old "Ugh, why did we get that thing?"
    new "Ugh, pourquoi est-ce qu'on a acheté ce truc ?"

    # game/dinner_1.rpy:66
    old "Meow! Meow! Meow! Meow!"
    new "Miaou ! Miaou ! Miaou ! Miaou !"
