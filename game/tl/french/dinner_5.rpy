# game/dinner_5.rpy:13
translate french start_dinner_5_f55186b6:

    # f "Hey Qiying! Hey Nick!"
    f "Salut Qiying ! Salut Nick !"

# game/dinner_5.rpy:14
translate french start_dinner_5_d011fc69:

    # f "I'm home!"
    f "Je suis rentré !"

# game/dinner_5.rpy:18
translate french start_dinner_5_62f8cf76:

    # m "Hi honey."
    m "Salut chéri."

# game/dinner_5.rpy:19
translate french start_dinner_5_f5e27a49:

    # n "Sup dad, how was your day?"
    n "Salut papa, comment ça allait la journée ?"

# game/dinner_5.rpy:21
translate french start_dinner_5_068adf4d:

    # f "Stayed overtime. Hopefully the boss will notice it before my Performance Review."
    f "Je suis resté plus longtemps. Avec un peu de chance, le patron le remarquera avant mon Évaluation."

# game/dinner_5.rpy:22
translate french start_dinner_5_fdad373a:

    # f "Really, though, I was just playing web games all day. Haha!"
    f "Alors qu'en fait, j'ai joué à des jeux sur internet tout la journée. Haha !"

# game/dinner_5.rpy:23
translate french start_dinner_5_7127504a:

    # n "Ha ha."
    n "Ha ha."

# game/dinner_5.rpy:25
translate french start_dinner_5_6c179002:

    # f "Nick, why aren't {i}your{/i} web games any fun?"
    f "Nick, pourquoi est-ce que {i}tes{/i} jeux ne sont pas amusants ?"

# game/dinner_5.rpy:29
translate french start_dinner_5_39b14e0c:

    # n "I thought my games were fun..."
    n "Je croyais que mes jeux étaient amusants..."

# game/dinner_5.rpy:30
translate french start_dinner_5_2e0297d9:

    # f "Well then! You have a sick sense of fun, don't you. Haha!"
    f "Ah ! Eh bien, tu as un drôle de sens de l'amusement. Haha !"

# game/dinner_5.rpy:31
translate french start_dinner_5_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:33
translate french start_dinner_5_9a1fd444:

    # n "Not all games have to be fun."
    n "Tous les jeux n'ont pas à être amusants."

# game/dinner_5.rpy:34
translate french start_dinner_5_53b43fa5:

    # f "Oh yes. You're right."
    f "Oh oui. C'est vrai."

# game/dinner_5.rpy:35
translate french start_dinner_5_6cc57a6c:

    # f "BAD games aren't any fun. Haha!"
    f "Les MAUVAIS jeux ne sont pas amusants. Haha !"

# game/dinner_5.rpy:36
translate french start_dinner_5_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:38
translate french start_dinner_5_02b9afc4:

    # n "ART!"
    n "L'ART !"

# game/dinner_5.rpy:39
translate french start_dinner_5_fcf14152:

    # f "Pfft. What's the use of art?"
    f "Pfft. Quel est l'intéret ?"

# game/dinner_5.rpy:40
translate french start_dinner_5_5f0f3fc7:

    # f "Next thing you know, you're going to be writing bad amateur poetry, or something."
    f "Bientôt, tu écriras de la mauvaise poésie en amateur..."

# game/dinner_5.rpy:41
translate french start_dinner_5_24796dbe_2:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:46
translate french casual_c4f8bac3:

    # f "Hey Qi, what's that sauce on your plate?"
    f "Hey Qi, qu'est-ce que c'est que cette sauce dans ton assiette ?"

# game/dinner_5.rpy:47
translate french casual_33afd7ec:

    # f "Uh..."
    f "Euh..."

# game/dinner_5.rpy:53
translate french casual_39978072:

    # n "It's vomit."
    n "C'est du vomi."

# game/dinner_5.rpy:56
translate french casual_f822d1bc:

    # f "Nick! One week grounded!"
    f "Nick ! Puni pendant une semaine !"

# game/dinner_5.rpy:57
translate french casual_4f1eb275:

    # f "Don't insult your mother's cooking like that."
    f "N'insulte pas la cuisine de ta mère comme ça."

# game/dinner_5.rpy:58
translate french casual_0f3fb774:

    # f "Her food insults itself plenty enough. Haha!"
    f "Sa cuisine s'insulte assez elle-même. Haha !"

# game/dinner_5.rpy:62
translate french casual_56bec95b:

    # n "Don't eat it! It's, uh, really not good."
    n "Ne le mange pas ! C'est, euh, vraiment pas bon."

# game/dinner_5.rpy:65
translate french casual_831890d2:

    # f "Nick! One day grounded!"
    f "Nick ! Puni jusqu'à demain !"

# game/dinner_5.rpy:66
translate french casual_fd12a62b:

    # f "Show some respect. Have more faith in your mother's cooking!"
    f "Un peu de respect. aie un peu de foi dans la cuisine de ta mère !"

# game/dinner_5.rpy:67
translate french casual_863cfe26:

    # f "Because the way she cooks, we could certainly use a miracle! Haha!"
    f "Parce que vu comment elle cuisine, on aurait bien besoin d'un miracle ! Haha !"

# game/dinner_5.rpy:71
translate french casual_5e6e14aa:

    # n "Why don't you give it a try, dad?"
    n "Pourquoi tu n'essayes pas, papa ?"

# game/dinner_5.rpy:74
translate french casual_1cbb7e2e:

    # m "Nick..."
    m "Nick..."

# game/dinner_5.rpy:75
translate french casual_daf1cafa:

    # f "Don't mind if I do!"
    f "Pas besoin de me le dire deux fois !"

# game/dinner_5.rpy:76
translate french casual_85c0d8c1:

    # f "[[eats a spoonful]"
    f "[[mange une cuillerée]"

# game/dinner_5.rpy:77
translate french casual_c485f357:

    # f ". . ."
    f ". . ."

# game/dinner_5.rpy:78
translate french casual_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:79
translate french casual_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_5.rpy:80
translate french casual_0819da7a:

    # f "Well, you've cooked up worse, hun. Haha!"
    f "Eh bien tu as déjà fait pire, chérie. Haha !"

# game/dinner_5.rpy:86
translate french casual_2_26ab0a69:

    # m "Dear..."
    m "Chéri..."

# game/dinner_5.rpy:87
translate french casual_2_ea4d82a9:

    # f "So, son! How's school?"
    f "Alors, fils ! Comment ça va, à l'école ?"

# game/dinner_5.rpy:91
translate french casual_2_4dd92707:

    # n "School's fine."
    n "Ça va."

# game/dinner_5.rpy:93
translate french casual_2_0f4d539a:

    # f "Really, fine?"
    f "Vraiment, ça va ?"

# game/dinner_5.rpy:95
translate french casual_2_23a69206:

    # f "What about your poor grades in [studying_subject] and [studying_subject_2]?"
    f "Et tes notes en [studying_subject!tl] et en [studying_subject_2!tl] ?"

# game/dinner_5.rpy:97
translate french casual_2_35e43df9:

    # f "What about your poor grades in [studying_subject]?"
    f "Et tes notes en [studying_subject!tl] ?"

# game/dinner_5.rpy:99
translate french casual_2_81b2b2e8:

    # m "Nick and I were just talking about that."
    m "On parlait justement de ça, Nick et moi."

# game/dinner_5.rpy:102
translate french casual_2_36430f88:

    # n "I'm studying at a friend's place tomorrow."
    n "Je révise chez un copain, demain."

# game/dinner_5.rpy:109
translate french casual_2_21e93cd5:

    # f "Don't you remember? I just grounded you for tomorrow."
    f "Tu ne te rappelles pas ? Tu es puni pour demain."

# game/dinner_5.rpy:111
translate french casual_2_ceb5e2e6:

    # f "Don't you remember? I just grounded you for a week."
    f "Tu ne te rappelles pas ? Tu es puni pour une semaine."

# game/dinner_5.rpy:112
translate french casual_2_e92bf64e:

    # f "You must get your stupid from your mother's side. Haha!"
    f "Tu dois tenir ta stupidité de ta mère. Haha !"

# game/dinner_5.rpy:114
translate french casual_2_70e2ee74:

    # n "Um. I..."
    n "Hum. Je..."

# game/dinner_5.rpy:118
translate french casual_2_35810f4b:

    # f "I'm bumping it up. You're now grounded for a week."
    f "Je l'augmente. Tu es puni pour ue semaine."

# game/dinner_5.rpy:120
translate french casual_2_75808fd9:

    # f "I'm bumping it up. You're now grounded for TWO weeks."
    f "Je l'augmente. Tu es puni pour DEUX semaines."

# game/dinner_5.rpy:122
translate french casual_2_a6b3c3d9:

    # m "Speaking of studying..."
    m "En parlant de réviser..."

# game/dinner_5.rpy:128
translate french casual_2_681df429:

    # n "DAD I'M BI--"
    n "PAPA JE SUIS BI--"

# game/dinner_5.rpy:131
translate french casual_2_fe199afb:

    # m "BICYCLING to school every day starting next week."
    m "BIEN DÉCIDÉ à aller en cours à vélo à partir de la semaine prochaine."

# game/dinner_5.rpy:132
translate french casual_2_69b3553f:

    # f "Oh good!"
    f "Oh, c'est bien !"

# game/dinner_5.rpy:133
translate french casual_2_23d58d88:

    # f "You could certainly lose some weight, or else how will you get a girlfriend?"
    f "Tu pourras perdre un peu de poids, sinon comment tu vas pouvoir avoir une copine ?"

# game/dinner_5.rpy:134
translate french casual_2_eb4c3da3:

    # f "You must get your chubbiness from your mother. Haha!"
    f "Tu dois tenir ton ventre de ta mère. Haha !"

# game/dinner_5.rpy:135
translate french casual_2_7127504a:

    # n "Ha ha."
    n "Ha ha."

# game/dinner_5.rpy:136
translate french casual_2_68190013:

    # m "Speaking of school..."
    m "À propos d'école..."

# game/dinner_5.rpy:141
translate french getting_a_tutor_6d48f8a3:

    # m "We were discussing probably getting a home tutor."
    m "Nous parlions de la possibilité de prendre des cours de soutien scolaire."

# game/dinner_5.rpy:142
translate french getting_a_tutor_808c7c29:

    # f "Oh! Is this the Claire kid?"
    f "Oh ! Avec cette fille, Claire ?"

# game/dinner_5.rpy:148
translate french getting_a_tutor_325ce74a:

    # n "Mom, we both promised we wouldn't talk about this..."
    n "Maman, on a promis de ne pas en parler..."

# game/dinner_5.rpy:150
translate french getting_a_tutor_67ed2bd7:

    # m "You {i}just{/i} tried talking about it."
    m "Tu {i}viens{/i} d'essayer d'en parler."

# game/dinner_5.rpy:152
translate french getting_a_tutor_017e219c:

    # n "Mom, you said we wouldn't talk about this..."
    n "Maman, tu as dit qu'on n'en parlerait pas..."

# game/dinner_5.rpy:153
translate french getting_a_tutor_938ef8d4:

    # m "You're the one who didn't promise not to talk!"
    m "C'est toi qui n'a pas promis de ne rien dire !"

# game/dinner_5.rpy:155
translate french getting_a_tutor_fd505f55:

    # n "Mom, you said you wouldn't talk about this if I didn't..."
    n "Maman, tu as dit que tu n'en parlerais pas si je ne disais rien..."

# game/dinner_5.rpy:157
translate french getting_a_tutor_67ed2bd7_1:

    # m "You {i}just{/i} tried talking about it."
    m "Tu {i}viens{/i} d'essayer d'en parler."

# game/dinner_5.rpy:159
translate french getting_a_tutor_e191ba6a:

    # f "Talking about what?..."
    f "Parler de quoi ?..."

# game/dinner_5.rpy:160
translate french getting_a_tutor_00709da3:

    # f "I'm the head of this household. You two better not be hiding secrets from me."
    f "Je suis le chef de cette famille. Vous avez intéret à ne rien me cacher."

# game/dinner_5.rpy:161
translate french getting_a_tutor_37b08c49:

    # m "Oh... Nick just really, really likes Claire."
    m "Oh... Nick aime juste vraiment, vraiment beaucoup Claire."

# game/dinner_5.rpy:165
translate french getting_a_tutor_0c39e44b:

    # n "What?! No I don't!"
    n "Quoi ?! Non !"

# game/dinner_5.rpy:166
translate french getting_a_tutor_0f9031fd:

    # f "Don't be so shy about it."
    f "Ne sois pas si timide."

# game/dinner_5.rpy:169
translate french getting_a_tutor_9a8091c5:

    # n "Fine. You got me. I have a crush on Claire."
    n "Très bien. Tu m'as eu. J'ai un crush sur Claire."

# game/dinner_5.rpy:172
translate french getting_a_tutor_af89f426:

    # n "I have a boyfriend."
    n "J'ai un copain."

# game/dinner_5.rpy:173
translate french getting_a_tutor_b5da321f:

    # f "Yes son! You're going to be a boyfriend!"
    f "Oui, mon fils ! Tu vas être un petit copain !"

# game/dinner_5.rpy:174
translate french getting_a_tutor_ff20c746:

    # n "{i}Have{/i}. I {i}have{/i} a--"
    n "{i}Ai{/i}. J'{i}ai{/i} un--"

# game/dinner_5.rpy:179
translate french getting_a_tutor_2_aa760637:

    # f "You're becoming a man, son!"
    f "Tu deviens un homme, fils !"

# game/dinner_5.rpy:180
translate french getting_a_tutor_2_91a79d11:

    # f "If I were your age, I ditch your mother and chase Claire, too! Haha!"
    f "Si j'avais ton âge, je laisserais tomber ta mère et j'irai voir Claire, moi aussi ! Haha !"

# game/dinner_5.rpy:182
translate french getting_a_tutor_2_2efe4ee4:

    # n "That's totes weird, dude."
    n "T'es cringe, mec."

# game/dinner_5.rpy:183
translate french getting_a_tutor_2_f3477e2b:

    # f "Talking back? Careful, I'll box your ears, boy!"
    f "Tu me réponds ? Attention à ta tête, mon garçon !"

# game/dinner_5.rpy:186
translate french getting_a_tutor_2_38cb821a:

    # m "We were also thinking about changing schools for Nick."
    m "On pensait aussi changer d'école pour Nick."

# game/dinner_5.rpy:187
translate french getting_a_tutor_2_bc0dd678:

    # m "Maybe to Claire's school."
    m "Peut-être pour l'école de Claire."

# game/dinner_5.rpy:189
translate french getting_a_tutor_2_89d6f015:

    # m "Claire will be tutoring Nick every day after school in [studying_subject] and [studying_subject_2]."
    m "Claire lui fera du soutien tous les jours en [studying_subject!tl] et en [studying_subject_2!tl]."

# game/dinner_5.rpy:191
translate french getting_a_tutor_2_cd2973e5:

    # m "Claire will be tutoring Nick every day after school in [studying_subject]."
    m "Claire lui fera du soutien tous les jours en [studying_subject!tl]."

# game/dinner_5.rpy:193
translate french getting_a_tutor_2_c03020d7:

    # f "Nick, how does all this sound? Yes or no?"
    f "Nick, comment ça t'irait ? Oui ou non ?"

# game/dinner_5.rpy:194
translate french getting_a_tutor_2_fe8f8f77:

    # m "He loves the ide--"
    m "Il adore l'idé--"

# game/dinner_5.rpy:195
translate french getting_a_tutor_2_a07bf23f:

    # f "Shut up, Qi. I asked my son."
    f "Ferme-la, Qi. J'ai posé une question à mon fils."

# game/dinner_5.rpy:196
translate french getting_a_tutor_2_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_5.rpy:200
translate french getting_a_tutor_2_ed6d1053:

    # f "Mister Nicklaus Liow."
    f "Monsieur Nicklaus Liow."

# game/dinner_5.rpy:202
translate french getting_a_tutor_2_cd934a68:

    # f "You want to change schools to chase your hot tutor girlfriend?"
    f "Tu veux changer d'école pour aller avec ta jolie copine qui te fait du soutien ?"

# game/dinner_5.rpy:204
translate french getting_a_tutor_2_33cb8201:

    # f "You want to spend all your after-school hours with your hot tutor girlfriend?"
    f "Tu veux passer tout ton temps libre avec ta jolie copine qui te fait du soutien ?"

# game/dinner_5.rpy:206
translate french getting_a_tutor_2_988e49bd:

    # n "It's complicated, I--"
    n "C'est compliqué, je--"

# game/dinner_5.rpy:207
translate french getting_a_tutor_2_dda7ed34:

    # f "No pansy middle-of-the-road answers."
    f "Pas d'entre-deux de fillettes."

# game/dinner_5.rpy:208
translate french getting_a_tutor_2_1aadb87b:

    # f "Yes. Or. No."
    f "Oui. Ou Non."

# game/dinner_5.rpy:210
translate french getting_a_tutor_2_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:220
translate french agree_with_dad_97e4a3c9:

    # n "...Yes."
    n "...Oui."

# game/dinner_5.rpy:222
translate french agree_with_dad_3f103db4:

    # f "Hm."
    f "Hm."

# game/dinner_5.rpy:223
translate french agree_with_dad_81b16a74:

    # f "You two seem to have made this big life decision very eagerly!"
    f "Vous avez l'air d'avoir pris cette décision avec beaucoup d'enthousiasme !"

# game/dinner_5.rpy:224
translate french agree_with_dad_f92615a1:

    # f "So eagerly, in fact, you made it in less than an hour, and tried to hide it from me. What a sudden change."
    f "À tel point que vous l'avez prise en moins d'une heure, et en essayant de me le cacher. Quel changement, tout d'un coup."

# game/dinner_5.rpy:225
translate french agree_with_dad_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_5.rpy:226
translate french agree_with_dad_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:228
translate french agree_with_dad_7dbb04ec:

    # f "Nick, you did something naughty, didn't you?"
    f "Nick, tu as fait une bêtise, n'est-ce pas ?"

# game/dinner_5.rpy:229
translate french agree_with_dad_01e71a85:

    # f "What did you do."
    f "Dis-moi ce que tu as fait."

# game/dinner_5.rpy:233
translate french agree_with_dad_c917b237:

    # n "I failed my midterms."
    n "J'ai foiré mes partiels."

# game/dinner_5.rpy:235
translate french agree_with_dad_aa1af9a9:

    # f "...Oh."
    f "...Oh."

# game/dinner_5.rpy:236
translate french agree_with_dad_0118ea17:

    # f "Yeah, you need to get your grades back up."
    f "Oui, il faut que tu fasses quelque chose pour ces notes."

# game/dinner_5.rpy:240
translate french agree_with_dad_a5e40fc9:

    # f "Or you'll be stuck in a teaching job like your mother! Haha!"
    f "Sinon tu finiras avec un boulot pourri comme ta mère ! Haha !"

# game/dinner_5.rpy:241
translate french agree_with_dad_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:245
translate french agree_with_dad_5e021b18:

    # n "I had sex with Jack."
    n "J'ai fait l'amour avec Jack."

# game/dinner_5.rpy:248
translate french agree_with_dad_9fc45229:

    # m "[[sob]"
    m "[[sanglots]"

# game/dinner_5.rpy:249
translate french agree_with_dad_c485f357:

    # f ". . ."
    f ". . ."

# game/dinner_5.rpy:253
translate french agree_with_dad_247f483b:

    # n "I had sex with Claire."
    n "J'ai fait l'amour avec Claire."

# game/dinner_5.rpy:255
translate french agree_with_dad_e8237130:

    # m "...Nick!"
    m "...Nick !"

# game/dinner_5.rpy:256
translate french agree_with_dad_c485f357_1:

    # f ". . ."
    f ". . ."

# game/dinner_5.rpy:257
translate french agree_with_dad_07a20a6f:

    # f " Nnnnnniiiiiiiiice."
    f " Nnnnnniiiiiiiiice."

# game/dinner_5.rpy:258
translate french agree_with_dad_2a625be3:

    # m "...Dear!"
    m "...Chéri !"

# game/dinner_5.rpy:259
translate french agree_with_dad_9b7a4ffd:

    # f "Wait, uh, you didn't get her pregnant, did you?"
    f "Attend, eh, tu ne l'as pas mise enceinte, hein ?"

# game/dinner_5.rpy:260
translate french agree_with_dad_4dba524a:

    # n "No. I'm not stupid."
    n "Non. Je ne suis pas stupide."

# game/dinner_5.rpy:264
translate french agree_with_dad_938da6dd:

    # f "Good. Otherwise you'd be stuck for the next two decades raising a kid, like me! Haha!"
    f "Bien. Sinon tu serais bloqué pendant vingt ans à élever un gosse, comme moi ! Haha !"

# game/dinner_5.rpy:265
translate french agree_with_dad_7127504a:

    # n "Ha ha."
    n "Ha ha."

# game/dinner_5.rpy:272
translate french agreeable_ending_1bbcf40e:

    # f "For a moment there, Nick, I thought you'd been smoking pot with your hippie classmate Jack, or something!"
    f "Pendant un moment, Nick, j'ai cru que tu étais allé fumer dees joints avec ton copain hippie, Jack, ou quelque chose dans ce genre !"

# game/dinner_5.rpy:275
translate french agreeable_ending_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:276
translate french agreeable_ending_bcbea423:

    # f "So!"
    f "Alors !"

# game/dinner_5.rpy:277
translate french agreeable_ending_00d7eaa0:

    # f "Who wants to watch a movie this weekend? I hear Inception is good."
    f "Qui veut aller voir un film ce weekend ? Il parait qu'Inception est pas mal."

# game/dinner_5.rpy:281
translate french agreeable_ending_9c044be0:

    # n "Let's watch it! I haven't seen it yet."
    n "Allons-y ! Je ne l'ai pas encore vu."

# game/dinner_5.rpy:282
translate french agreeable_ending_d03074c6:

    # f "Then it's a plan!"
    f "Super !"

# game/dinner_5.rpy:283
translate french agreeable_ending_bddac2bd:

    # f "Hey Nick, you know who's acting in the movie?"
    f "Hey Nick, tu sais qui joue dans le film ?"

# game/dinner_5.rpy:284
translate french agreeable_ending_824144fa:

    # n "Um. Leonardo DiCaprio?"
    n "Hum. Leonardo DiCaprio ?"

# game/dinner_5.rpy:285
translate french agreeable_ending_9d0ae473:

    # f "No no, Ellen Page."
    f "Non non, Ellen Page."

# game/dinner_5.rpy:286
translate french agreeable_ending_57fbe8f0:

    # f "Doesn't Claire look a little bit like her?"
    f "Claire lui ressemble un peu, non ?"

# game/dinner_5.rpy:287
translate french agreeable_ending_05427814:

    # n "I guess."
    n "J'imagine."

# game/dinner_5.rpy:290
translate french agreeable_ending_370a5a26:

    # n "Uh... let's do a different movie..."
    n "Euh... peut-être un autre film, plutôt..."

# game/dinner_5.rpy:291
translate french agreeable_ending_7f89538e:

    # f "What, Inception too complicated for you?"
    f "Quoi, Inception est trop compliqué pour toi ?"

# game/dinner_5.rpy:292
translate french agreeable_ending_288fcea9:

    # n "Hey..."
    n "Hey..."

# game/dinner_5.rpy:294
translate french agreeable_ending_d661a49b:

    # f "Sure, I understand if you failed [studying_subject] and [studying_subject_2]..."
    f "Je comprend que tu aie foiré en [studying_subject!tl] et en [studying_subject_2!tl]..."

# game/dinner_5.rpy:296
translate french agreeable_ending_5b15e991:

    # f "Sure, I understand if you failed [studying_subject]..."
    f "Je comprend que tu aie foiré en [studying_subject!tl]..."

# game/dinner_5.rpy:297
translate french agreeable_ending_7cc91f51:

    # f "But come on, this is a {i}movie{/i}!"
    f "Mais enfin, c'est un {i}film{/i} !"

# game/dinner_5.rpy:298
translate french agreeable_ending_feffadae:

    # f "You can't have inherited that much stupid from your mother's side! Haha!"
    f "Tu ne peux pas avoir hérité de tant de stupidité de ta mère ! Haha !"

# game/dinner_5.rpy:299
translate french agreeable_ending_7127504a:

    # n "Ha ha."
    n "Ha ha."

# game/dinner_5.rpy:302
translate french agreeable_ending_1a42794d:

    # n "Oh, I already saw Inception."
    n "Oh, j'ai déjà vu Inception."

# game/dinner_5.rpy:303
translate french agreeable_ending_5a90aa6e:

    # f "Oh ho, I see..."
    f "Oh ho, je vois..."

# game/dinner_5.rpy:304
translate french agreeable_ending_7f45817d:

    # f "You went on a little movie date with your special friend Claire, didn't you?"
    f "Tu es allé faire une petite sortie au cinéma avec ton amie Claire, hein ?"

# game/dinner_5.rpy:305
translate french agreeable_ending_2fe1bf98:

    # n "Yeah."
    n "Ouais."

# game/dinner_5.rpy:306
translate french agreeable_ending_e9c81014:

    # n "A date with my special friend."
    n "Un rendez-vous avec ma super amie."

# game/dinner_5.rpy:311
translate french argue_with_dad_7206875c:

    # n "...No."
    n "...Non."

# game/dinner_5.rpy:313
translate french argue_with_dad_ec3a180d:

    # f "Excuse me?"
    f "Pardon ?"

# game/dinner_5.rpy:314
translate french argue_with_dad_04a70959:

    # n "No. Mom's doing this so I can't see Jack anymore."
    n "Non. Maman fait ça pour que je ne puisse plus voir Jack."

# game/dinner_5.rpy:315
translate french argue_with_dad_c9a96210:

    # f "Jack."
    f "Jack."

# game/dinner_5.rpy:316
translate french argue_with_dad_afcdef02:

    # n "My friend."
    n "Mon ami."

# game/dinner_5.rpy:320
translate french argue_with_dad_4445e19d:

    # n "My boyfriend."
    n "Mon petit ami."

# game/dinner_5.rpy:323
translate french argue_with_dad_9fc45229:

    # m "[[sob]"
    m "[[sanglots]"

# game/dinner_5.rpy:325
translate french argue_with_dad_b2060feb:

    # m "Jack did this to our son!"
    m "Jack a fait ça à notre fils !"

# game/dinner_5.rpy:326
translate french argue_with_dad_a1513e54:

    # f "That kid chose his lifestyle, but I will not have it be yours, Nick."
    f "Ce garçon a peut-être choisi cette voie dans la vie, mais ce ne sera pas la tienne, Nick."

# game/dinner_5.rpy:329
translate french argue_with_dad_44f8e6e7:

    # n "Mom hates him, coz he happens to be gay."
    n "Maman le déteste parce qu'il est gay."

# game/dinner_5.rpy:332
translate french argue_with_dad_9fc45229_1:

    # m "[[sob]"
    m "[[sanglots]"

# game/dinner_5.rpy:334
translate french argue_with_dad_61cb9845:

    # f "You made your mother cry."
    f "Tu as fait pleurer ta mère."

# game/dinner_5.rpy:336
translate french argue_with_dad_06ec810b:

    # m "And his parents are drug addicts!"
    m "Et ses parents sont des drogués !"

# game/dinner_5.rpy:337
translate french argue_with_dad_05d73d26:

    # f "Jack chose that lifestyle, but I will not have it be yours, Nick."
    f "Ce garçon a peut-être choisi cette voie dans la vie, mais ce ne sera pas la tienne, Nick."

# game/dinner_5.rpy:340
translate french argue_with_dad_74804683:

    # n "Mom hates him, coz she THINKS he's gay."
    n "Maman le déteste parce qu'elle PENSE qu'il est gay."

# game/dinner_5.rpy:343
translate french argue_with_dad_9fc45229_2:

    # m "[[sob]"
    m "[[sanglots]"

# game/dinner_5.rpy:345
translate french argue_with_dad_2dbb035c:

    # m "Jack IS gay!"
    m "Jack EST gay !"

# game/dinner_5.rpy:347
translate french argue_with_dad_06ec810b_1:

    # m "And his parents are drug addicts!"
    m "Et ses parents sont des drogués !"

# game/dinner_5.rpy:348
translate french argue_with_dad_05d73d26_1:

    # f "Jack chose that lifestyle, but I will not have it be yours, Nick."
    f "Ce garçon a peut-être choisi cette voie dans la vie, mais ce ne sera pas la tienne, Nick."

# game/dinner_5.rpy:355
translate french argument_ending_24796dbe:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:358
translate french argument_ending_de337fc6:

    # m "Jack acts like the woman, not him..."
    m "Jack fait la femme, pas lui..."

# game/dinner_5.rpy:360
translate french argument_ending_d3aa055b:

    # m "Nick's not fully gay, he told me himself he's still attracted to girls!"
    m "Nick n'est pas complètement gay, il m'a dit lu-même qu'il était toujours attiré par les filles !"

# game/dinner_5.rpy:361
translate french argument_ending_24796dbe_1:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:363
translate french argument_ending_20d15a84:

    # m "Earlier Nick told me he was just confused!"
    m "Tout à l'heure, Nick m'a dit qu'il était juste un peu perdu !"

# game/dinner_5.rpy:364
translate french argument_ending_11ab4b4a:

    # f "Oh, clearly he is."
    f "Oh, clairement."

# game/dinner_5.rpy:365
translate french argument_ending_24796dbe_2:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:367
translate french argument_ending_894db0c6:

    # n "Look, like I told Mom just now, I'm your SON, isn't that enou--"
    n "Comme je viens de le dire à maman, je suis votre FILS, ce n'est pas suff--"

# game/dinner_5.rpy:369
translate french argument_ending_aa813c1b:

    # f "Nick, you're changing schools."
    f "Nick, tu vas changer d'école."

# game/dinner_5.rpy:370
translate french argument_ending_24796dbe_3:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:371
translate french argument_ending_9f344406:

    # m "huuu... huuu... huuu..."
    m "huuu... huuu... huuu..."

# game/dinner_5.rpy:373
translate french argument_ending_c469c666:

    # f "Your mother and I will do random checks on your texts and emails."
    f "Ta mère et moi, nous ferons des vérifications de tes SMS et de tes mails."

# game/dinner_5.rpy:374
translate french argument_ending_24796dbe_4:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:375
translate french argument_ending_8c6a2026:

    # m "owww... owww..."
    m "owww... owww..."

# game/dinner_5.rpy:377
translate french argument_ending_b815b8b0:

    # f "I swear, if I have to pay Claire extra to make you realize you're straight, I will."
    f "Si j'ai besoin de payer Claire un peu plus pour te faire comprendre que tu es normal, je le ferai."

# game/dinner_5.rpy:378
translate french argument_ending_24796dbe_5:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:382
translate french argument_ending_9c5efba2:

    # m "When I was crying earlier, he accused it of being fake!"
    m "Quand je pleurais tout à l'heure, il m'a accusé de faire semblant !"

# game/dinner_5.rpy:383
translate french argument_ending_9cc6bf57:

    # f "Qi, shut up. We're not talking about you."
    f "Qi, ferme-la. Ce n'est pas de toi dont on parle."

# game/dinner_5.rpy:385
translate french argument_ending_362aadbb:

    # m "When I was crying earlier, he was mocking it!"
    m "Quand je pleurais tout à l'heure, il s'est moqué de moi !"

# game/dinner_5.rpy:386
translate french argument_ending_9cc6bf57_1:

    # f "Qi, shut up. We're not talking about you."
    f "Qi, ferme-la. Ce n'est pas de toi dont on parle."

# game/dinner_5.rpy:388
translate french argument_ending_6a1ac1f9:

    # f "So Nick."
    f "Alors, Nick."

# game/dinner_5.rpy:389
translate french argument_ending_3c869948:

    # f "Would you like to say anything, anything at all, about all that?"
    f "Est-ce que tu as quelque chose à dire ?"

# game/dinner_5.rpy:394
translate french argument_ending_53c1cef0:

    # n "Yes."
    n "Oui."

# game/dinner_5.rpy:395
translate french argument_ending_c6f3c79e:

    # n "FUCK this."
    n "Va te faire mettre."

# game/dinner_5.rpy:396
translate french argument_ending_9452715f:

    # n "And FUCK you."
    pass

# game/dinner_5.rpy:399
translate french argument_ending_4093eb4d:

    # n "Fuck BOTH of you, you narcissistic slimy pieces of SHI--"
    n "Allez vous faire foutre, tous les deux, bande de tas de MER--"

# game/dinner_5.rpy:405
translate french argument_ending_d0d8cffe:

    # n "No. I accept my punishment."
    n "Non. J'accepte ma punition."

# game/dinner_5.rpy:406
translate french argument_ending_a1260353:

    # f "Good. At least you're taking this like a man."
    f "Bien. Au moins, tu réagis comme un homme."

# game/dinner_5.rpy:407
translate french argument_ending_24796dbe_6:

    # n ". . ."
    n ". . ."

# game/dinner_5.rpy:411
translate french argument_ending_9288a760:

    # m "sniff..."
    m "sniff..."

# game/dinner_5.rpy:412
translate french argument_ending_414b389d:

    # f "I'm going out to the bar, and getting something actually edible to eat."
    f "Je vais au bar, et aller m'acheter quelque chose de mangeable."

# game/dinner_5.rpy:416
translate french argument_ending_25439b70:

    # f "Honey sweetie dear? Your cooking is shit."
    f "Chérie ? Ta cuisine est nulle."

# game/dinner_5.rpy:419
translate french argument_ending_4733cca1:

    # m ". . ."
    m ". . ."

# game/dinner_5.rpy:423
translate french argument_ending_4a317f18:

    # m "BAWWWWW"
    m "BAWWWWW"

# game/dinner_5.rpy:429
translate french argument_ending_f9c8dd0b:

    # n "You can't hurt me."
    n "Tu ne peux pas me faire de mal."

# game/dinner_5.rpy:430
translate french argument_ending_c485f357:

    # f ". . ."
    f ". . ."

# game/dinner_5.rpy:431
translate french argument_ending_2aa1d92a:

    # m "Dear, no..."
    m "Chéri, non..."

# game/dinner_5.rpy:432
translate french argument_ending_104f7221:

    # f "Mighty strong words, son."
    f "Des mots lourds de conséquence, fils."

# game/dinner_5.rpy:433
translate french argument_ending_94c5f56d:

    # m "Honey, please don't!"
    m "Chéri, non !"

# game/dinner_5.rpy:434
translate french argument_ending_70d13948:

    # f "At least you're standing up to me. Like a man."
    f "Au moins tu me fais face. Comme un homme."

# game/dinner_5.rpy:435
translate french argument_ending_6e61f7e2:

    # m "Please! It's my fault! Don't--"
    m "S'il te plaît ! C'est ma faute ! Ne--"

# game/dinner_5.rpy:436
translate french argument_ending_5ba157ad:

    # f "Ice keeps the swelling down."
    f "La glace empêchera de gonfler."

# game/dinner_5.rpy:437
translate french argument_ending_3d79683b:

    # m "DEAR!"
    m "CHÉRI !"

# game/dinner_5.rpy:445
translate french dinner_ending_punch_76b2fe88:

    # nvl clear
    nvl clear

# game/dinner_5.rpy:462
translate french dinner_ending_76b2fe88:

    # nvl clear
    nvl clear

translate french strings:

    # game/dinner_5.rpy:27
    old "I thought my games were fun..."
    new "Je croyais que mes jeux étaient amusants..."

    # game/dinner_5.rpy:27
    old "Not all games have to be fun."
    new "Tous les jeux n'ont pas à être amusants."

    # game/dinner_5.rpy:27
    old "ART!"
    new "L'ART !"

    # game/dinner_5.rpy:51
    old "It's vomit."
    new "C'est du vomi."

    # game/dinner_5.rpy:51
    old "Don't eat it! It's, uh, really not good."
    new "Ne le mange pas ! C'est, euh, vraiment pas bon."

    # game/dinner_5.rpy:51
    old "Why don't you give it a try, dad?"
    new "Pourquoi tu n'essayes pas, papa ?"

    # game/dinner_5.rpy:89
    old "School's fine."
    new "Ça va."

    # game/dinner_5.rpy:89
    old "I'm studying at a friend's place tomorrow."
    new "Je révise chez un copain, demain."

    # game/dinner_5.rpy:89
    old "DAD I'M BISEXUAL AND BANGING JACK."
    new "PAPA JE SUIS BISEXUEL ET JE BAISE JACK."

    # game/dinner_5.rpy:163
    old "What?! No I don't!"
    new "Quoi ?! Non !"

    # game/dinner_5.rpy:163
    old "Fine. You got me. I have a crush on Claire."
    new "Très bien. Tu m'as eu. J'ai un crush sur Claire."

    # game/dinner_5.rpy:163
    old "I have a boyfriend."
    new "J'ai un copain."

    # game/dinner_5.rpy:231
    old "I failed my midterms."
    new "J'ai foiré mes partiels."

    # game/dinner_5.rpy:231
    old "I had sex with Jack."
    new "J'ai fait l'amour avec Jack."

    # game/dinner_5.rpy:231
    old "I had sex with Claire."
    new "J'ai fait l'amour avec Claire."

    # game/dinner_5.rpy:279
    old "Let's watch it! I haven't seen it yet."
    new "Allons-y ! Je ne l'ai pas encore vu."

    # game/dinner_5.rpy:279
    old "Uh... let's do a different movie..."
    new "Euh... peut-être un autre film, plutôt..."

    # game/dinner_5.rpy:279
    old "Oh, I already saw Inception."
    new "Oh, j'ai déjà vu Inception."

    # game/dinner_5.rpy:318
    old "My boyfriend."
    new "Mon petit ami."

    # game/dinner_5.rpy:318
    old "Mom hates him, coz he happens to be gay."
    new "Maman le déteste parce qu'il est gay."

    # game/dinner_5.rpy:318
    old "Mom hates him, coz she THINKS he's gay."
    new "Maman le déteste parce qu'elle PENSE qu'il est gay."

    # game/dinner_5.rpy:391
    old "Yes. Fuck this, and fuck you."
    new "Oui. Va te faire foutre."

    # game/dinner_5.rpy:391
    old "No. I accept my punishment."
    new "Non. J'accepte ma punition."

    # game/dinner_5.rpy:391
    old "You can't hurt me."
    new "Tu ne peux pas me faire de mal."
