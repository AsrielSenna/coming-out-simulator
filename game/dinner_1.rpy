label start_dinner_1:

    # /////// SET UP SCENE ////////

    show background dinner
    show clock ticking
    show clockn 1855
    show dinner_nicky_sit as nicky
    show expression Null() as dad
    show expression Null() as mom
    show dinner table

    play music dinner_ticking

    # ////////////////////////////

    pause 2.5
    n "Where is everyone?..."
    n ". . ."

    menu:
        "Moooom?":
            $ message = "Moooom?"
            jump waiting_1
        "Daaaaad?":
            $ message = "Daaaaad?"
            jump waiting_1
        "Hello, anybody?":
            $ message = "Hello, anybody?"
            jump waiting_1

label waiting_1:

    $ what_you_called_out = message;
    $ n("[message!t]")

    n ". . ."

    menu:
        "[[start eating]":
            $ message = "[[start eating]"
            $ waiting_action = "eat"
            jump waiting_2
        "[[wait some more]":
            $ message = "[[wait some more]"
            $ waiting_action = "wait"
            jump waiting_2
        "[[play with food]":
            $ message = "[[play with food]"
            $ waiting_action = "play"
            jump waiting_2

label waiting_2:

    $ n("[message!t]")
    n ". . ."

    play music dinner_meowing

    show clock meowing
    show clockn 1900
    pause 1

    show dinner_nicky_defiant as nicky

    menu:
        "Cut the crying, cacophonous cat clock!":
            n "Cut the crying, cacophonous cat clock!"

            show mom stand
            show clock ticking
            play music dinner_ticking

            if (im_a_poet):
                m "Did you learn poetry from a friend?"
            else:
                m "Poetic."

            show dinner_nicky_sit as nicky
            n "Oh, hey mom."

        "Ugh, why did we get that thing?":
            n "Ugh, why did we get that thing?"

            show mom stand
            show clock ticking
            play music dinner_ticking

            m "Your grandfather gave it to us."

            show dinner_nicky_sit as nicky
            n "Oh! Hey mom."

        "Meow! Meow! Meow! Meow!":

            n "Meow."
            n "Meow!"

            show dinner_nicky_outrage as nicky
            n "MEOW!"

            show mom stand

            m "Nick, what are you doing?..."

            show clock ticking
            play music dinner_ticking
            show dinner_nicky_sit as nicky

            n "MEOOOhhhh didn't see you. Ahem. Hey mom."


label Waiting_End:
    jump start_dinner_2
