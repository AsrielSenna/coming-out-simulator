# is short
# What ARE you. Fake crying, and don't tell your dad
# Weave it

label start_dinner_4:

    n ". . ."
    m "It's because your dad's almost never home, isn't it?"
    m "Without a strong male role model, you become confused..."

    menu:
        "Yeah, coz Dad's SUCH a great role model.":
            n "Yeah, coz Dad's SUCH a great role model."
            m "Nick, no matter what, he's your father. You should love him."
        "That's not how it works. I'd be bi anyway.":
            n "That's not how it works. I'd be bi anyway."
            m "How do you know?! Are you an expert in psychology?!"
        "You know what? Maybe you're right.":
            n "You know what? Maybe you're right."
            m "I know..."
    jump my_fault

label my_fault:

    show clockn 1930

    n ". . ."
    m "This is all my fault..."
    m "I told you to be careful around those kinds of people, but I told you too late..."

    show mom cry

    m "[[sob]"
    m "Oh Nick! My poor baby!"

    show dinner_nicky_sit as nicky

    menu:
        "Mom... please don't cry...":
            $ message = "Mom... please don't cry..."
            jump cry_1
        "Quit your fake crying.":
            $ message = "Quit your fake crying."
            jump cry_2
        "[[cry]":
            $ message = "[[cry]"
            jump cry_3

label cry_1:

    $ crying = "sympathy"

    $ n("[message!t]")
    m "huu... huu... huu..."
    n "I'm sorry. About Jack, the lies, everything."
    m "owww... owww..."
    n "I take it all back."
    m "sniff..."
    n "...please..."
    jump what_are_you

label cry_2:

    $ crying = "anger"
    show dinner_nicky_defiant as nicky

    $ n("[message!t]")
    m "huu... huu... huu..."
    n "Seriously, it is SO fake."
    m "owww... owww..."
    n "Will you shut up?!"
    m "sniff..."
    n "SHUT. UP."
    jump what_are_you

label cry_3:

    $ crying = "mocking"
    show dinner_nicky_outrage as nicky

    n "BAWWWWW"
    m "huu... huu... huu..."
    n "WAH WAH WAH WAH WAHHH"
    m "owww... owww..."
    n "BRRrrRR-BRR-BRbR BWAH BWAHRR rrrRRR-WaahHH WO WO WO RaaahhH"
    m "sniff..."

    show dinner_nicky_defiant as nicky
    n "Okay, we done?"
    jump what_are_you

label what_are_you:

    m ". . ."
    m "Nick... what are you?"
    n "Excuse me?"

    show dinner_nicky_sit as nicky

    show mom sit
    m "What {i}are{/i} you?"

    menu:
        "I'm bisexual.":

            $ what_are_you = "bisexual"

            n "I'm bisexual."
            if (admit_bisexuality):
                m "...and you said that means..."
            n "Sexually attracted to both men and women."
            m "You can't be both."
            m "You have to pick one."
            n "That's... not how it works. At all."
        "I'm just confused.":

            $ what_are_you = "confused"

            n "I'm just confused."
            m "...I know."
            m "I'm sorry Jack confused you."
            m "You're just going through a phase, it's okay."
            n ". . ."
            m "It's okay. It's okay..."
        "I'm your son, dammit.":

            $ what_are_you = "son"

            n "I'm your son, dammit."
            n ". . ."
            n "Isn't that enough?"
    jump have_you_had_sex

label have_you_had_sex:
    m ". . ."
    m "Did you have sex with Jack."
    menu:
        "Yes.":
            n "Yes."
            m "[[DRY HEAVE]"
        "No.":
            n "No."
            m "Please stop lying... I saw your texts..."
            n "We were just sexting, we didn't actually--"
            m "...and your photos..."
        "I'm not saying.":
            n "I'm not saying."
            m "oh my god... you did."
    jump have_you_had_sex_2

label have_you_had_sex_2:

    n ". . ."
    m "Which... one of you is the woman?"

    show dinner_nicky_outrage as nicky

    n "OH COME ON!"
    n "That's like asking which chopstick is the spoo--"
    m "Which one of you?..."

    show dinner_nicky_defiant as nicky

    menu:
        "I'm usually the bottom.":
            $ top_or_bottom = "bottom"

            n "I'm usually the bottom."
        "Jack is, mostly.":
            $ top_or_bottom = "top"

            n "Jack is, mostly."
            m "Th-that... means you could still be straight! R-right?..."
            m "If... you know... you're the one who puts your..."
            m "your..."
        "We take turns.":
            $ top_or_bottom = "versatile"

            n "We take turns."
    jump throw_up

label throw_up:

    play sound dinner_vomit

    show clockn 1940
    show mom vomit
    show dinner table_2
    pause 1

    menu:
        "what.":
            n "what."
        "whaaat.":
            n "whaaat."
        "whaaaaaaaaaaaaaaat.":
            n "whaaaaaaaaaaaaaaat."
    jump father_soon

label father_soon:

    show mom sit

    m ". . ."
    m "Your father will be back soon."
    n "The food's cold. Well, except for the spot you just uh, reversed, on."
    m "Your dad's late. Must have been a stressful day at work."
    m "So... please... when he's back..."
    m "Promise me you'll keep all this secret?"
    n ". . ."

    m "Don't tell him about Jack."

    if what_are_you == "bisexual":
        m "Don't tell him you think you're bisexual."
    elif what_are_you == "confused":
        m "Don't tell him you're confused about your sexuality."
    elif what_are_you == "son":
        m "Don't tell him you lied to us so you could... do things with Jack."

    if top_or_bottom == "top":
        m "Don't tell him you make Jack a woman."
    elif top_or_bottom == "bottom":
        m "Don't tell him you act like a woman with Jack."
    elif top_or_bottom == "versatile":
        m "Don't tell him you and Jack both act like women."

    m "Okay?..."

    menu:
        "Okay.":
            $ promise_silence = "yes"

            n "Okay."
            m "Okay."
            m ". . ."
            m "Your father's here."
        "No. Not okay.":
            $ promise_silence = "no"

            n "No. Not okay."
            m "Nick, no no no, please--"
            m "Oh no. Your father's here."
        "As long as you don't tell him, either.":
            $ promise_silence = "tit for tat"

            n "As long as you don't tell him, either."
            m "I won't."
            n "Promise me you won't."
            m "I pr--"
            m "Shhh. Your father's here."
    jump father_soon_2

label father_soon_2:
    show dinner_nicky_sit as nicky
    jump start_dinner_5
